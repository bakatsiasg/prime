'use strict';

/**
 * @ngdoc service
 * @name talos-api.Services.ApplicationSettings
 * @description ApplicationSettings Service
 */

talosApi.service('ApplicationSettings', [
        'TalosCore', 'TalosValues', 'Constants',
        function( TalosCore, TalosValues, Constants ) {
            var ApplicationSettings = {};

            ApplicationSettings.settings = {};
            ApplicationSettings.scopeTypes = Constants.APPLICATION_SETTING_SCOPE_TYPE_APPLICATION;

           ApplicationSettings.getValue = function (key)
          {
            return ApplicationSettings.settings[key];
          }


            ApplicationSettings.get = function(scopeTypes,callback) {

              if(ApplicationSettings.settings[scopeTypes])
              {
                callback(ApplicationSettings.settings[scopeTypes])
              }
              else
              {
                  TalosCore.http2Talos(
                    'GET', '/appsettings/app/'+'816502C9-23D7-49A2-D730-C1B84A51AD6F',//Constants.APPLICATION_ID,
                    { 'scopeTypes': ApplicationSettings.scopeTypes},
                    null).then(function (success)
                {
                  var j = 0;
                  var l = success.data.length;
                    ApplicationSettings.settings[scopeTypes] = success.data;
                   for (j; j < l; j++) {
                          //if(success.data[j].key == Constants.APPLICATION_SETTING_HOW_IT_WORKS
                          //||success.data[j].key == Constants.APPLICATION_SETTING_SIDE_MENU)
                          //{
                          //    var arr = success.data[j].value.split("|");
                          //    var resultArr = new Array();
                          //
                          //    var i = 0;
                          //    var l2 = arr.length;
                          //    var  value ;
                          //    for(;i<l2;i++)
                          //    {
                          //        value  =  arr[i].split(",");
                          //        resultArr.push({index:(i+1),html:value[0],state:value[1]})
                          //    }
                          //    ApplicationSettings.settings[success.data[j].key] = resultArr;
                          //}
                          //else{ApplicationSettings.settings[success.data[j].key] = success.data[j].value;}
                      ApplicationSettings.settings[success.data[j].key] = success.data[j].value;
                  }
                  callback(ApplicationSettings.settings[scopeTypes]);
                },function(error){
                  return callback(null);
                });
              }


            };

            return ApplicationSettings;
        }
    ]
);
