/**
 * Created by georgefloros on 3/30/15.
 */
'use strict';
talosApi.factory('EventService', ["$rootScope",
        function($rootScope){
            var EventService = {};
            EventService.sendEvent = function (event,args)
            {
                $rootScope.$broadcast(event,args);
            };
            return EventService;
        }]);