'use strict';

/**
 * @ngdoc service
 * @name talos-api.Services.Profile
 * @description Profile Service that keep the current logged in user's Profile (that get it from User Service)
 */

talosApi.service('Profile', [
        'User', '$q', '$rootScope', 'Constants', 'TalosValues',"$log",'EventService',
    'Events','Universities','Tournaments','ApplicationSettings',
        function ( User, $q, $rootScope, Constants, TalosValues,$log,EventService,Events,Universities,Tournaments,ApplicationSettings) {
            var Profile = {};
            Profile.setBallances = function(callBack)
            {

                var errorMsg = function(msg){
                    Profile.checkInPoints = 0;
                    Profile.lotteryPoints = 0;
                    Profile.lotteryActive = false;
                    Profile.activeEntries = false;
                    Profile.hasExtraGift = false;
                    callBack(msg);
                };

                if(Profile.userHasCheckInMetadata)
                {

                    Tournaments.getUserStatistics(Constants.GAME_TYPE,[0,3],[ApplicationSettings.getValue(Constants.APPLICATION_SETTING_LEADERBOARD_USER_KEY)+"="+Profile.school.itemId]).then(function(success) {
                        $log.debug(success);


                        var i,length,tournamentS;
                        length = success.data.length;
                        if(length > 0)
                        {
                            for(i = 0 ; i < length ; i++)
                            {
                                tournamentS =  success.data[i];
                                switch(tournamentS.tournamentTypeId)
                                {
                                    case 0: // leaderboard
                                    {
                                        Profile.checkInPoints = tournamentS.score !=null ? parseInt(tournamentS.score):0;
                                        break;
                                    }
                                    case 3: // lottery
                                    {
                                        Profile.lotteryPoints = tournamentS.score !=null ? parseInt(tournamentS.score):0;
                                        Profile.activeEntries = tournamentS.status;
                                        break;
                                    }
                                }
                            }
                        }

                        var endFlow = function (){
                            EventService.sendEvent(Events.PROFILE_UPDATED,null);
                            callBack(success);
                        }
                        User.getAchievements().then(function(achievement){

                            length = achievement.data.length;
                            if(length > 0)
                            {
                                var achievement;
                                for(i = 0 ; i < length ; i++)
                                {
                                    achievement = achievement.data[i];
                                    if(achievement.achievementTypeId == ApplicationSettings.getValue(Constants.APPLICATION_SETTING_ACHIEVEMENT_ID) && achievement.numOfTimesAchieved >=1 )
                                    {
                                        Profile.hasExtraGift = true;
                                        break;
                                    }
                                }
                            }
                            endFlow();
                        },function(achievementError){
                            Profile.hasExtraGift = false;
                            endFlow();
                        });

                    },function(error){

                        errorMsg(error)
                    });
                }
                else
                {
                    errorMsg(null);
                }
            };
            Profile.setPushToken = function(token)
            {
                Profile.pushToken = token;
                Profile.updateUserPushToken();
            };
            Profile.updateUserPushToken = function()
            {
                if(!Profile.isGuest && Profile.pushToken) {
                    User.registerpushNotificationDevice(Profile.pushToken.key, Profile.pushToken.value);
                }
            };

            Profile.set = function (data,callBack) {
                Profile.profile = data;
                Profile.isPendingActivation = data.status == 6;
                Profile.isLocked = data.status == 7;
                Profile.isGuest = data.guest == true  || Profile.isPendingActivation == true;
                Profile.Activated = Profile.isGuest == false && Profile.isLocked == false && Profile.isPendingActivation == false;

                Profile.isFacebookUser = (data.extAppSourceIds) && (data.extAppSourceIds.indexOf(Constants.EXTERNAL_SYSTEM_FACEBOOK) != -1);
                Profile.isTwitterUser = (data.extAppSourceIds) && (data.extAppSourceIds.indexOf(Constants.EXTERNAL_SYSTEM_TWITTER) != -1);

                //Profile.isYellowDayUser = false;
                //if(data.registeredFromApplication !=null)
                //{
                //    var reg = data.registeredFromApplication.toLowerCase();
                //    var appIds = ApplicationSettings.getValue(Constants.APPLICATION_SETTING_REGISTER_FROM_APPLICATION).toLowerCase();
                //    Profile.isYellowDayUser = appIds.indexOf(reg) > -1
                //}

                Profile.isYellowDayUser = data.registeredFromApplication !=null &&  ApplicationSettings.getValue(Constants.APPLICATION_SETTING_REGISTER_FROM_APPLICATION).toLowerCase().indexOf(data.registeredFromApplication.toLowerCase()) > -1;


                var schoolId = "";
                var universityId = "";
                angular.forEach(data.customUserProfileFieldList, function (value, key) {
                        if (value.key == 'Personal.department') {
                            schoolId = value.value;
                        }
                        else if (value.key == 'Personal.university') {
                            universityId = value.value;
                        }
                        else if (value.key == 'Personal.code') {
                            Profile.studentId = value.value;
                        }
                    });
                    Profile.userHasCheckInMetadata = schoolId != "" && universityId != "" && Profile.studentId != "";
                    if (Profile.userHasCheckInMetadata) {
                        EventService.sendEvent(Events.ENABLE_CHECK_IN);
                        var i, j, university, school, univeritiesLength, schoolsLength;

                        univeritiesLength = Universities.universities.length;
                        var found = false;
                        for (i = 0; i < univeritiesLength; i++) {
                            university = Universities.universities[i];
                            if (university.itemId == universityId) {
                                {
                                    Profile.university = university;
                                    Profile.universityIndex = i;
                                    schoolsLength = university.schools.length;
                                    for (j = 0; j < schoolsLength; j++) {
                                        school = university.schools[j];
                                        if (school.itemId == schoolId) {
                                            Profile.school = school;
                                            Profile.schoolIndex = j;
                                            found = true;
                                            break;
                                        }
                                    }
                                }
                                if(found)
                                    break;
                            }
                        }
                    }
                   Profile.updateUserPushToken();
                   EventService.sendEvent(Events.PROFILE_UPDATED,null);
                };

            Profile.get = function(callback,forced) {
               $log.debug("Profile.get");
                if (Profile.profile != undefined && Profile.profile!=null &&!forced) {
                    callback(Profile.profile);
                }else{
                    User.get(TalosValues.getUserId())
                      .then(function(data) {
                            Profile.set(data.data);
                            Profile.setBallances(function(data){
                                callback(Profile.profile);
                            });
                      }
                    ,function(error)
                      {
                          Profile.profile = null;
                          callback(Profile.profile);
                      });
                  }

            };

            Profile.clear = function() {
                Profile.profile=null;
                Profile.isGuest = true;
                Profile.isFacebookUser = false;
                Profile.isTwitterUser = false;
                Profile.isPendingActivation = false;
                Profile.isLocked = false;
                Profile.isYellowDayUser = false;
                Profile.Activated = false;
                Profile.userHasCheckInMetadata = false;
                Profile.studentId = "";
                Profile.university = null;
                Profile.school = null;
                Profile.universityIndex = -1;
                Profile.schoolIndex = -1;
                Profile.checkInPoints = 0;
                Profile.lotteryPoints = 0;
                Profile.activeEntries = false;
                Profile.hasExtraGift = false;


                //Profile.lon = 0.0;
                //Profile.lat = 0.0;
            };

            Profile.imageUpdated = function() {
                Profile.profile.picUrl = Constants.USER_AVATAR_IMAGE_URL.replace("$USERID", TalosValues.getUserId());
            };

            Profile.pushToken = null;
            Profile.clear();


            return Profile;
        }
    ]);
