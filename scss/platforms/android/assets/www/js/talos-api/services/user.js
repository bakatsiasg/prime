'use strict';

/**
 * @ngdoc service
 * @name talos-api.Services.User
 * @description User Factory
 */

    talosApi.service('User', [
        'TalosCore', 'TalosValues','$rootScope','Constants','$log',
        function( TalosCore, TalosValues,$rootScope,Constants,$log) {
            var User = {};


            User.createGuest = function(){
              return  TalosCore.http2Talos('POST', '/users/guest/getSession',
                    null,
                    {
                        "username":"",
                        "password":"",
                        "languageCode":TalosValues.getLanguage(),
                        "info":"",
                        "latitude":0,
                        "longitude":0,
                        "trackingInfo":null,
                        "clientIPAddress":"",
                        "nationalityIsoCode":TalosValues.getLanguage(),
                        "clientTypeId":Constants.CLIENT_TYPE
                    }
                ).then(function(data) {
                      $log.debug('Guest : ' + data);
                        TalosValues.setUserId(data.data.userId);
                        TalosValues.setUserSessionId(data.data.userSessionId);
                        return TalosCore.http2Talos('POST', '/users/' + TalosValues.getUserId() + '/authKey',
                            null,
                            {"expiresSeconds":Constants.TOKEN_EXPIRATION_SEC}
                        ).then(function(data) {
                                //console.log('authorization KEY : ' + data.data.authorizationKey)
                                TalosValues.setAuthKey(data.data.authorizationKey);
                                $rootScope.hasAuthKey = true;
                                return data;
                            },function(error){
                                return error;
                            });
                    },function(error){
                      $log.debug('Guest : ' + error);
                        return error;
                    });

            };

            User.getVItems = function(userId){

            };
            User.authenticatedKey = function(key) {
                return  TalosCore.http2Talos('GET', '/users/authenticateKey',null,{'authenticatedKey':key})
            };
            User.get = function (userId) {
                return  TalosCore.http2Talos('GET', '/users/' + userId, null, null)
            };
            User.myProfile = function () {
                return TalosCore.requireSession(function () {
                    return TalosCore.http2Talos('GET', '/users/' + TalosValues.getUserId(), null, null);
                });
            };
            User.updateMyProfile = function (userDto,newPassword) {
                return TalosCore.requireSession(function () {
                    return TalosCore.http2Talos(
                        'POST', '/users/' + TalosValues.getUserId(),
                        null,
                        {'user':userDto,'newPassword':newPassword});
                });
            };
            User.balances = function (unitTypeIds) {
                return TalosCore.requireSession(function () {
                    return TalosCore.http2Talos(
                        'GET', '/users/' + TalosValues.getUserId() + '/balance',
                        { unitTypeId: unitTypeIds },
                        null);
                });
            };
            User.upgrade = function (userObj) {
                return TalosCore.requireSession(function () {
                    return TalosCore.http2Talos(
                        'POST', '/users/guest/upgrade',
                        null,
                        userObj);
                });
            };
            User.updateToken = function (params) {
                return TalosCore.requireSession(function () {
                    return TalosCore.http2Talos(
                        'POST', '/users/external/updateToken',
                        null,
                        params
                    );
                });
            };
            User.associateSession = function (authenticationSource, token, secret, externalUserID, externalAppId) {
                return TalosCore.requireSession(function () {
                    return TalosCore.http2Talos(
                        'POST', '/users/external/associateSession',
                        null,
                        {
                            authenticationSource : authenticationSource,
                            authToken : token,
                            authTokenSecret : secret,
                            externalApplicationId : externalAppId,
                            userId : externalUserID
                        });
                });
            };
            User.uploadAvatar = function (imageBase64) {
                return TalosCore.requireSession(function () {
                    return TalosCore.http2Talos(
                        'POST', '/users/' + TalosValues.getUserId() + '/avatars',
                        null,
                        {
                            imageData : imageBase64
                        });
                });
            };
            User.requestActivationByEmail = function (email) {
                return TalosCore.http2Talos('GET', '/users/requestActivationByEmail/' + email, null, null);
            };


            User.resetPasswordRequest = function (email) {
                return TalosCore.http2Talos('GET', '/users/resetPassword/' + email, null, null);
            };

            User.getFriends = function (from, to) {
                return TalosCore.requireSession(function () {
                    return TalosCore.http2Talos(
                        'GET', '/users/' + TalosValues.getUserId() + '/friends',
                        {'rangeFrom': from, 'rangeTo': to},
                        null);
                });
            };

            User.stats = function(input) {
                return TalosCore.requireSession(function() {
                    return TalosCore.http2Talos(
                        'POST', '/users/' + TalosValues.getUserId() + '/stats',
                        null,
                        input);
                });
            };

            User.updateParkingPlace = function(input) {
                return TalosCore.requireSession(function() {
                    return TalosCore.http2Talos(
                        'POST', '/mallBG/parking/' + TalosValues.getUserId(),
                        null,
                        input);
                });
            };

            User.level = function (userId) {
                return TalosCore.requireSession(function () {
                    return TalosCore.http2Talos(
                        'GET', '/users/' + userId + '/level',
                        null,
                        null);
                });
            };
            User.nickNameAvailable = function (nickName)
            {
                $log.debug("User.nickNameAvailable");
                return TalosCore.http2Talos(
                    'GET', '/users/nicknameCheck/'+nickName,
                    null,null);
            };

            User.savemetadata = function (keyvalues)
            {
                $log.debug("User.savemetadata");
              return TalosCore.requireSession(function () {
                return TalosCore.http2Talos(
                  'POST', '/users/saveMetadata',
                    null,keyvalues);
              });
            };
            User.getAchievements = function(){
                return TalosCore.requireSession(function () {
                    return TalosCore.http2Talos(
                        'GET', '/users/' + TalosValues.getUserId() + '/achievements',
                        null,null);
                });
            };
            User.elevateSession = function(username,password){
                return TalosCore.requireSession(function () {
                    return TalosCore.http2Talos(
                        'POST', '/users/' + TalosValues.getUserId() + '/elevateSession',
                        null,
                        {'username':username,'password':password,'elevationPeriodInSeconds':50000});
                });
            };
            User.upgradeOrRegister = function(user,upgrade,callBack) {

                if (upgrade) // upgrade
                {
                    User.upgrade(user).then(function(success){
                        callBack(true,"");
                    },function(error){
                        callBack(false,error.data.code);
                    });
                }
                else {
                        User.createGuest().then(function(success){
                            User.upgrade(user).then(function(success){
                                callBack(true,"");
                            },function(error){
                                callBack(false,error.data.code);
                            });
                        },function(error){
                        });
                    }
            };

            User.registerpushNotificationDevice = function (key,value){
                var keyValues = [];
                keyValues.push({'key':key,'value':value});
                User.savemetadata({'userMetadataToSave':keyValues}).then(function(success){
                    $log.debug(success);
                },function(error){
                    $log.debug(error);
                });
            };

            User.merge = function(){
                return TalosCore.http2Talos('GET',"/checkInClass/merge/"+TalosValues.getOldUserSessionId(),
                    null,
                    null)
            };
            return User;
        }
    ]);
