'use strict';

/**
 * @ngdoc service
 * @name talos-api.Services.Resources
 * @description Resources Service
 */

talosApi.service('Resources',
        ['$q','Constants','TalosCore','localStorageService','ApplicationSettings',function($q,Constants, TalosCore,localStorageService,ApplicationSettings) {

            function getResources (from, to, input,storedKey,appsettingValue,callback){
                TalosCore.http2Talos(
                    'POST', '/item/resources/search',
                    {'rangeFrom': from, 'rangeTo': to},
                    input).then(function (rdata) {
                    try {
                        var length = rdata.data.length;
                        if (storedKey) {

                            if(length > 0) {
                                var i = 0;
                                var resource;
                                for (; i < length; i++) {
                                    resource = rdata.data[i];
                                    localStorageService.set(storedKey + '_resource_' + resource.itemId + "_" + resource.itemTypeTO.itemTypeId + "_" + resource.resourceTypeTO.resourceTypeId + "_" + resource.languageTO.languageId + "_" + appsettingValue, resource);
                                }
                            }
                            else{
                                rdata.data = new Array();
                            }
                            }

                        callback(rdata);
                    } catch (error) {
                        callback({'data':null})
                    }
                }, function (error) {
                        callback({'data':null})

                    })

                //return deferred.promise();
            }


            var getMultipleResources = this.getMultipleResources = function(from, to, input,storedKey) {
                var deferred = $q.defer();
                //setTimeout(function() {
                    var appsettingValue;
                    if (storedKey) {

                        appsettingValue = ApplicationSettings.getValue(storedKey);

                        var returnResources=new Array();
                        var aksResources = new Array();
                        var length = input.languageIds.length;
                        var length2 = input.resources.length;
                        var storedResource;
                        var resource;
                        var i = 0;
                        var j = 0;
                        var ask = false;
                        for(; i<length ;i++){
                            j = 0;
                            for(;j<length2 ; j++){
                                resource = input.resources[j];
                                storedResource = localStorageService.get(storedKey + '_resource_'+resource.itemId+"_"+resource.itemTypeId+"_"+resource.resourceTypeId+"_"+input.languageIds[i]+"_"+appsettingValue);
                                if(storedResource)
                                {
                                    returnResources.push(storedResource);
                                }
                                else if(aksResources.indexOf(resource) == -1)
                                {
                                    aksResources.push(resource);
                                    ask = true;
                                }
                            }
                        }
                        if(ask)
                        {
                            input.resources = aksResources;
                            getResources(from, to, input,storedKey,appsettingValue,function(data){
                            if(data){
                                deferred.resolve({'data': returnResources.concat(data.data)});
                            }
                            else{
                                deferred.reject(data)
                            }});
                        }
                        else{
                            setTimeout(function(){deferred.resolve({'data':returnResources})},500);
                        }
                    }
                    else {
                        getResources(from, to, input,storedKey,appsettingValue,function(data){
                            if(data){
                                deferred.resolve(data);
                            }
                            else{
                                deferred.reject(data)
                            }});


                    }
               // },500);
                return deferred.promise;

            };

        }]);
