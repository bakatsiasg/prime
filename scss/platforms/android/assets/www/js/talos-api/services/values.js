'use strict';

talosApi.factory('TalosValues', [
        'localStorageService',
        function(localStorageService) {
        var values = {};
        var _userSessionId = "";
            var _oldUserSessionId = "";

        values.setUserSessionId = function (userSesionId) {
            _oldUserSessionId = _userSessionId;
            _userSessionId = userSesionId;
        };
        values.getUserSessionId = function () {
            return _userSessionId;
        };

            values.getOldUserSessionId = function () {
                return _oldUserSessionId;
            };

        values.setUserId = function (userId) {
            localStorageService.set('UserId', userId);
        };
        values.getUserId = function () {
            var _userId = localStorageService.get('UserId');
            return _userId ? _userId: "";
        };
        values.setAuthKey = function (authKey) {
            localStorageService.set('AuthKey', authKey);
        };
        values.getAuthKey = function () {
            var _authKey = localStorageService.get('AuthKey');
            return _authKey ? _authKey: "";
        };

        values.setLanguage = function (lang) {
            localStorageService.set('lang', lang);
        };
        values.getLanguage = function () {
            var lang = localStorageService.get('lang');
            return lang ? lang: 'el';
        };
        /* clear data relative to user, called on logout */

            values.clearData = function () {
                values.setAuthKey("");
                values.setUserId("");
                values.setUserSessionId("");

            };
        values.clearUserData = function () {
            values.setAuthKey("");
            values.setUserId("");
            values.setUserSessionId("");
            values.setLanguage("");
        };

        return values;
    }]);
