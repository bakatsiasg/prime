/**
 * Created by georgefloros on 4/20/15.
 */


services.service('Utils',['$rootScope','Constants','localStorageService','TalosValues',
    '$translate','EventService','$log','base64',
    function ($rootScope, Constants,localStorageService,TalosValues,
              $translate,EventService,$log,base64) {
   var Utils = {};

    Utils.getResourceUrl = function(itemId, itemTypeId, resourceTypeId) {
        return Constants.RESOURCE_URL + "?itemId=" + itemId + "&itemTypeId=" + itemTypeId + "&resourceTypeId=" + resourceTypeId + "&languageId=" + Constants['LANGUAGE_ID_'+TalosValues.getLanguage()];
    };

    Utils.userIsPendingOrLock = function (){
        if(Profile.isPendingActivation)
        {
            $rootScope.showMainPopUp("templates/popUp/youHaveToConfirmEmail.html",null);

            return true;
        }
        else if(Profile.isLocked)
        {
            $rootScope.showErrorPopUp({msg:"your account is locked",actions:[{'label':"ok",action:null}]});
            return true;
        }
        else
        {
            return false;
        }

    };


    Utils.convertToTalosDate = function(date) {
        return (date) ? $filter('date')(date, 'dd/MM/yyyy') : "";
    };

    Utils.convertFromTalosDate = function(date) {
        //$log.debug("convertFromTalosDate " + date)
        // using 'shortDate' format which is MM/dd/yyyy or dd/MM/yyyy, depending on locale
        return (date && date != null) ? new Date(moment(date,'DD/MM/YYYY')) : null;
    };
    Utils.logOutUser = function(forced){

        if(Profile.isGuest == false || forced == true) {
            $rootScope.showTabBar = false;
            $rootScope.tapBarModel = '';
            $rootScope.hasAuthKey = false;
            $rootScope.enableSideMenu = true;
            Profile.clear();
            localStorageService.clearAll();
            localStorageService.cookie.clearAll();
            $rootScope.goToState('loading');
            EventService.sendEvent("$translateLoadingError");
            TalosValues.clearUserData();
        }
    };

    Utils.updateProfileDto = function (user){

        var userObj = angular.copy(Profile.profile);
        userObj.username = user.nickname;
        userObj.nickname = user.nickname;
        userObj.preferedLanguageCode = TalosValues.getLanguage();
        userObj.nationalityIsoCode = TalosValues.getLanguage();
        userObj.customUserProfileFieldList =
        [
            {"key": 'Personal.name', "value": user.firstName}
            ,{"key": 'Personal.surname', "value": user.lastName}
            ,{"key": 'Personal.university', "value": user.universityId}
            ,{"key": 'Personal.department', "value": user.schoolId}
            ,{"key": 'Personal.code', "value": user.studentId}
        ];

        return userObj;
    };
    Utils.createCheckInUserDto = function(user){
            var userObj =  {
                "username":user.nickname,
                "password":user.password,
                "nickname":user.nickname,
                "email":user.email.text,
                "gender":"",
                "birthdate":"",
                "nationalityIsoCode":TalosValues.getLanguage(),
                "profilePictureUrl":"",
                "preferedLanguageCode":TalosValues.getLanguage(),
                "customUserProfileFieldList":
                    [
                        {"key": 'Personal.name', "value": user.firstName}
                        ,{"key": 'Personal.surname', "value": user.lastName}
                        ,{"key": 'Personal.university', "value": user.universityId}
                        ,{"key": 'Personal.department', "value": user.schoolId}
                        ,{"key": 'Personal.code', "value": user.studentId}
                    ]
            };
            return userObj;
    };
    Utils.createFormUserDto = function(user){
        var userObj = {'firstName':"",
                        'lastName':"",
                        'email':{
                            'text':""
                        },
                        'password':"",
                        'confirmPass':"",
                        'studentId':"",
                        'universities':Universities.universities,
                        'university':-1,
                        'school':-1,
                        'nickname':'',
                        'terms':false
                        };

        if(user!=null)
        {
            if(angular.isDefined(user.guest) && user.guest == false) {
                userObj.nickname = user.nickname;
                userObj.password = Constants.DEFAULT_PASSWORD;
                userObj.confirmPass = Constants.DEFAULT_PASSWORD;
                userObj.terms = true;
            }
            var i, j,value,length,length2;

            length = user.customUserProfileFieldList ?user.customUserProfileFieldList.length : 0;
            var schoolId = "";
            var universityId = "";
            for(i = 0;i<length;i++)
            {
                value = user.customUserProfileFieldList[i];

                if (value.key == 'Personal.department') {
                    schoolId = value.value;
                }
                else if (value.key == 'Personal.university') {
                    universityId = value.value;
                }
                else if (value.key == 'Personal.code') {
                    userObj.studentId = value.value;
                }
                else if (value.key == 'contact.email')
                {
                    userObj.email.text = value.value;
                }
                else if (value.key == 'Personal.surname')
                {
                    userObj.lastName = value.value;
                }
                else if (value.key == 'Personal.name')
                {
                    userObj.firstName = value.value;
                }

            }

            if (schoolId != "" && universityId != "") {

                //length = Universities.universities.length;
                //var university,school;
                //var found = false;
                userObj.school = Profile.schoolIndex;
                userObj.university = Profile.universityIndex;
                //for (i = 0; i < length; i++) {
                //    university = Universities.universities[i];
                //    if (university.itemId == universityId)
                //        {
                //            userObj.university = i;
                //            length2 = university.schools.length;
                //            for (j = 0; j < length2; j++) {
                //                school = university.schools[j];
                //                if (school.itemId == schoolId) {
                //                    userObj.school = j;
                //                    found = true;
                //                    break;
                //                }
                //            }
                //        }
                //        if(found)
                //        break;
                //    }
                }
            }

        return userObj;
    };
    Utils.createHowList = function (prefix) {
        var arr = [];
        var items = $translate.instant(prefix + "_States").split(",");
        var length = items == null ? 0 : items.length;
        var index_;
        for (var i = 0; i < length; i++) {
            index_ = i + 1;
            arr.push({index: index_, html: prefix + "_" + index_, state: items[i]})
        }
        return arr;
    };
    Utils.getResource = function(item, resourceId) {
        try {
            var lang = TalosValues.getLanguage();
            return item.resources[resourceId+"_"+TalosValues.getLanguage()];
        } catch(error) {
            return "";
        }
    };

    Utils.hoursFromLastAction = function(lastActionKey) {
        var lastAction = localStorageService.get(lastActionKey);
        var lastCheckIn = lastAction == ""|| lastAction == null || lastAction == undefined ? Date.now():new Date(lastAction).getTime();
        var now = Date.now();
        var one_hour=1000*60*60;
        return  Math.ceil((now - lastCheckIn)/(one_hour));
    };

    Utils.LocalStorageToBase64 = function(){
        var values = {};
        angular.forEach(localStorageService.keys(),function(value,key){
            if(value != "hasSeenInstrucions" && value != "lang")
            {
                values[value] = localStorageService.get(value);
            }
        });
        var basestr = base64.encode(unescape(encodeURIComponent(JSON.stringify(values))));

        var blob = new Blob(['<?xml version="1.0" encoding="utf-8"?><storage>'+basestr+'</storage>'], {type: "text/plain;charset=utf-8"});
        saveAs(blob, "data.xml");
        return basestr;
    };

        Utils.Base64ToLocalStorage = function(base){
            var baseDec = JSON.parse(decodeURIComponent(escape(base64.decode(base))));
            $log.debug("baseDec = "+baseDec);
            angular.forEach(baseDec,function(value,key){
                localStorageService.set(key,value);
            })
        };

        Utils.ShowYouAreNowCICMember = function(params,cancel,isYellowDay){

            var hasSeenCicMember = localStorageService.get("hasSeenCicMember");
            if(hasSeenCicMember == null || angular.isUndefined(hasSeenCicMember) || hasSeenCicMember == false) {
                $rootScope.showMainPopUp("templates/popUp/youAreNowCICMember.html", params,true);
                localStorageService.set("hasSeenCicMember",true);
            }
            else{
                cancel();
            }


        };
        Utils.getSessionIdAndUserIdFromUrl = function(){
            var obj = {'valid':false};
            if(deepLinkUrl != null) // deepLinkUrl  global value from js/lib/custom/handleUrl.js
            {

                cinc://start?session=;user=
                try {
                    var data = deepLinkUrl.replace("cinc://","").split("?");
                    if (data[0] == "start")
                    {
                        var values  = data[1].split("&");
                        var i= 0,l=values.length;
                        var value;
                        for(;i<l;i++)
                        {
                            value = values[i].split("=");
                            obj[value[0]] = value[1];
                        }

                        obj.valid = angular.isDefined(obj['session']) &&  angular.isDefined(obj['user']);
                    }
                }
                catch(err){
                    $log.debug("getSessionFromUrl "+err)
                    return obj;
                }
            }
            deepLinkUrl = null;
            return obj;
        };


    return Utils;
}]);