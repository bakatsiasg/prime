/**
 * Created by georgefloros on 4/24/15.
 */
'use strict';
services.service('RegisterOrUpgradeUser',['$rootScope','Constants','localStorageService','TalosValues','User','$translate','NetworkCodes',function ($rootScope, Constants,localStorageService,TalosValues,User,$translate,NetworkCodes) {

var RegisterOrUpgradeUser = {};

    var createFormErrors = function(){
        $rootScope.showLoading('Please Wait');

        $rootScope.formErrors = {'userNameError':false,'emailError':false,'nickNameError':false,'guestExist':false}
    }
    var validateFormErrors = function(networkCode){

        if(networkCode == NetworkCodes.NICKNAME_ALREADY_USED)
        {
            $rootScope.formErrors.nickNameError = true;
        }
        if(networkCode == NetworkCodes.GUEST_EXISTS)
        {
            $rootScope.formErrors.guestExist = true;
        }
        else if (networkCode == NetworkCodes.USER_PASSWORD_INVALID)
        {
            $rootScope.formErrors.passwordInvalid = true;
        }
        else if(networkCode == NetworkCodes.USERNAME_ALREADY_USED || networkCode == NetworkCodes.USER_USERNAME_INVALID)
        {
            $rootScope.formErrors.userNameError = true;
        }
        else if(networkCode == NetworkCodes.EMAIL_ALREADY_USED)
        {
            $rootScope.formErrors.emailError = true;
        }
        else{
            $rootScope.showErrorPopUp({msg:"NETWORK_ERROR_TRY_AGAIN",actions:[{'label':'ok',action:null}]});
        }
        $rootScope.hideLoading();
    }
    RegisterOrUpgradeUser.updateProfile = function(user,newPassword,imageData,callBack)
    {
        createFormErrors();
        User.updateMyProfile(user,newPassword)
            .then(
            function(data){
                if (imageData) {
                    User.uploadAvatar(imageData)
                        .then(
                        function(success){
                            delete $rootScope['formErrors'];
                            callBack(true);
                        },
                        function(error){
                            callBack(false);
                        }
                    );
                } else {
                    callBack(true);
                }
            },
            function(error){
                validateFormErrors(error.data.code);
            }
        );



    }
    RegisterOrUpgradeUser.registerOrUpgradeUser = function(user,callBack)
    {
        createFormErrors();
        User.upgradeOrRegister(user,$rootScope.hasAuthKey,function(success,networkCode){
            $rootScope.hideLoading();
            if(success) {
                delete $rootScope['formErrors'];
                callBack(true);
            }
            else{
                validateFormErrors(networkCode);
            }
        });
    }

 return RegisterOrUpgradeUser;
}])