// Ionic Starter App
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
var app = angular.module('etisalat', ['ionic', 'etisalat.controllers', 'etisalat.services','etisalat.directives','etisalat.talos-api','etisalat.filters','pascalprecht.translate','LocalStorageModule','ngCordova','ngPatternRestrict']);
var controllers = angular.module('etisalat.controllers', []);
var services  = angular.module('etisalat.services', []);
var directives  = angular.module('etisalat.directives', []);
var talosApi  = angular.module('etisalat.talos-api',[]);
var filters = angular.module('etisalat.filters',[]);

app.run(['$ionicPlatform','$cordovaKeyboard','$cordovaStatusbar','$cordovaSplashscreen',function($ionicPlatform,$cordovaKeyboard,$cordovaStatusbar,$cordovaSplashscreen) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    ionic.Platform.fullScreen();
    ionic.Platform.isFullScreen = true;
    if (window.cordova && window.cordova.plugins.Keyboard) {
      $cordovaKeyboard.hideAccessoryBar(false);
      $cordovaKeyboard.disableScroll(ionic.Platform.isIOS());
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      $cordovaStatusbar.hide();
    }

    setTimeout(function() {
      if(navigator.splashscreen) {
        $cordovaSplashscreen.hide();
      }
    }, 500);


  });
}])
    .config(['$stateProvider','$urlRouterProvider','localStorageServiceProvider','$translateProvider','$ionicConfigProvider','$sceProvider','$logProvider',function($stateProvider, $urlRouterProvider,localStorageServiceProvider,$translateProvider,$ionicConfigProvider,$sceProvider,$logProvider) {


      localStorageServiceProvider.setPrefix('CheckIn');
      $translateProvider.useLoader('translateLoader');
      $ionicConfigProvider.tabs.style("bottom");
      $ionicConfigProvider.navBar.alignTitle('center');
      $ionicConfigProvider.views.swipeBackEnabled(false);

      //if(ionic.Platform.isAndroid()) {
      //    //$ionicConfigProvider.scrolling.jsScrolling(false);
      //}
      $logProvider.debugEnabled(true);

      $sceProvider.enabled(false);
      $stateProvider
          .state('loading',
          {
              cache: false,
              url:"/",
              templateUrl:"templates/loading.html",
              controller:'LoadingCtrl'
          })

          .state('home',
          {
            cache: false,
            url:"/",
            templateUrl:"templates/home.html",
            controller:'HomeCtrl',
          })

      $urlRouterProvider.otherwise('/');


    }]).factory('translateLoader', ['$q', '$translate','Constants','$http', function ($q, $translate,Constants,$http) {
      return function (options) {
//            if (!options || (!angular.isString(options.prefix) || !angular.isString(options.suffix))) {
//                throw new Error('Couldn\'t load static files, no prefix or suffix specified!');
//            }
        var deferred = $q.defer();
        $http.get(Constants.TRANSLATE_URL+options.key+Constants.TRANSLATE_SUFFIX)
            .success(function (data) {
              deferred.resolve(data);
            }).error(function (data) {
              return null;
            });
        return deferred.promise;
      }
    }]);
