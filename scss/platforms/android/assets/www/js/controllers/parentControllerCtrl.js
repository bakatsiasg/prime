/**
 * Created by georgefloros on 3/30/15.
 */
'use strict';
controllers.controller('ParentCtrl',['$scope','$rootScope','$log','$cordovaKeyboard','TalosValues','$translate','EventService',
    '$ionicLoading','Events','$state','$ionicPlatform',
    function($scope,$rootScope,$log,$cordovaKeyboard,TalosValues,$translate,EventService,$ionicLoading,Events,$state,$ionicPlatform) {

        $ionicPlatform.registerBackButtonAction(function(event){
            //if( $rootScope.popUpIsOn) {
            //    event.preventDefault();
            //    event.stopPropagation();
            //}
            //else
            //{
            //    $ionicHistory.goBack(-1);
            //}
        },100,"");

        $ionicPlatform.on('resume', function() {

        });

        $rootScope.english = true;
     $rootScope.appHeight = window.innerHeight;


        $rootScope.closeKeyBoard = function(){

            if($cordovaKeyboard.isVisible())
            {
                $cordovaKeyboard.close();
            }

        };
        $rootScope.changeLanguage = function (langKey) {
                $rootScope.language = langKey;
                TalosValues.setLanguage(langKey);
                $translate.use(langKey);
                moment.locale(langKey);
                EventService.sendEvent(Events.LANGUAGE_SET);
              };

        $rootScope.changeLanguage = function (langKey) {
            $rootScope.language = langKey;
            TalosValues.setLanguage(langKey);
            $translate.use(langKey);
            moment.locale(langKey);
            EventService.sendEvent(Events.LANGUAGE_SET);
        };


        $rootScope.showLoading = function(message) {
            $rootScope.loadingIsOn = true;
            $ionicLoading.hide();
            $ionicLoading.show({
                template:$translate.instant(message)+'</br><object ng-if="loadingIsOn" height="20%" width="30%" type="image/svg+xml" data="img/loadingOrange.svg"> </object>'
            });
        };

        $rootScope.hideLoading = function(){
            $rootScope.loadingIsOn = false;
            $ionicLoading.hide();
        };
        $rootScope.showErrorPopUp = function  (params){
            $rootScope.showMainPopUp('templates/popUp/errorMessage.html',params)
        };
        $rootScope.showMainPopUp = function  (content,params,showClose){
            $rootScope.hideLoading();
            $rootScope.mainPopUpProps = {show:true,
            content:content,
            params:params,
                showClose:angular.isDefined(showClose)?showClose:true
            };
            $rootScope.popUpIsOn = true;

        };
    $rootScope.closeMainPopUp = function  (){
        delete $rootScope['mainPopUpProps'];
        $timeout(function(){
            delete $rootScope['mainPopUpProps'];
            $rootScope.popUpIsOn = false;
        },250);
    };

        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {



            if(toState.name == 'loading' && fromState.name != "")
            {
                event.preventDefault();
            }
            if((toState.name == 'home.profile'||toState.name == 'home.myCheckIn') && (Profile.profile == null ||Profile.isGuest))
            {
                event.preventDefault();
                $state.go('home.login');
            }

            if((toState.name == 'home.login'||toState.name == 'home.register') && (!Profile.isGuest))
            {
                event.preventDefault();
                $state.go('home.checkIn');
            }


            $rootScope.hideLoading();

        });


    $rootScope.goToState =  function(state,params){

        if(state == '0')
            return;
        $state.go(state,params);
        $log.debug('$state.go(state); = '+ state);
    };

    //$ionicConfig.transitions.views.fade = function(enteringEle, leavingEle, direction, shouldAnimate) {
    //    function setStyles(ele, opacity) {
    //        var css = {};
    //        css[ionic.CSS.TRANSITION_DURATION] = d.shouldAnimate ? '' : 0;
    //        css.opacity = opacity === 1 ? '' : opacity;
    //        ionic.DomUtil.cachedStyles(ele, css);
    //    }
    //    var d = {
    //        run: function(step) {
    //            setStyles(enteringEle,(step)); // starting at 98% prevents a flicker
    //            setStyles(leavingEle,(1-step));
    //        },
    //        shouldAnimate: shouldAnimate && (direction == 'forward' || direction == 'back')
    //    };
    //    return d;
    //};
    //    $ionicConfig.transitions.navBar.fade = function(enteringHeaderBar, leavingHeaderBar, direction, shouldAnimate) {
    //
    //        function setStyles(ctrl, opacity) {
    //            var css = {};
    //            css[ionic.CSS.TRANSITION_DURATION] = d.shouldAnimate ? '' : 0;
    //            css.opacity = opacity === 1 ? '' : opacity;
    //            ctrl.setCss('buttons-left', css);
    //            ctrl.setCss('buttons-right', css);
    //            ctrl.setCss('back-button', css);
    //
    //            css[ionic.CSS.TRANSFORM] = 'translate3d(' + 0 + 'px,0,0)';
    //            ctrl.setCss('back-text', css);
    //
    //            css[ionic.CSS.TRANSFORM] = 'translate3d(' + 0 + 'px,0,0)';
    //            ctrl.setCss('title', css);
    //        }
    //
    //        function enter(ctrlA, ctrlB, step) {
    //            if (!ctrlA || !ctrlB) return;
    //            setStyles(ctrlA, step);
    //        }
    //
    //        function leave(ctrlA, ctrlB, step) {
    //            if (!ctrlA || !ctrlB) return;
    //            setStyles(ctrlA, 1 - step);
    //        }
    //        var d = {
    //            run: function(step) {
    //                var enteringHeaderCtrl = enteringHeaderBar.controller();
    //                var leavingHeaderCtrl = leavingHeaderBar && leavingHeaderBar.controller();
    //                if (d.direction == 'back') {
    //                    leave(enteringHeaderCtrl, leavingHeaderCtrl, 1 - step);
    //                    enter(leavingHeaderCtrl, enteringHeaderCtrl, 1 - step);
    //                } else {
    //                    enter(enteringHeaderCtrl, leavingHeaderCtrl, step);
    //                    leave(leavingHeaderCtrl, enteringHeaderCtrl, step);
    //                }
    //            },
    //            direction: direction,
    //            shouldAnimate: shouldAnimate && (direction == 'forward' || direction == 'back')
    //        };
    //
    //        return d;
    //    };

}]);