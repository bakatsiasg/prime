/**
 * Created by georgefloros on 6/10/15.
 */
'use strict';
controllers.controller('LoadingCtrl',['$scope','$rootScope','$log','$timeout','$translate','Constants',
    function($scope,$rootScope,$log,$timeout,$translate,Constants) {


        var loadLanguages,loadingLanguageCurrentIndex,loadingLanguageLength;

        Constants.TRANSLATE_URL = "translate/";
        Constants.TRANSLATE_SUFFIX = ".json";

        loadLanguages = ["el","en"];
        loadingLanguageLength = loadLanguages.length -  1;


        loadingLanguageCurrentIndex = 0;
        $translate.use(loadLanguages[loadingLanguageCurrentIndex]);



        $rootScope.$on('$translateLoadingSuccess',function(){
            if(loadingLanguageCurrentIndex == loadingLanguageLength)
            {
                $log.debug('$translateLoadingSuccess');
                setTimeout(function(){
                    $rootScope.changeLanguage(loadLanguages[0]);
                    $rootScope.goToState("home");
                },0);
            }
            else
            {
                loadingLanguageCurrentIndex++;
                $translate.use(loadLanguages[loadingLanguageCurrentIndex]);
            }
        });
        $rootScope.$on('$translateLoadingError',function(){
            $log.debug("$translateLoadingError");
        });






    }]);