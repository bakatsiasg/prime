/**
 * Created by georgefloros on 4/9/15.
 */
'use strict';
directives.directive('enableScroll',['$rootScope','$log',function($rootScope,$log) {
    return {
        restrict: 'A',
        scope: true,
        link: function($scope, element, attrs) {

            //setTimeout(function(){
            if(angular.isDefined($rootScope.showTabBar) && $rootScope.showTabBar == true) {
            //    var myEl = angular.element(element[0].getElementsByClassName('scroll'))[0];
            //    var elHeight = element[0].offsetHeight - element[0].offsetTop;
            //    $log.debug("element offsetHeight = " + element[0].offsetHeight);
            //    $log.debug("myEl offsetHeight = " + myEl.offsetHeight);
            //    if (elHeight < myEl.offsetHeight) {
                    angular.element(element[0]).addClass('extra-margin-bottom');
                }
            //}
            //},100);

        }
    }
}]);