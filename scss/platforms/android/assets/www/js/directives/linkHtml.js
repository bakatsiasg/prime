/**
 * Created by georgefloros on 4/7/15.
 */
'use strict';
    directives.directive('linkHtml',['$compile',function($compile) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                scope.$watch(attrs.linkHtml, function(newValue, oldValue) {
                    element.html(newValue);
                    $compile(element.contents())(scope);
                });
            }
        }
    }]);