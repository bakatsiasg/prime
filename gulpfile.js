var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var paths = {
  sass: ['./scss/**/*.scss']
};
gulp.task('default', ['sass']);
gulp.task('sass', function(done) {
  gulp.src('./scss/ionic.app.scss')
      .pipe(sass({errLogToConsole: true}))
      .pipe(gulp.dest('./app/css/'))
      //.pipe(minifyCss({
      //  keepSpecialComments: 0
      //}))
      .pipe(rename({ extname: '.css' }))
      .pipe(gulp.dest('./app/css/'))
      .on('end', done);
});
gulp.task('watch', function() {
  gulp.watch(paths.sass, ['sass']);
});
gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
      .on('log', function(data) {
        gutil.log('bower', gutil.colors.cyan(data.id), data.message);
      });
});
gulp.task('git-check', function(done) {
  if (!sh.which('git')) {
    console.log(
        '  ' + gutil.colors.red('Git is not installed.'),
        '\n  Git, the version control system, is required to download Ionic.',
        '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
        '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});


// *********
// GULP Prepare WWW
// *********

var fs          = require('fs');
var del         = require('del');
var _           = require('lodash');
var watch       = require('gulp-watch');
var minifyJS    = require('gulp-uglify');
var vinylPaths  = require('vinyl-paths');
var inject      = require('gulp-inject');
var runSequence = require('run-sequence');
var replace     = require('gulp-replace-task');
var usemin = require('gulp-usemin');

var paths = {
  distFiles:  'www/**/**.*',
  gulpFile:   'gulpfile.js',
  distCSS:    'www/css',
  distJS:     'www/js',
  dist:       'www',

  src: {
    assetsFile: 'app/assets.json',
    index:      'app/index.html',
    fonts:      'app/fonts/**.*',
    imgs:       'app/img/**/**.*',
    templates:       'app/templates/**/**.*',
    translate:       'app/translate/**.*',
    dataXml:'app/data.xml',
    path:       'app/',
    css:        'app/css/**/**.*',
    js:         'app/js/**/**.*'
  },

  dest: {
    fonts: 'www/fonts',
    imgs:  'www/img',
    templates:'www/templates',
    translate:'www/translate/',

  }
};

gulp.task('dev:pipeline', ['devTasks']);

gulp.task('devTasks', function (callback) {
  runSequence(
      'clean',
      'sass',
      'copyApp',
      callback
  );
})

gulp.task('copyApp', function () {
  return gulp.src(paths.src.path+"**/*")
      .pipe(gulp.dest(paths.dist));
});


gulp.task('prod:pipeline', ['prodTasks']);

gulp.task('prodTasks', function (callback) {
  runSequence(
      'clean',
      'sass',
      'prod:processFonts',
      'prod:processImgs',
      'prod:processTemplates',
      'prod:processTranslate',
      'prod:processDataXml',

      'prod:usemin',
      callback
  );
})
gulp.task('clean', function () {
  var sources = [
    'www/**/**.*',
    'www/**/',
    '!www'
  ]

  return gulp.src(sources).pipe(vinylPaths(del));
});
gulp.task('prod:processFonts', function () {
  return gulp.src(paths.src.fonts)
      .pipe(gulp.dest(paths.dest.fonts));
});

gulp.task('prod:processImgs', function () {
  return gulp.src(paths.src.imgs)
      .pipe(gulp.dest(paths.dest.imgs));
});
gulp.task('prod:processTemplates', function () {
  return gulp.src(paths.src.templates)
      .pipe(gulp.dest(paths.dest.templates));
});
gulp.task('prod:processTranslate', function () {
  return gulp.src(paths.src.translate)
      .pipe(gulp.dest(paths.dest.translate));
});
gulp.task('prod:processDataXml', function () {
  return gulp.src(paths.src.dataXml)
      .pipe(gulp.dest(paths.dist));
});

gulp.task('prod:usemin', function () {
  return gulp.src(paths.src.index)
      .pipe(usemin({
        js: [minifyJS()],
        js1: [minifyJS()],
        css: [minifyCss(), 'concat']
        // in this case css will be only concatenated (like css: ['concat']).
      }))
      .pipe(gulp.dest(paths.dist));
});

