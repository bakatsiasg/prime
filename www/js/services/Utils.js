/**
 * Created by georgefloros on 4/20/15.
 */


services.service('Utils',['$rootScope','Constants','localStorageService','TalosValues','$translate','EventService','Profile','$ionicLoading','$ionicPopup',
          function ($rootScope, Constants,localStorageService,TalosValues,$translate,EventService,Profile, $ionicLoading, $ionicPopup) {
    var Utils = {};
    Utils.trackScreen = function(screenName)
    {
        //Uncomment for google analytics

        //try{
        //
        //    if($rootScope.isMobile)
        //        window.analytics.trackView(screenName);
        //    else
        //        ga('send',screenName);
        //}
        //catch(error)
        //{
        //    $log.log(error);
        //}
    }
    Utils.numberFormat = function(number, decimals, decPoint, thousandsSep) {
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep,
            dec = (typeof decPoint === 'undefined') ? '.' : decPoint,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k).toFixed(prec);
            };

        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }

        return s.join(dec);
    }

    Utils.getIntegerPart = function(f, decPoint) {
        var s = f.toString(),
            dec = (typeof decPoint === 'undefined') ? '.' : decPoint;
        s = s.split(dec);
        if (!s.length) s = '';
        else s = s[0];

        return s? s: '0';
    }

    Utils.getDecimalPart = function(f, n, decPoint) {
        var s = f.toString(),
            dec = (typeof decPoint === 'undefined') ? '.' : decPoint;
        s = s.split(dec);
        if (s.length < 2) s = '';
        else s = s[1];
        if (s.length > n) s = s.substring(0, n - 1);
        else s = s + Array(n - s.length + 1).join("0");

        return s;
    }

    Utils.getObjectFromList = function(l, p, v) {
        var res  = l.filter(function(o) {
            return o[p] == v;
        });
        return res? res : null;
    }

    Utils.getStatisticsValue = function(stats, statsType, itemTypeId, itemId, valueType) {
        var j;
        var value;
        for (j = 0; j < stats.length; j++) {
            if ((stats[j].statisticsType != statsType) || (stats[j].itemTypeId != itemTypeId) || (stats[j].itemId != itemId)) continue;
            switch (valueType) {
                case 'value':
                    value = ((stats[j].totalRatings)? stats[j].ratings / stats[j].totalRatings: 0);
                    break;
                case 'rated':
                    value = stats[j].ratingSubmitted;
                    break;
                case 'ratings':
                    value = stats[j].totalRatings;
                    break;
            }

            return value;
        }
    }

    Utils.parseDate = function(d) {
        var str = d.split(" ");
        var j;
        d = '';
        for (j = 0; j < str.length; j++) {
            if (j > 5) break;
            d += (j? " ": "") + str[j];
        }
        return d;
    }

    Utils.getDateDif = function(d1, d2) {
        var d = d1 - d2;

        //var days = Math.floor(d / (1000 * 60 * 60 * 24));
        //d -= days * (1000 * 60 * 60 * 24);

        var hours = Math.floor(d / (1000 * 60 * 60));
        d -= hours * (1000 * 60 * 60);

        var minutes = Math.floor(d / (1000 * 60));
        d -= minutes * (1000 * 60);

        var seconds = Math.floor(d / 1000);

        return hours + ":" + ((minutes < 10)? '0' + minutes: minutes);// + ":" + ((seconds < 10)? '0' + seconds: seconds);
    }
    Utils.getResourceUrl = function(itemId, itemTypeId, resourceTypeId) {
        return Constants.RESOURCE_URL + "?itemId=" + itemId + "&itemTypeId=" + itemTypeId + "&resourceTypeId=" + resourceTypeId + "&languageId=" + Constants['LANGUAGE_ID_'+TalosValues.getLanguage()];
    };

    Utils.convertToTalosDate = function(date) {
        return (date) ? $filter('date')(date, 'dd/MM/yyyy') : "";
    }

    Utils.convertFromTalosDate = function(date) {
        //console.log("convertFromTalosDate " + date)
        // using 'shortDate' format which is MM/dd/yyyy or dd/MM/yyyy, depending on locale
        return (date && date != null) ? new Date(moment(date,'DD/MM/YYYY')) : null;
    }



    Utils.getResource = function(item, resourceId) {
        try {
            var lang = TalosValues.getLanguage();
            return item.resources[resourceId+"_"+TalosValues.getLanguage()];
        } catch(error) {
            return "";
        }
    }

    Utils.hoursFromLastAction = function(lastActionKey) {
        var lastAction = localStorageService.get(lastActionKey);
        var lastCheckIn = lastAction == ""|| lastAction == null || lastAction == undefined ? Date.now():new Date(lastAction).getTime();
        var now = Date.now();
        var one_hour=1000*60*60
        return  Math.ceil((now - lastCheckIn)/(one_hour));
    }
    
    
   Utils.checkIsGuestAndLogin = function(msg,confirmCallBack,cancelCallBack)
          {
              if(Profile.profile.guest) {
                   var confirmPopup = $ionicPopup.confirm({
                         title: 'Consume Ice Cream',
                         template: 'Are you sure you want to eat this ice cream?'
                       });
                       confirmPopup.then(function(res) {
                         if(res) {
                           console.log('You are sure');
                            if(confirmCallBack)confirmCallBack();
                            $state.go("login");
                         } else {
                           console.log('You are not sure');
                           if(cancelCallBack)cancelCallBack();
                         }
                       });
                  return true;
              }
              else
              {
                  return false;
              }

          }
    
    
    Utils.closeLoadingPopUp =function()
    {
         $ionicLoading.hide();
    }
    
    
    Utils.loadingPopup = function (message)
    {
           $ionicLoading.show({
               template: 'Loading...'
           });
   }
    
    return Utils;
}]);