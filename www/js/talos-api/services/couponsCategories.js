/**
 * Created by georgefloros on 12/17/14.
 */
talosApi.factory('CouponCategories', [
        'TalosCore', 'TalosValues','ApplicationSettings','localStorageService','Constants','Resources',
        function( TalosCore, TalosValues,ApplicationSettings,localStorageService,Constants,Resources) {
            var CouponCategories = {};
            CouponCategories.couponCategories = null;
                CouponCategories.activeFilters = null;


            CouponCategories.get = function(categoryId, depth, withLeaves,callback) {
                if (CouponCategories.couponCategories) {
                    callback(CouponCategories.couponCategories);
                }
                else {
                    var appsettingValue = ApplicationSettings.getValue(Constants.APPLICATION_SETTING_COUPONS_CATEGORIES_VERSION);
                    var storeVersion = localStorageService.get(Constants.APPLICATION_SETTING_COUPONS_CATEGORIES_VERSION);

                    if (appsettingValue == storeVersion) {
                        CouponCategories.couponCategories = localStorageService.get('couponCategories');
                        callback(CouponCategories.couponCategories);
                    }
                    else {
                        TalosCore.http2Talos(
                            'GET', '/category/' + categoryId,
                            {'depth': depth, 'withleaves': withLeaves,'languageIds': Constants.LANGUAGE_IDS, 'resourceTypeIds': Constants.RESOURCE_TYPE_TITLE},
                            null).then(function (success) {

                                //Main category will be the "All" option in lists.
                                var mainCategory = success.data[0];
                                CouponCategories.couponCategories = success.data[0].children;
                                var resource = {};
                                var   resources = Array();
                                var   input = {};
                                var reffernceObjs={};
                                mainCategory.names = {};
                                mainCategory.leaves = new Array();
                                mainCategory.ids = new Array();
                                var i,j,l1,l2,langLength,langIndex;



                                var languages = Constants.LANGUAGE_IDS;

                                langLength = languages.length;
                                for(langIndex=0;langIndex<langLength;langIndex++)
                                {
                                    mainCategory.names[Constants['LANGUAGE_CODE_' + languages[langIndex]]] = "";

                                }

                                resource = {};
                                resource.itemId = mainCategory.itemIdForResourceLookup;
                                resource.itemTypeId = mainCategory.itemTypeIdForResourceLookup;
                                resource.resourceTypeId = Constants.RESOURCE_TYPE_NAME;
                                resources.push(resource);
                                reffernceObjs[mainCategory.itemIdForResourceLookup+"_"+mainCategory.itemTypeIdForResourceLookup+"_"+Constants.RESOURCE_TYPE_NAME] = mainCategory;

                                var item;
                                var leaf;
                                l1 = CouponCategories.couponCategories.length;

                                for(i=0;i<l1;i++)
                                {
                                    item =  CouponCategories.couponCategories[i];
                                    item.names = {};
                                    item.ids = new Array();

                                    for(langIndex=0;langIndex<langLength;langIndex++)
                                    {
                                        item.names[Constants['LANGUAGE_CODE_' + languages[langIndex]]] = "";
                                    }
                                    resource = {};
                                    resource.itemId = item.itemIdForResourceLookup;
                                    resource.itemTypeId = item.itemTypeIdForResourceLookup;
                                    resource.resourceTypeId = Constants.RESOURCE_TYPE_NAME;
                                    resources.push(resource);
                                    reffernceObjs[item.itemIdForResourceLookup+"_"+item.itemTypeIdForResourceLookup+"_"+Constants.RESOURCE_TYPE_NAME] = item;

                                    l2 = item.leaves.length;
                                    for(j=0;j<l2;j++)
                                    {
                                        leaf = item.leaves[j];
                                        leaf.id = leaf.itemId;//create id property to achive similarity /////
                                        leaf.names = {};
                                         for(langIndex=0;langIndex<langLength;langIndex++)
                                        {
                                            leaf.names[Constants['LANGUAGE_CODE_' + languages[langIndex]]] = "";
                                        }
                                        resource = {};
                                        resource.itemId = leaf.itemIdForResourceLookup;
                                        resource.itemTypeId = leaf.itemTypeIdForResourceLookup;
                                        resource.resourceTypeId = Constants.RESOURCE_TYPE_NAME;
                                        resources.push(resource);

                                        reffernceObjs[leaf.itemIdForResourceLookup+"_"+leaf.itemTypeIdForResourceLookup+"_"+Constants.RESOURCE_TYPE_NAME] = leaf;


                                        item.ids.push(leaf.id);
                                        mainCategory.ids.push(leaf.id);
                                        mainCategory.leaves.push(leaf);

                                    }

                                }

                                input.languageIds = languages;
                                input.resources = resources;
                                Resources.getMultipleResources(0, -1, input).then(function(data) {

                                    l1 = data.data.length;
                                    for(i=0;i<l1;i++)
                                    {
                                        resource = data.data[i];
                                        reffernceObjs[resource.itemId+"_"+resource.itemTypeTO.itemTypeId+"_"+Constants.RESOURCE_TYPE_NAME].names[Constants["LANGUAGE_CODE_" + resource.languageTO.languageId]] = resource.textResource;
                                    }

                                    try{delete mainCategory.children}catch(error){};
                                    CouponCategories.couponCategories.splice(0,0,mainCategory);

                                    reffernceObjs = null;//clear the unused refference;
                                    localStorageService.set('couponCategories', CouponCategories.couponCategories);
                                    localStorageService.set(Constants.APPLICATION_SETTING_COUPONS_CATEGORIES_VERSION, appsettingValue);
                                    callback(CouponCategories.couponCategories)
                                });
                            }
                            , function (error) {
                                callback(null);
                            });
                    }
                }
            }
            return CouponCategories;
            }]);
