'use strict';

/**
 * @ngdoc service
 * @name talos-api.Services.Achievements
 * @description Achievements Service
 */
talosApi.factory('Achievements', [
        'TalosCore', 'TalosValues', 
        function( TalosCore, TalosValues ) {
            var Achievements = {};
            Achievements.get = function(userId) {
                return TalosCore.requireSession(function() {
                    return TalosCore.http2Talos(
                        'GET', '/users/' + ((userId == null)? TalosValues.getUserId(): userId) + '/achievements',
                        null,
                        null);                    
                });
            };
            
            return Achievements;
        }
    ]
);
