'use strict';

/**
 * @ngdoc service
 * @name talos-api.Services.Polls
 * @description Polls Service
 */
talosApi.factory('Polls', [
        'TalosCore', 'TalosValues', 
        function( TalosCore, TalosValues ) {
            var Polls = {};
            Polls.start = function(categoryId) {
                return TalosCore.http2Talos(
                    'PUT', '/poll/start/' + categoryId,
                    null,
                    null);                    
            };
            
            return Polls;
        }
    ]
);
