'use strict';

talosApi.factory('TalosValues', [
        'localStorageService',
        function(localStorageService) {
        var values = {};
        var _userSessionId = '';
        var _userId = '';

        values.setUserSessionId = function (userSesionId) {
            _userSessionId = userSesionId;
        }
        values.getUserSessionId = function () {
            return _userSessionId;
        }

        values.setUserId = function (userId) {
            _userId = userId;
        }
        values.getUserId = function () {
            return _userId;
        }
        values.setAuthKey = function (authKey) {
            localStorageService.set('AuthKey', authKey);
        }
        values.getAuthKey = function () {
            return localStorageService.get('AuthKey');
        }

        values.setLanguage = function (lang) {
            localStorageService.set('lang', lang);
        }
        values.getLanguage = function () {
            var lang = localStorageService.get('lang');
            return lang ? lang: 'el';
        }

        /* clear data relative to user, called on logout */
        values.clearUserData = function () {
            values.setAuthKey("");
            values.setUserId("");
            values.setUserSessionId("");
        }

        return values;
    }]);
