/**
 * Created by georgefloros on 12/4/14.
 */
'use strict';
talosApi.service('InitService',
    ['$rootScope','TalosCore','TalosValues','localStorageService','ApplicationSettings','Profile','Constants','$timeout', 'HomePageCategories', 
     function($rootScope,TalosCore, TalosValues,localStorageService,ApplicationSettings,Profile,Constants,$timeout, HomePageCategories) {

      var InitService = {};

      InitService.init = function(scopeTypes,callback) {

                    Profile.get(function(profiledata) {

          if(profiledata){

            ApplicationSettings.get(scopeTypes,function(appSettingsdata)
            {
              if(appSettingsdata)
              {
                callback(2);
               
                HomePageCategories.get(Constants.CATEGORY_HOME,-1,true,function(data) {
                  if (data != null) {
                    callback(3)//last call;
                  }
                  else {
                    callback(false)
                  }
                });                       
              }
              else
              {
                callback(false);
              }

            });

          }
          else
          {
            callback(false);
          }


        }, true);

      }


      return InitService;
    }]);




