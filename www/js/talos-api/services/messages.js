'use strict';

//const MESSAGE_STATUS_READ = 1;
//const MESSAGE_STATUS_UNREAD = 0;

/**
 * @ngdoc service
 * @name talos-api.Services.Messages
 * @description Messages Service
 */
talosApi.factory('Messages', [
        'TalosCore', 'TalosValues',
        function( TalosCore, TalosValues ) {
            var Messages = {};
            Messages.get = function(from, to) {
                return TalosCore.requireSession(function() {
                    return TalosCore.http2Talos(
                        'GET', '/messages/' + TalosValues.getUserId() + '/messages',
                        { 'rangeFrom': from, 'rangeTo': to },
                        null);
                });
            };
            Messages.post = function(username, recipientId, content) {
                return TalosCore.requireSession(function() {
                    return TalosCore.http2Talos(
                        'POST', '/messages/' + TalosValues.getUserId(),
                        null,
                        {
                            'username': username,
                            'recipientId': recipientId,
                            'content': content
                        }
                    );
                });
            };
            Messages.delete = function(messageIds) {
                return TalosCore.requireSession(function() {
                    return TalosCore.http2Talos(
                        'DELETE', '/messages/' + TalosValues.getUserId() + '/messages',
                        null,
                        { 'messageIds' : messageIds }
                    );
                });
            };
            Messages.status = function(messageIds, status) {
                return TalosCore.requireSession(function() {
                    return TalosCore.http2Talos(
                        'POST', '/messages/' + TalosValues.getUserId() + '/messages/status',
                        null,
                        { 'messageIds' : messageIds, 'status' : status }
                    );
                });
            };
            return Messages;
        }
    ]
);
