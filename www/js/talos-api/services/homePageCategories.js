/**
 * Created by georgefloros on 12/19/14.
 */
talosApi.factory('HomePageCategories', [
        'TalosCore', 'TalosValues','ApplicationSettings','localStorageService','Constants','Resources',
        function( TalosCore, TalosValues,ApplicationSettings,localStorageService,Constants,Resources)
        {
            var HomePageCategories = {};
            HomePageCategories.homeCategories = null;

            HomePageCategories.setIsLoading= function(isLoading)
            {  
                if(!HomePageCategories.homeCategories)
                return;

                var l,i;
                l =  HomePageCategories.homeCategories.length;
                var item;
                for(i = 0 ;i<l;i++) 
                {
                    item = HomePageCategories.homeCategories[i];
                    item.isLoading = isLoading;
                }
            }


            HomePageCategories.get = function(categoryId, depth, withLeaves,callback) {
                if (HomePageCategories.homeCategories) {
                    callback(HomePageCategories.homeCategories);
                }
                else {
                    var appsettingValue = ApplicationSettings.getValue(Constants.APPLICATION_SETTING_HOME_CATEGORIES_VERSION);
                    var storeVersion = localStorageService.get(Constants.APPLICATION_SETTING_HOME_CATEGORIES_VERSION);
                    if (appsettingValue == storeVersion) {
                        HomePageCategories.homeCategories = localStorageService.get('homeCategories');
                        callback(HomePageCategories.homeCategories);
                    }
                    else {
                        TalosCore.http2Talos(
                            'GET', '/category/' + categoryId,
                            {'depth': depth, 'withleaves': withLeaves,'languageIds': Constants.LANGUAGE_IDS, 'resourceTypeIds': Constants.RESOURCE_TYPE_NAME},
                            null).then(function (success) {

                                //Main category will be the "All" option in lists.

                                HomePageCategories.homeCategories = success.data[0].children;
                                var resource = {};
                                var   resources = Array();
                                var   input = {};
                                var reffernceObjs={};

                                var l, i,langLength,langIndex;
                                l =  HomePageCategories.homeCategories.length;
                                var languages = Constants.LANGUAGE_IDS;
                                langLength = languages.length;

                                var item;
                                for(i = 0 ;i<l;i++)
                                {
                                    item = HomePageCategories.homeCategories[i];
                                    item.isLoading = true;
                                    item.action = '';
                                    item.allowGuest = 1;
                                    item.names = {};
                                    item.index = i;
                                    item.color = '#000000';
                                    //if(i == 0)
                                    //item.imageResourceType = Constants.RESOURCE_TYPE_IMAGE_FEATURED;
                                    //else
                                    item.imageResourceType = Constants.RESOURCE_TYPE_IMAGE;

                                    for(langIndex=0;langIndex<langLength;langIndex++)
                                    {
                                        item.names[Constants['LANGUAGE_CODE_' + languages[langIndex]]] = "";

                                    }
                                    resource = {};
                                    resource.itemId = item.itemIdForResourceLookup;
                                    resource.itemTypeId = item.itemTypeIdForResourceLookup;
                                    resource.resourceTypeId = Constants.RESOURCE_TYPE_NAME;
                                    resources.push(resource);

                                    resource = {};
                                    resource.itemId = item.itemIdForResourceLookup;
                                    resource.itemTypeId = item.itemTypeIdForResourceLookup;
                                    resource.resourceTypeId = Constants.RESOURCE_TYPE_ACTION;
                                    resources.push(resource);

                                    resource = {};
                                    resource.itemId = item.itemIdForResourceLookup;
                                    resource.itemTypeId = item.itemTypeIdForResourceLookup;
                                    resource.resourceTypeId = Constants.RESOURCE_TYPE_COLOR;
                                    resources.push(resource);

                                    resource = {};
                                    resource.itemId = item.itemIdForResourceLookup;
                                    resource.itemTypeId = item.itemTypeIdForResourceLookup;
                                    resource.resourceTypeId = Constants.RESOURCE_TYPE_ALLOW_GUEST;
                                    resources.push(resource);

                                    reffernceObjs[item.itemIdForResourceLookup + "_" + item.itemTypeIdForResourceLookup] = item;
                                }

                                input.languageIds = languages
                                input.resources = resources;

                                Resources.getMultipleResources(0, -1, input).then(function(data) {
                                    l = data.data.length
                                    for(i=0;i<l;i++)
                                    {
                                        resource = data.data[i]

                                        if(resource.resourceTypeTO.resourceTypeId == Constants.RESOURCE_TYPE_ACTION) {
                                            reffernceObjs[resource.itemId + "_" + resource.itemTypeTO.itemTypeId].action = resource.textResource;
                                        }
                                        else if(resource.resourceTypeTO.resourceTypeId == Constants.RESOURCE_TYPE_COLOR)
                                        {
                                            reffernceObjs[resource.itemId+"_"+resource.itemTypeTO.itemTypeId].color = resource.textResource;
                                        }
                                        else if(resource.resourceTypeTO.resourceTypeId == Constants.RESOURCE_TYPE_NAME)
                                        {
                                            reffernceObjs[resource.itemId+"_"+resource.itemTypeTO.itemTypeId].names[Constants["LANGUAGE_CODE_" + resource.languageTO.languageId]] = resource.textResource;
                                        }
                                        else if(resource.resourceTypeTO.resourceTypeId == Constants.RESOURCE_TYPE_ALLOW_GUEST)
                                        {
                                            reffernceObjs[resource.itemId+"_"+resource.itemTypeTO.itemTypeId].allowGuest = resource.textResource != "" ?parseInt(resource.textResource):1;
                                        }


                                    }
                                    reffernceObjs = null;//clear the unused refference;
                                    localStorageService.set('homeCategories', HomePageCategories.homeCategories);
                                    localStorageService.set(Constants.APPLICATION_SETTING_HOME_CATEGORIES_VERSION, appsettingValue);
                                    callback(HomePageCategories.homeCategories);
                                });
                            }
                            , function (error) {
                                callback(null);
                            });
                    }
                }
            }
            return HomePageCategories;
        }]);
