'use strict';

/**
 * @ngdoc service
 * @name talos-api.Services.Levels
 * @description Levels Service
 */
talosApi.factory('Levels', [
        'TalosCore', 
        function( TalosCore ) {
            var Levels = {};
            Levels.get = function(gameTypeId) {
                return TalosCore.http2Talos(
                    'GET', '/reference/gameType/' + gameTypeId + '/levels',
                    null,
                    null);                    
            };
            
            return Levels;
        }
    ]
);
