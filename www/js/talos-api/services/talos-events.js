'use strict';

/**
 * @ngdoc service
 * @name talos-api.Services.TalosEvents
 * @description Events TalosEvents
 */
talosApi.factory('TalosEvents', [
        'TalosCore', 'TalosValues', 
        function( TalosCore, TalosValues ) {
            var TalosEvents = {};
            TalosEvents.add = function(input) {
                return TalosCore.http2Talos(
                    'PUT', '/event/add',
                    null,
                    input);                    
            };
            
            return TalosEvents;
        }
    ]
);
