/**
 * @ngdoc service
 * @name talos-api.Services.User
 * @description User Factory
 */
talosApi.factory('User', [
        'TalosCore', 'TalosValues','$rootScope','Constants','$log',
        function( TalosCore, TalosValues ,$rootScope,Constants,$log) {
            var User = {};

            User.get = function (userId) {
                return  TalosCore.http2Talos('GET', '/users/' + userId, null, null)
            };
            User.myProfile = function () {
                return TalosCore.requireSession(function () {
                    return TalosCore.http2Talos('GET', '/users/' + TalosValues.getUserId(), null, null);
                });
            };
            User.updateMyProfile = function (userDto) {
                return TalosCore.requireSession(function () {
                    return TalosCore.http2Talos(
                        'POST', '/users/' + TalosValues.getUserId(),
                        null,
                        userDto);
                });
            };
            User.balances = function (unitTypeIds) {
                return TalosCore.requireSession(function () {
                    return TalosCore.http2Talos(
                        'GET', '/users/' + TalosValues.getUserId() + '/balance',
                        { unitTypeId: unitTypeIds },
                        null);
                });
            };
            User.upgrade = function (userObj) {             
                console.log("Upgrade Function");
                return TalosCore.requireSession(function () {
                    return TalosCore.http2Talos(
                        'POST', '/users/guest/upgrade',
                        null,
                        userObj);
                });
            };
            User.updateToken = function (params) {
                return TalosCore.requireSession(function () {
                    return TalosCore.http2Talos(
                        'POST', '/users/external/updateToken',
                        null,
                        params
                    );
                });
            };
            User.associateSession = function (authenticationSource, token, secret, externalUserID, externalAppId) {
                return TalosCore.requireSession(function () {
                    return TalosCore.http2Talos(
                        'POST', '/users/external/associateSession',
                        null,
                        {
                            authenticationSource : authenticationSource,
                            authToken : token,
                            authTokenSecret : secret,
                            externalApplicationId : externalAppId,
                            userId : externalUserID
                        });
                });
            };
            User.uploadAvatar = function (imageBase64) {
                return TalosCore.requireSession(function () {
                    return TalosCore.http2Talos(
                        'POST', '/users/' + TalosValues.getUserId() + '/avatars',
                        null,
                        {
                            imageData : imageBase64
                        });
                });
            };

            User.resetPasswordRequest = function (email) {
                return TalosCore.http2Talos('GET', '/users/resetPassword/' + email, null, null);
            }

            User.getFriends = function (from, to) {
                return TalosCore.requireSession(function () {
                    return TalosCore.http2Talos(
                        'GET', '/users/' + TalosValues.getUserId() + '/friends',
                        {'rangeFrom': from, 'rangeTo': to},
                        null);
                });
            };

            User.stats = function(input) {
                return TalosCore.requireSession(function() {
                    return TalosCore.http2Talos(
                        'POST', '/users/' + TalosValues.getUserId() + '/stats',
                        null,
                        input);
                });
            }            

            User.level = function (userId) {
                return TalosCore.requireSession(function () {
                    return TalosCore.http2Talos(
                        'GET', '/users/' + userId + '/level',
                        null,
                        null);
                });
            };

            User.savemetadata = function (keyvalues)
            {
                console.log("User.savemetadata");
              return TalosCore.requireSession(function () {
                return TalosCore.http2Talos(
                  'POST', '/users/saveMetadata',
                    null,keyvalues);
              });
            }
            return User;
        }
    ]);
