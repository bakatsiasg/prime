'use strict';

/**
 * @ngdoc service
 * @name talos-api.Services.Coupons
 * @description Coupons Service
 */
talosApi.factory('Coupons', [
        'TalosCore', 'TalosValues', 'Constants',
        function( TalosCore, TalosValues, Constants ) {
            var Coupons = {};
            Coupons.get = function(from, to, brandProductIds, includeStatistics, brandId, tagIds, brandTagIds, brandProductTypeId, brandProductStatusId, withDateValidation) {
                return TalosCore.requireSession(function() {
                    return TalosCore.http2Talos(
                        'GET', '/brands/' + TalosValues.getUserId() + '/products',
                        {'rangeFrom': from, 'rangeTo': to, 'brandProductIds': brandProductIds, 'includeStatistics': includeStatistics, 'brandId': brandId, 'tagIds': tagIds, 'brandTagIds': brandTagIds, 'brandProductTypeId' : brandProductTypeId, 'brandProductStatusId': brandProductStatusId, 'withDateValidation': withDateValidation},
                        null);                                                                                                                      
                });
            };
            
            Coupons.getMyCoupons = function(from, to, brandProductCouponIds, brandProductCouponStatusIds, includeStatistics, includeQRCode) {
                return TalosCore.requireSession(function() {
                    return TalosCore.http2Talos(
                        'GET', '/users/' + TalosValues.getUserId() + '/coupons',
                        {'rangeFrom': from, 'rangeTo': to, 'brandProductCouponIds': brandProductCouponIds, 'brandProductCouponStatusIds': brandProductCouponStatusIds, 'includeStatistics': includeStatistics, 'friendsOnly': false, 'includeQRCode': includeQRCode},
                        null);                                                            
                });
            };            
            
            Coupons.purchase = function(input) {
                return TalosCore.requireSession(function() {
                    return TalosCore.http2Talos(
                        'POST', '/users/purchase/' + TalosValues.getUserId(),
                        null,
                        input);                                    
                });
            }
                                    
            return Coupons;
        }
    ]
);
