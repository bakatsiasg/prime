'use strict';

/**
 * @ngdoc service
 * @name talos-api.Services.Tournaments
 * @description Tournaments Service
 */
talosApi.factory('Tournaments', [
        'TalosCore', 'TalosValues', 
        function( TalosCore, TalosValues ) {
            var Tournaments = {};
            var tournaments;
            
            Tournaments.get = function(from, to, currentlyActive, tournamentType, gameType, callback) {
                 TalosCore.http2Talos(
                    'GET', '/tournaments',
                    {'rangeFrom': from, 'rangeTo': to, 'currentlyActive': currentlyActive, 'tournamentType': tournamentType, 'gameType': gameType},
                    null).then(function(success){
                         
                          tournaments = success.data;
                          callback(tournaments);
                    }, function (error){
                        callback(null);
                    });                
            };
            
            Tournaments.getTournament = function(id, callback){
                
                 var i,j;
                 var l = tournaments.length;
                 for (i=0; i<l; i++){
                     if (id==tournaments[i].itemIdForResourceLookup){
                         callback(tournaments[i]); 
                     }
                 }
            }
            
            Tournaments.getUserParticipation = function(tournamentIds) {
                return TalosCore.requireSession(function() {
                    return TalosCore.http2Talos(
                        'POST', '/tournaments/status/' + TalosValues.getUserId(),
                        null,
                        tournamentIds);                    
                });
            };            
            
            Tournaments.join = function(tournamentId) {
                return TalosCore.requireSession(function() {
                    return TalosCore.http2Talos(
                        'POST', '/tournaments/' + tournamentId,
                        null,
                        {});                    
                });
            };                        
            
            Tournaments.getUserRanking = function(tournamentId) {
                return TalosCore.requireSession(function() {
                    return TalosCore.http2Talos(
                        'GET', '/tournaments/' + tournamentId + '/ranking/' + TalosValues.getUserId(),
                        null,
                        null);                    
                });
            };      
            
            Tournaments.getRankings = function(from, to, tournamentId) {
                return TalosCore.requireSession(function() {
                    return TalosCore.http2Talos(
                        'GET', '/tournaments/' + tournamentId + '/ranking',
                        {'rangeFrom': from, 'rangeTo': to},
                        null);                    
                });
            };              
            
            return Tournaments;
        }
    ]
);
