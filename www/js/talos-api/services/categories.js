'use strict';

/**
 * @ngdoc service
 * @name talos-api.Services.Comments
 * @description Comments Service
 */
talosApi.factory('Categories', [
        'TalosCore', 'TalosValues', 
        function( TalosCore, TalosValues ) {
            var Categories = {};
            Categories.get = function(categoryId, depth, withLeaves) {
                return TalosCore.http2Talos(
                    'GET', '/category/' + categoryId,
                    {'depth': depth, 'withleaves': withLeaves},
                    null);                    
            };
            
            return Categories;
        }
    ]
);
