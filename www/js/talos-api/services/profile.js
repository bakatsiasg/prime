'use strict';

/**
 * @ngdoc service
 * @name talos-api.Services.Profile
 * @description Profile Service that keep the current logged in user's Profile (that get it from User Service)
 */
talosApi.factory('Profile', [

        'User', '$q', '$rootScope', 'Constants', 'TalosValues',"$log",'EventService','Events',
        function ( User, $q, $rootScope, Constants, TalosValues,$log,EventService,Events) {


          var Profile = {};
          

          Profile.get = function(callback, forced) {
               $log.log("Profile.get");
               if (Profile.profile != undefined && Profile.profile!=null && !forced  )
               {
                     callback(Profile.profile);
               }
               else{
                      User.myProfile()
                      .then(function(data) {
                        Profile.profile = data.data;
                        Profile.isGuest = Profile.profile.guest;
                        Profile.isFacebookUser = (Profile.profile.extAppSourceIds) && (Profile.profile.extAppSourceIds.indexOf(Constants.EXTERNAL_SYSTEM_FACEBOOK)!=-1);
                        Profile.isTwitterUser = (Profile.profile.extAppSourceIds) && (Profile.profile.extAppSourceIds.indexOf(Constants.EXTERNAL_SYSTEM_TWITTER)!=-1);
                        EventService.sendEvent(Events.PROFILE_RECEIVED,Profile.profile);
                        callback(Profile.profile);
                      }
                    ,function(error)
                      {
                        Profile.clear();
                        callback(Profile.profile);
                      });
                    
                }
            }

            Profile.clear = function() {
                 Profile.profile= null;
                 Profile.isGuest = true;
                 Profile.isFacebookUser = false;
                 Profile.isTwitterUser = false;
            }

            Profile.imageUpdated = function() {
                 Profile.profile.picUrl = Constants.USER_AVATAR_IMAGE_URL.replace("$USERID", TalosValues.getUserId());
//                notifyObservers();
            };

            var scope = $rootScope.$new();
            scope.$on('NEW_USER', function() {
                //console.log('Profile on NEW_USER');
                 Profile.clear();    
                    Profile.get(function(){
                    }, true);
            })

//            scope.$on('PROFILE_UPDATED', function() {
//                //console.log('Profile on PROFILE_UPDATED');
//                _profile = null;
//                Profile.get(function(){
//                    notifyObservers();
//                }, false);
//            })


             Profile.clear();

            return Profile;
        }
    ]);