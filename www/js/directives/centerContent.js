/**
 * Created by georgefloros on 4/9/15.
 */
'use strict';
directives.directive('centerContent',['$compile','$log',function($compile,$log) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
             $log.debug("element width = "+element[0].offsetWidth);
            $log.debug("window.innerWidth = "+window.innerWidth);

            if(window.innerWidth > 375) // iphone 6
            {

                //max-width: 375px;
                //max-height: 667px;
                //margin: auto;

                //var margin = (window.innerWidth - 355) /2;
                //var css ={'margin-right':margin+'px','margin-left':margin+'px'};

                var css ={'max-width':375+'px','max-height':667+'px','margin':'auto'};
                ionic.DomUtil.cachedStyles(element[0], css);
            }

        }
    }
}]);