/**
 * Created by georgefloros on 4/9/15.
 */
'use strict';
directives.directive('pageBreak',['$compile',function($compile) {
    return {
        restrict: 'E',
        link: function(scope, element, attrs) {

            var num = angular.isDefined(attrs.breaks)?parseInt(attrs.breaks) : 2;
            var i=0 ;
            for(;i < num;i++)
            {
                var b = angular.element('<br/>');
                element[0].appendChild(b[0]);
            }
        }

        }
    }]);