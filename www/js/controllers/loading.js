/**
 * Created by georgefloros on 5/4/15.
 */

'use strict';
controllers.controller('LoadingCtrl',['$scope','$rootScope','$state', '$document', 'InitService', 'EventService','Events', '$log', '$translate', 'Constants', 'Profile', 'User', 'localStorageService', 'ApplicationSettings','GlobalData','Utils',
                       function($scope, $rootScope, $state, $document, InitService, EventService, Events, $log, $translate, Constants, Profile, User, localStorageService, ApplicationSettings, GlobalData, Utils) {
   
          console.log("Hello Loading Controller!");
            //$scope.isGuest = true;
      
  	     function showLoadingMessage()
         {
             $rootScope.showLoadingMessage = true;
             $rootScope.currentLoading = 1;
             $rootScope.loadingTotal = 3;
             $rootScope.initNetworkError = false;
          }
            
               
          function showNetworkErrorMessage()
          {
            $rootScope.initNetworkError = true;
            $rootScope.showLoadingMessage = false;
            console.log('Error network message');
          }
  
     
          $rootScope.initService = function ()
          {
            console.log("Loading Controller!");
            showLoadingMessage();
            InitService.init(Constants.APPLICATION_SETTING_SCOPE_TYPE_GAMETYPE,function(data) {
              if(data){
                $rootScope.currentLoading = data;

                if($rootScope.currentLoading == $rootScope.loadingTotal) {
                  EventService.sendEvent(Events.TRANSLATE_CONTROLLER_CONFIG);
                  EventService.sendEvent(Events.LOAD_HOME_ITEMS);

                  var user = GlobalData.user;
                  
                  var firstStartUp = localStorageService.get('firstStartUp');
                  console.log(Profile.profile.guest);
                  if(!Profile.profile.guest)
                  {
                    $state.go('tab.home');
                    $rootScope.fullscreen = true;
                  }
                  else{
//                    checkForDeepLink(); 
                      $state.go('login');
                  }
                  localStorageService.set('firstStartUp',true)
                  $rootScope.showSplash = false;
             
                }
              }
              else
              {
                showNetworkErrorMessage();
              }});
          }
          
                         
              

          
//          function checkForDeepLink(){
//            if(deepLinkUrl!='')
//            {
//              $rootScope.handleAction(deepLinkUrl);
//              deepLinkUrl='';
//            }
//          }
       


//            $rootScope.$on('$translateLoadingSuccess', function(){
//                
//                  $log.log("$translateLoadingSuccess");
//                    
//      
//                 if($rootScope.languagesArrCurrentIndex >= $rootScope.languagesArrLength)
//                {
//                    
//                     delete $rootScope['languagesArr'];
//                     delete $rootScope['languagesArrLength'];
//                     delete $rootScope['languagesArrCurrentIndex'];
// 
//                      $rootScope.initService();
//                 }
//                 else{
//                      $translate.use($rootScope.languagesArr[++$rootScope.languagesArrCurrentIndex]);
//                 }
// 
//                 
//             });

   
                
            $rootScope.$on(Events.APP_CONFIG_LOADED,function(){
                    $log.log("APP_CONFIG_LOADED");
                    $rootScope.initService();   
            });        
            $rootScope.$on('$translateLoadingError',function(){
                    $log.log("$translateLoadingError");
                    $rootScope.initService();   
            });
            
            EventService.sendEvent(Events.LOAD_APP_CONFIG,{url:'Prime.xml'})
    
    }]);

