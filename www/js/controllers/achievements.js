'use strict';
controllers.controller('AchievementsCtrl',['$scope','$rootScope','$location','EventService','Events',
    '$state','Constants','Utils','TalosValues','localStorageService',function($scope,$rootScope,$location,EventService,Events,$state,
        Constants,Utils, TalosValues,localStorageService) {


    $scope.achievements = [{
    id:0,
    title:'Ασφαλειες Υγειας',
    description:'Ολοκλήρωσε 100 ασφαλειες υγείας',
    image:'img/achievment1.png',
    points:'1000',
    ratio:'',
    aquired: true,
    currentPoints: 100,
    totalPoints:200

  }
  , {
    id:1,
    title:'Ασφάλειες Σκάφων',
    description:'Ολοκλήρωσε 200 ασφάλειες σκαφών',
    image:'img/achievment2.png',
    points:'2000',
    ratio:'75/200',
    aquired: false,
    currentPoints: 100,
    totalPoints:200
  }, {
    id:2,
    title:'Ασφάλειες Υγείας',
    description:'Ολοκλήρωσε 100 ασφάλειες υγείας',
    image:'img/achievment3.png',
    points:'1000',
    ratio:'',
    aquired: true,
    currentPoints: 100,
    totalPoints:200
  }];

       
  
  $scope.getProgress = function(id) {
   var percent =   $scope.achievements[id].currentPoints /$scope.achievements[id].totalPoints;
   var width = percent * 150;
   var style = width + 'px';
   console.log(style);
   return style;


    };

    // $scope.style1 = '150px';
}]);