/**
 * Created by georgefloros on 5/4/15.
 */
'use strict';

controllers.controller('RegisterCtrl', ['$scope','$rootScope','$ionicViewService','$translate','Events','EventService','User','$state','$filter','Utils','TalosValues', 'States','$ionicLoading','$ionicPopup','Profile','$ionicSideMenuDelegate',
        function($scope,$rootScope,$ionicViewService,$translate,Events,EventService,User,$state,$filter,Utils,TalosValues, States,$ionicLoading, $ionicPopup, Profile, $ionicSideMenuDelegate) {
    
         console.log("Hello Register Controller!");
         
         States.get('8A8A8081-16A7-7A31-0116-A77A6C640001', true).then(function(data) {     
            $scope.statesArray = data.data;
            console.log("Success", $scope.statesArray ); 
            }, function(data) {
                console.log("Error: No data was found");
        });
        
           // $scope.changeLanguage();
           // $scope.changeLanguage();
         
         /* usefull function to convert date in angular and talos system */
         var convertToTalosDate = function(date) {
                return (date) ? $filter('date')(date, 'dd/MM/yyyy') : "";
         }
         
         var convertFromTalosDate = function(date) {
                //console.log("convertFromTalosDate " + date)
                // using 'shortDate' format which is MM/dd/yyyy or dd/MM/yyyy, depending on locale
              return (date && date != null) ? new Date(moment(date,'DD/MM/YYYY')) : null;
         }

         States.setLocaleNames(TalosValues.getLanguage());
         $scope.states = States.states;
         
         $scope.today = $filter('date')(new Date(), 'yyyy-MM-dd');


         if (angular.isUndefined($scope.user)) {
               $scope.formInvalid = false;
               // used to disable buttons on click
               $scope.disableButtons = false;
               $scope.retypepassword ="";
               
               $scope.user2 = {
                   "retypepassword":""
               };
               
               $scope.user = {
                    "username":"",
                    "password":"",
                    "nickname":"",
                    "email":"",
                    "birthdate":"",
                    "nationalityIsoCode":TalosValues.getLanguage(),
                    "profilePictureUrl":"",
                    "preferedLanguageCode":TalosValues.getLanguage(),
                    "customFields": {
                        "name" : "",
                        "surname" : "",
                        "nameday": "",
                        "Nomos":"",
                        "gender":""
                    }
                };
            }


          $scope.quickRegistration = function () {
                
                console.log("QuickRegistration function");
                
                                
                if (this.registration_form.$valid) {
                    
//                    $ionicLoading.show({
//                                template: 'Loading...'
//                                    
//                    });  

                    // further validation:
                    if ($scope.user.password != $scope.user2.retypepassword) {
                        $scope.formInvalid = true;
                        $scope.passwordMismatch = true;
                    }
                    else {
                        $scope.formInvalid = false;
                        $scope.passwordMismatch = false;
                        $scope.accept();
                    }
                    
                }
                else {
                       $scope.formInvalid = true;
                          var alertPopup = $ionicPopup.alert({
                              title: 'Error!',
                              template: 'Παρακαλώ συμπληρώστε όλα τα απαραίτητα πεδία'
                       });
                }

        }
 
 
            $scope.accept = function ()
            {
                
              
                $scope.disableButtons = true;
                $scope.processUser();
               
                $ionicLoading.show({
                                template: 'Register, please wait...'       
                                }); 
                                
                User.upgrade($scope.user)
                    .success(function (data)
                    {
                        /* trigger event to update user relative data (like UserDTO in Profile Service) */
                      Profile.get(function(){
                               $ionicLoading.hide();
                          
                              var alertPopup = $ionicPopup.alert({
                                         title: 'Congrats!',
                                         template: 'Εγγραφήκατε επιτυχώς στην εφαρμογή! Ακολουθήστε της οδηγίες στο ημέιλ που μας έστειλες για να συνεχίσεις.'
                                });
                      
                         alertPopup.then(function(res) {
 
                              $rootScope.goToState('login');
                              $scope.back();
                              $ionicSideMenuDelegate.toggleLeft();
                              $scope.user.email="";
                              $scope.user.username="";
                              $scope.user.password="";
                              $scope.user2.retypepassword="";
                              $scope.user.customFields.gender="";
                              $scope.user.birthdate="";
                              $scope.user.customFields.Nomos="";

                            });
                     
                      
                      },true);
                 
                      
           
                    })
                    .error(function (data)
                    {
                        $scope.disableButtons = false;
                        $scope.restoreUser();

                        var message = 'Registration_Failure';
                        var redirState = 'registration.basic';
                        if (data.code && data.errorId) {
                            switch (data.code) {
                                case 1015:
                                    message = 'Registration_BirthdateIncorrect';
                                    redirState = 'registration.step2';
                                    break;
                                case 1014:
                                case 1050:
                                    message = 'Registration_UsernameAlreadyUsed';
                                    redirState = 'registration.basic';
                                    break;
                                case 1020:
                                    message = 'Registration_EmailAlreadyUsed';
                                    redirState = 'registration.basic';
                                    break;
                                case 1063:
                                    message = 'Registration_MoreCardAlreadyUsed';
                                    redirState = 'registration.step2';
                                    break;
                            }
                        }
                        
                       $ionicLoading.hide();
                       
                       
                      //LEIPEI TO MESSAGE
                      var alertPopup = $ionicPopup.alert({
                                         title: 'Failure!',
                                         template: 'Δεν καταφέρατε να εγγραφείτε στο σύστημα!'
                       });
                       
//                        ngDialog.open({
//                                          template: 'templates/dialogs/registration_failure.html',
//                                          className: 'ngdialog-theme-mall',
//                                          data: angular.toJson({
//                                                                   'body': $filter('translate')(message),
//                                                                   'status': 'error'
//                                                               }),
//                                          showClose: false,
//                                          closeByEscape: false,
//                                          controller: ['$scope', '$state', function($scope, $state) {
//                                              $scope.ok = function() {
//                                                  $scope.closeThisDialog();
//                                                  if (redirState) {
//                                                      $state.go(redirState);
//                                                  }
//                                              }
//                                          }]
//                                      }
//
//                        );
                    });
            }

            /**
             * Prepare $scope.user object for POSTing to server
             */
            $scope.processUser = function () {
                // set nickname == username
                $scope.user.nickname = $scope.user.username;

                $scope.user.customUserProfileFieldList = [];

                // parse dates and format to dd/MM/yyyy
                if ($scope.user.birthdate) {
                    $scope.user.birthdate = $filter('date')($scope.user.birthdate, 'dd/mm/yyyy');
                    $scope.user.customUserProfileFieldList.push({"key": "Personal.birthday", "value": $scope.user.birthdate});
                }
                if ($scope.user.customFields && $scope.user.customFields.nameday) {
                    $scope.user.customFields.nameday = $filter('date')($scope.user.customFields.nameday, 'dd/mm/yyyy');
                }

                // transfer customFields in appropriate format for JSON request
                angular.forEach($scope.user.customFields, function (val, key)
                {
                    if (val) {
                        /* if the key ends with day ( 3 ==> length of "day") */
                        if (key.indexOf("day") == key.length - 3) {
                            val = convertToTalosDate(val);
                        }

                        $scope.user.customUserProfileFieldList.push({"key": "Personal." + key, "value": val});
                    }
                });

                // remove redundant property before POST-ing
                delete $scope.user.customFields;

                if (angular.isDefined($scope.user.moreCard) && ($scope.user.moreCard != '')) {
                    $scope.user.customUserProfileFieldList.push({"key": 'Personal.idNumber', "value": $scope.user.moreCard});
                    delete $scope.user.moreCard;
                }
                try{delete $scope.user.user}catch(error){};
            }

            $scope.restoreUser = function () {
                // restore customFields
                $scope.user.customFields = {};
                angular.forEach($scope.user.customUserProfileFieldList, function (val, index)
                {
                    if (val.key != 'Personal.idNumber') {
                        if (val.key.indexOf("day") > -1) {
                            // does not correctly take into account the locale!!!
                            $scope.user.customFields[val.key.replace('Personal.', '')] = convertFromTalosDate(val.value);
                        }
                        else {
                            $scope.user.customFields[val.key.replace('Personal.', '')] = val.value.id;
                        }
                    }
                    else {
                        $scope.user.moreCard = val.value;
                    }
                });

              try{delete $scope.user.customUserProfileFieldList}catch(error){};//good clean up
            }

            $scope.decline = function() {
                // show modal
                ngDialog.open({
                                  template: 'templates/dialogs/t_and_c_declined.html',
                                  className: 'ngdialog-theme-mall',
                                  controller: ['$scope', '$state', function($scope, $state) {
                                      $scope.ok = function() {
                                          $scope.closeThisDialog();
                                          $state.go('login');
                                      }
                                  }]
                              });
            }

            $scope.showView = true;

            $scope.childrenVariableNames = [];
            $scope.addChild = function(){
                //console.log("addChild");
                var l = $scope.childrenVariableNames.length;
                if (l<10) {
                    $scope.childrenVariableNames.push( "child" + (l+1) +"Birthday" );
                    $scope.user.customFields["child" + (l+1) +"Birthday"] = new Date();
                }
            }

            $scope.getTermsUrl = function()
            {
                return Constants.TERMS_URL.replace("$lang",TalosValues.getLanguage());
            }
    
    
            $scope.back = function(){
              $state.go('login');
            };
    
            $scope.back1 = function(){
              $state.go('tab.home');
            };
    
    
    }]);

