'use strict';
'use strict';

controllers.controller('CouponsCtrl',['$scope','$rootScope','$state','$filter', 'Metadata', 'ItemStatistics','Constants', 'TalosValues', 'Coupons', 'Resources', 'TalosEvents','ApplicationSettings', '$stateParams', '$translate', '$log', '$location','CouponCategories', 'Utils', '$anchorScroll','$ionicScrollDelegate','Brands',
                        function ($scope, $rootScope, $state, $filter, Metadata,  ItemStatistics, Constants, TalosValues, Coupons, Resources, TalosEvents, ApplicationSettings, $stateParams , $translate, $log, $location, CouponCategories, Utils, $anchorScroll, $ionicScrollDelegate, Brands) {

       
        $scope.coupons=[];
    
        $scope.couponDetails = function(coupon) {
                $state.go('tab.giftDetail', {'couponID': coupon.brandProductId});
                console.log(coupon.brandProductId);
        }
        
        console.log('CouponsCtrl');
        
        $scope.pressed = "Active";
   
	$scope.setGroupFilter = function(group) {
            $scope.pressed = group;
            };
            
            
        // $scope.setAllFilter = function(){   
        //         $scope.pressed = 'all';
        // };
           
        $scope.listIsLoading = false;
        $scope.itemId = '';
        //$scope.myVariable = 'Hello, world!';
        $scope.itemTypeId = '';
        $scope.bookmarkListType = 'wishlist';
        $scope.name = '';
        $scope.shareTitle = $filter('translate')('ShareEmail_Title_Coupon');
        $scope.favBody = $filter('translate')('Wishlist_Add_ConfirmationBody');
        $scope.favSuccessMsg = $filter('translate')('Wishlist_Added');
        $scope.favErrorMsg = $filter('translate')('Wishlist_NotAdded');
       
        $scope.showView = false;
        $scope.isLoadingCoupons = false;    
        
        var _allItemsLoaded = false;
       
        var varlen = true;       
        $scope.couponsSize = -1;


         /* coupon state */
        $scope.coupon = {};
        $scope.couponStatusId = '';
       
        var brandFilter = null;

        /* Coupon status */
        $scope.allCouponStatus = Array(
        Constants.COUPON_STATUS_PURCHASED,
        Constants.COUPON_STATUS_EXPIRED,
        Constants.COUPON_STATUS_EXPIRED_CLEARED,
        Constants.COUPON_STATUS_EXPIRED_CLEARED_FAILED,
        Constants.COUPON_STATUS_REDEEMED,
        Constants.COUPON_STATUS_REDEEMED_CLEARED,
        Constants.COUPON_STATUS_REDEEMED_CLEARED_FAILED
        );

          
        $scope.activeCouponStatus = Constants.COUPON_STATUS_PURCHASED;

        $scope.consumedCouponStatus = Array(
          Constants.COUPON_STATUS_REDEEMED,
          Constants.COUPON_STATUS_REDEEMED_CLEARED,
          Constants.COUPON_STATUS_REDEEMED_CLEARED_FAILED
        );

        $scope.expiredCouponStatus = Array(
          Constants.COUPON_STATUS_EXPIRED,
          Constants.COUPON_STATUS_EXPIRED_CLEARED,
          Constants.COUPON_STATUS_EXPIRED_CLEARED_FAILED
        );

        $scope.couponStatus = $scope.allCouponStatus;
        $scope.couponStatusText = '';
        $scope.couponStatusMode = 'all';

        $scope.showCouponFilters = ($state.current.name == 'tab.gifts');
         
        $scope.couponFilterId = '';
        
        $scope.couponFilterName = '';
        $scope.couponFilterCategory = '';
                
        $scope.brands    =  [];
        
      $scope.showDetailsButton = ($state.current.name == 'tab.gifts' && !$scope.isMobile);


      $scope.getCouponStatusText = function() {
             return ($scope.couponStatusText.length? $scope.couponStatusText: $translate.instant('CouponStatus_All'));
      }

      $scope.getCurrentCouponFilter = function() {
            return ($scope.couponFilterId.length || $scope.brandTagFilter.length? $scope.couponFilterName: 'Filter BY:');
      }

      $scope.getCouponStatusText = function() {
             return ($scope.couponStatusText.length? $scope.couponStatusText: $translate.instant('CouponStatus_All'));
      }

      $scope.getCurrentCouponFilter = function() {
            return ($scope.couponFilterId.length || $scope.brandTagFilter.length? $scope.couponFilterName: 'Filter BY:');
      }



      $scope.viewTitle = function() {
                var title = ($state.current.name == 'tab.gifts')? 'Coupons_Title': 'MyCoupons_Title';
                if (($state.current.name != 'coupon'))
                  title = $filter('translate')(title);
                return title;
      }
      
      
        
      function loadAfterFilterSelected()
      {
                    $scope.showViewStep = 2;
                    $scope.isLoadingCoupons = true;
                    $scope.coupons = [];
                    $scope.couponsSize = 0;

                    $scope.addMoreItems();
      }
      
      
        $scope.applyBrandFilter = function(type,id, name)
      {
        if(type == 'all')
        {
          $scope.selectedBrand = null;
          $scope.brandFilter = null;
        }
        else{
         $scope.selectedBrand = id.brandId;
          $scope.brandFilter = id.brandId;
          $scope.couponFilterName = name;
        }
        
        loadAfterFilterSelected();
      }
      
        
        $scope.hasFilter = function() {
            return ($scope.couponFilterId.length? true: false);
        }
                
        
        $scope.getFilterName = function(item, category) {
           
                    try {
                        if (item.names[TalosValues.getLanguage()].length)
                            return item.names[TalosValues.getLanguage()];
                        else {
                            if (category == 'brand')
                                
                                return item.name;
                         }
                    } catch(error) {
                        return "";
                    }
        }
        
        
        
        $scope.addMoreItems = function() {
                $log.log('addMoreItems in CouponsCtrl')
                /* disable scrolling until data received */
                _allItemsLoaded = true;
          
          
                var from = $scope.coupons.length;
                from = from || 0;
                var to = from + Constants.PAGE_SIZE; /* PAGGING for Coupons */
                var method = 'get';
                var params = Array();

                  $scope.listIsLoading = true;
                if ($state.current.name == 'tab.gifts') {

                if ($scope.couponFilterCategory == 'brand')
                    brandFilter = $scope.couponFilterId;
                   
                   Brands.get(0, -1, Constants.BRAND_STATUS_PUBLISHED, null, null,function(data)
                     {
                        if(data!=null)
                        {
                            $scope.brands = data;
                        }
                     });
                     
                    Coupons.get(0, 8,[], true, $scope.brandFilter, null, null, Constants.BRAND_PRODUCT_TYPE_DEAL, Constants.BRAND_PRODUCT_STATUS_PUBLISHED).then(function(data) {    
                    $scope.handleCouponsResponse(data);
    
                        
                        
                  }, function() {
                        $scope.checkShowView(2);
                         
                  });    
                    
                } 
          

      };

        
                
           $scope.moreDataCanBeLoaded = function(){
                    if (($scope.couponsSize)!=($scope.coupons.length)){   
                         return varlen;      
                    }
           };    
        
        
        
        
      $scope.handleCouponsResponse = function(data) {
                if (data.data.length)
                  $scope.getCouponResources(data.data, 'coupons');
                else $scope.checkShowView(1);


                $scope.coupons = $scope.coupons.concat(data.data);

                $scope.couponsSize = parseInt(data.headers(Constants.HEADER_XTalosItemCount));
                if (!$scope.couponsSize) $scope.coupons = [];
                if ($scope.couponsSize > $scope.coupons.length)
                  _allItemsLoaded = false;

                $scope.checkShowView(1);
      }

      $scope.loadMore = function() {
            return _allItemsLoaded;
      };

        
      $scope.changeCouponStatus = function(newCouponStatus, newCouponStatusMode, newCouponStatusText) {
                $scope.showView = false;
                $scope.showViewStep = 0;

                $scope.couponStatus = newCouponStatus;
                $scope.couponStatusText = $filter('translate')(newCouponStatusText);
                $scope.couponStatusMode = newCouponStatusMode;

                _allItemsLoaded = false;
                $scope.coupons = [];
                $scope.couponsSize = 0;
                $scope.addMoreItems();
      }

      
      
      $scope.isSelectableCouponStatusMode = function(checkMode) {
        return !($scope.couponStatusMode == checkMode);
      }

      
      
      $scope.getCouponLogoResourceUrl = function(coupon) {
        try {
          return Resources.getResourceUrl(coupon.brandProductId, Constants.ITEM_TYPE_BRAND_PRODUCT, Constants.RESOURCE_TYPE_IMAGE, Constants['LANGUAGE_ID_' + TalosValues.getLanguage()]);
        } catch (error) {
          return "";
        }
        
      }

      
      
      
      $scope.couponHasTag = function(coupon) {
        try {
          return coupon.tagIds.length? true: false;
        } catch(error) { return false; }
      }

         
      
      
      $scope.getCouponEndDate = function(coupon) {
        try {
          var date;
          if ($state.current.name == 'mycoupons')
            date = new Date(coupon.endData);
          else
            date = new Date(coupon.activeUntil);
         // if ($state.current.name == 'coupon')
            return date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
          //else
            //return date.getDate() + ' ' + $filter('translate')('Month_' + date.getMonth()) + ' ' + date.getFullYear();
        } catch(error) {
          return "";
        }
      }

   
      
      
      $scope.getCouponResource = function(coupon, resource) {
        try {
          return coupon.resources[TalosValues.getLanguage()][resource];
        } catch(error) {
          return "";
        }
      }

      
      
      
      $scope.getCouponPrice = function(coupon) {
        try {
          var amount, resAmount = 0;
          
            angular.forEach(coupon.prices, function(price, key) {
              if (price.brandServiceId == Constants.DEFAULT_BRAND_SERVICE_ID) {
                amount = price.prices[0].amount;
                resAmount = (amount.value / Math.pow(10, amount.scale));
              }
            });
        
          return Utils.numberFormat(resAmount, 0, ',', '.');
        } catch(error) {
          return "";
        }
      }
   
      
      
       $scope.getCouponBrandImageResourceUrl = function(coupon) {

         try {
           return Resources.getResourceUrl(coupon.brandId, Constants.ITEM_TYPE_BRAND, Constants.RESOURCE_TYPE_IMAGE_FEATURED, Constants['LANGUAGE_ID_' + TalosValues.getLanguage()]);
         } catch (error) {
           return "";
         }          
         console.log("ImageCtrl");            
     }

      
      
      $scope.getCouponResources = function(coupons, state) {
        var resource = {},
          resources = Array(),
          input = {},
          property = "",
          j;
 

        try {
          angular.forEach(coupons, function(coupon, key) {
            coupon.resources = {};
            angular.forEach(Constants.LANGUAGE_IDS, function(language, languageKey) {
              coupon.resources[Constants['LANGUAGE_CODE_' + language]] = {
                "name": "",
                "description": "",
                "brandName": ""
              };
            });

            resource = {};
            resource.itemId = coupon.brandProductId;
            resource.itemTypeId = Constants.ITEM_TYPE_BRAND_PRODUCT;
            resource.resourceTypeId = Constants.RESOURCE_TYPE_NAME;
            resources.push(resource);

            if (state == "tab.gifts") {
                Utils.trackScreen("coupon id:"+coupon.brandProductId);
                $scope.postResources =  coupon.resources;
                resource = {};
                resource.itemId = coupon.brandProductId;
                resource.itemTypeId = Constants.ITEM_TYPE_BRAND_PRODUCT;
                resource.resourceTypeId = Constants.RESOURCE_TYPE_DESCRIPTION;
                resources.push(resource);
            }
          });

          input.languageIds = Constants.LANGUAGE_IDS;
          input.resources = resources;
          Resources.getMultipleResources(0, -1, input,Constants.APPLICATION_SETTING_COUPONS_RESOURCES_VERSION).then(function(data) {
            angular.forEach(data.data, function(resource, resourceKey) {
              if (resource.itemTypeTO.itemTypeId == Constants.ITEM_TYPE_BRAND_PRODUCT) {
                switch (resource.resourceTypeTO.resourceTypeId) {
                  case Constants.RESOURCE_TYPE_NAME:
                    property = "name";
                    break;
                  case Constants.RESOURCE_TYPE_DESCRIPTION:
                    property = "description";
                    break;                  
                }
                // coupons[j].resources[Constants["LANGUAGE_CODE_" + resource.languageTO.languageId]][property] = resource.textResource;
                angular.forEach(coupons, function(coupon, couponKey) {
                    
                    coupon.finalPrice = $scope.getCouponPrice(coupon);
                    coupon.finalEndDate = $scope.getCouponEndDate(coupon);
                    
                  if (resource.itemId == coupon.brandProductId) {
                    coupon.resources[Constants["LANGUAGE_CODE_" + resource.languageTO.languageId]][property] = resource.textResource;
                    if (property == 'name') {
                        coupon.resources[Constants["LANGUAGE_CODE_" + resource.languageTO.languageId]]['TWITTER_POST_TITLE'] = resource.textResource;
                        coupon.resources[Constants["LANGUAGE_CODE_" + resource.languageTO.languageId]]['POST_TO_WALL_TITLE'] = resource.textResource;
                        coupon.resources[Constants["LANGUAGE_CODE_" + resource.languageTO.languageId]]['POST_TO_WALL_PIC_URL'] = $scope.getCouponLogoResourceUrl(coupon);
                    } else if (property == 'description') {
                        coupon.resources[Constants["LANGUAGE_CODE_" + resource.languageTO.languageId]]['POST_TO_WALL_TEXT'] = resource.textResource;
                    }
                  }
                });
              } else if (resource.itemTypeTO.itemTypeId == Constants.ITEM_TYPE_BRAND) {
                property = "brandName";
                angular.forEach(coupons, function(coupon, couponKey) {
                  if (resource.itemId == coupon.brandId)
                    coupon.resources[Constants["LANGUAGE_CODE_" + resource.languageTO.languageId]][property] = resource.textResource;
                });
              }
            });


            $scope.name = $scope.getCouponResource($scope.coupon, "name");

            $scope.checkShowView(1);
          }, function() {
            $scope.checkShowView(1);
          });
        } catch (error) {
          $scope.checkShowView(1);
        }
      }

        
      $scope.couponDetails = function(coupon) {
           $state.go('tab.giftDetail', {'couponID': coupon.brandProductId});
      } 
        
      $scope.checkShowView = function(steps) {
        $scope.showViewStep += steps;
        $scope.listIsLoading = false;
        if ($scope.showViewStep == $scope.showViewNSteps) {
            $scope.showView = true;

            if($state.current.name == 'tab.gifts')
              $scope.isLoadingCoupons = false;
          }
      }                         
}]);