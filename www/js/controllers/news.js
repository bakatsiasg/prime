'use strict';

controllers.controller('NewsCtrl',['$scope','$rootScope','EventService','Events',
    '$state','Constants','Utils','TalosValues','localStorageService','$location','$log','$ionicHistory','$ionicSideMenuDelegate','$animate',"$timeout",'$translate','HomePageCategories','User','Resources','TalosCore','Messages',
    function($scope,$rootScope,EventService,Events,$state,
        Constants,Utils, TalosValues,localStorageService, $location, $log,$ionicHistory,$ionicSideMenuDelegate,$animate,$timeout,$translate, HomePageCategories, User, Resources,TalosCore,Messages) {




        // $scope.english = true;
        // var appContainer = document.getElementById('appContainer');
        // var appElement = angular.element(appContainer);
        // $scope.click = function (){
        //     $log.debug('click');
        //     $log.debug('$scope.english = '+$scope.english);

        //     var listener = function(e) {
        //         switch(e.type) {
        //             case "animationstart":
        //             {
        //                 $timeout(function(){
        //                     $scope.english = ! $scope.english;
        //                     var lang = $scope.english ?'en':'ar';
        //                     $translate.use(lang);
        //                     moment.locale(lang);
        //                 },300)
        //                 $log.debug("animationstart");
        //             }break;
        //             case "animationend":
        //             {
        //                 appContainer.removeEventListener("animationstart", listener, false);
        //                 appContainer.removeEventListener("animationend", listener, false);
        //                 appContainer.removeEventListener("animationiteration", listener, false);
        //                 appElement.removeClass('rotate');
        //                 $log.debug("animationend");
        //             }break;
        //         }
        //     }
        //     appContainer.addEventListener("animationstart", listener, false);
        //     appContainer.addEventListener("animationend", listener, false);
        //     $animate.addClass(appElement,'rotate');
        // }

        // $scope.$on(Events.LANGUAGE_SET,function(){
        //     $animate.addClass(appContainer,'rotate', function(){
        //         $animate.removeClass(appContainer,'rotate');
        //     });
        // })




//  $scope.goToState = function(path) {
//     $location.path(path);
//   }

//   $scope.homeData = [{
//     id:0,
//     header:'ΔΕΣ ΟΛΕΣ ΤΙΣ ΑΠΟΣΤΟΛΕΣ',
//     image:'img/card_image.png',
//     mainText:'Asfaleies en oiko gia kyria katoikia',
//     points:'100',
//     ratio:'5/10',
//     linkTo:'/tab/missions'
//   }
//   , {
//     id:1,
//     header:'ΔΕΣ ΟΛΑ ΤΑ ΔΩΡΑ ΕΞΑΡΓΥΡΩΣΗΣ',
//     image:'img/village_small.jpg',
//     mainText:'2 eisitiria gia ta village cinemas',
//     points:'200',
//     ratio:'7/10',
//     linkTo:'/tab/gifts'

//   },{
//     id:3,
//     header:'ΔΕΣ ΟΛΕΣ ΤΙΣ ΑΠΟΣΤΟΛΕΣ',
//     image:'img/card_image.png',
//     mainText:'Asfaleies en oiko gia kyria katoikia',
//     points:'100',
//     ratio:'5/10',
//     linkTo:'/tab/missions'
//   }
//   , {
//     id:4,
//     header:'ΔΕΣ ΟΛΑ ΤΑ ΔΩΡΑ ΕΞΑΡΓΥΡΩΣΗΣ',
//     image:'img/village_small.jpg',
//     mainText:'2 eisitiria gia ta village cinemas',
//     points:'200',
//     ratio:'7/10',
//     linkTo:'/tab/gifts'

//   },{
//     id:3,
//     header:'ΔΕΣ ΟΛΕΣ ΤΙΣ ΑΠΟΣΤΟΛΕΣ',
//     image:'img/card_image.png',
//     mainText:'Asfaleies en oiko gia kyria katoikia',
//     points:'100',
//     ratio:'5/10',
//     linkTo:'/tab/missions'
//     }];



            function loadItems(){

                HomePageCategories.setIsLoading(true);
                $scope.items = HomePageCategories.homeCategories;
                $scope.mainCategory = HomePageCategories.homeCategories[0];

            }

            if(HomePageCategories.homeCategories)
                loadItems();

            $scope.$on(Events.LOAD_HOME_ITEMS,loadItems);


            $scope.listInit = function() {
                $scope.messages = [];
            }

            $scope.listLoadMoreItems = function(onDataLoad) {
                var from = $scope.messages.length;
                from = from || 0;
                var to = from + 1; /* PAGGING for Notifications */

                Messages.get(from, to).then(function(data){
                    onDataLoad(data);
                });
            }

            $scope.disabledLoad = function() {
                return _loadIsDisabled;
            };


            $scope.itemIsLoaded = function(item){
                item.isLoading = false;
            }

            $scope.getItemImageUlr= function(item) {
                try {
                    return Resources.getResourceUrl(item.itemIdForResourceLookup, item.itemTypeIdForResourceLookup, item.imageResourceType, Constants['LANGUAGE_ID_' + TalosValues.getLanguage()]);
                } catch (error) {
                    return "";
                }
            }

            $scope.getItemName = function (item)
            {
                try {
                    return item.names[TalosValues.getLanguage()];
                } catch(error) {
                    return "";
                }
            }

            console.log($scope.items);

}]);