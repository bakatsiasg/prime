'use strict';

/**
 * @ngdoc object
 * @name core.Controllers.EditProfileCtrlController
 * @description EditProfileCtrlController
 * @requires ng.$scope
 */
controllers.controller('ProfileEditCtrl', [
    '$scope', '$rootScope',  'Profile', 'User', '$filter',   '$state', 'EventService', 'Events','States','TalosValues','Utils', '$ionicLoading', '$ionicPopup',
    function($scope, $rootScope, Profile, User, $filter, $state, EventService, Events, States, TalosValues,Utils, $ionicLoading, $ionicPopup) {
     // console.log('EditProfileCtrl Initialized')
  
  
 

      var convertToTalosDate = function(date) {
        return (date && date != null) ? $filter('date')(date, 'dd/MM/yyyy') : "";
      }

      var convertFromTalosDate = function(date) {
       // console.log("convertFromTalosDate " + date)
        return (date && date != null) ? new Date(moment(date,'DD/MM/YYYY')) : null;
      }

      function setLocalName(){
        // Profession.setLocaleNames(TalosValues.getLanguage());
        // Countries.setLocaleNames(TalosValues.getLanguage());
        States.setLocaleNames(TalosValues.getLanguage());
      }

      function getUser(){
          Profile.get(function(data) {
              $scope.user =  angular.copy(data);

              $scope.user.birthday = "";

        // console.log("== user before ==");
        // console.log(JSON.stringify($scope.user));
//
        /* for every custom Field create a property in user object
         *  eg user.customUserProfileFieldList["Profile.nameday"] => user.nameday
         * */
        angular.forEach(
          $scope.user.customUserProfileFieldList,
          function (val, key) {
            //   console.log("key : " + key + " | value : " + val);
            var varName = val.key.replace("Personal.", "");
//                        varName = varName.replace(".", "");
            //   console.log("varName : " + varName);
            if (val.value) {
              if (varName.indexOf("day") == varName.length - 3) {
                $scope.user[varName] = convertFromTalosDate(val.value);
              }
              else if(varName.indexOf("profession") > -1)
              {
                $scope.user[varName] = parseInt(val.value);
              }
              else
              {
                $scope.user[varName] = val.value;
              }
            }


            /* if the end of the name is "day" (eg birthday, nameday, child1Birthday)
             * update to represent a date in angular */

          });


States.get('8A8A8081-16A7-7A31-0116-A77A6C640001', true).then(function(data) {     
            $scope.statesArray = data.data;
            console.log("Success", $scope.statesArray ); 
            }, function(data) {
                console.log("Error: No data was found");
        });



        for (var i=1 ; i<11 ; i++) {
          var childDate = $scope.user[ "child" + i +"Birthday" ];
          if (childDate)
            $scope.childrenVariableNames.push( "child" + i +"Birthday" );
        }

        delete $scope.user.customUserProfileFieldList;

          /* clear birthday NOT USED will be updated from Personal.birthday */
        }, false);
        //console.log("== user after ==");
        // console.log(JSON.stringify($scope.user));
      }



      setLocalName();

      $scope.states = States.states;
      // $scope.countries = Countries.countries;
      // $scope.professions = Profession.professions;
      $scope.today = $filter('date')(new Date(), 'yyyy-MM-dd');


      var PASSWORD_STARS = "******";

      /* selected page */
      $scope.currentPage = 1;
      /* enable/disable editing*/
      $scope.editMode = false;
      /* user object, all field visualize this object except passwords */
      $scope.user = {};
      $scope.password = {
        pass : PASSWORD_STARS,
        retype : PASSWORD_STARS
      };
      /* list of name from user.customUserProfileFieldList
       * that represent chilrend birtday eg "child1Birthday", "child2Birthday" */
      $scope.childrenVariableNames = [];

      /* flag to check if user change his avatar */
      var userChangeAvatarBase64 = null;

      getUser();



      $scope.editBtn = function() {
        $scope.editMode = true;
      };


      $scope.editOkBtn = function() {


        $ionicLoading.show();
        if ($scope.password.pass !== $scope.password.retype) {
               $ionicPopup.alert({
                                         title: 'Error!',
                                         template: '{{"Password_missmatch" | translate}}'
                                });
                      
        }
        var talosUser = converToTalosUser($scope.user);
        var pass2send = ($scope.password.pass === PASSWORD_STARS) ? '' : $scope.password.pass;
        // hey its user-3-send, not 2-send, got it? funny ha?
        var user3send = {
          user: talosUser,
          newPassword: pass2send
        }
       // console.log('submitted: ' + JSON.stringify(user3send));
        try { delete(user3send.user['contact.valid.email']); } catch(e) {}
        try { delete(user3send.user['parkingPlace']); } catch(e) {}
        try { delete(user3send.user['occupation']); } catch(e) {}
       // console.log("Password 2 send : " + pass2send);

        User.updateMyProfile(user3send)
          .then(
          function(data){

            if (userChangeAvatarBase64) {
              User.uploadAvatar(userChangeAvatarBase64)
                .then(
                function(data){
                  afterUpdateAndUploadFinished();
                  Profile.imageUpdated();
                },
                function(data){
                }
              );
            } else {
              afterUpdateAndUploadFinished();
            }
          },
          function(data){
            $ionicLoading.hide();
            
            $ionicPopup.alert({
                 title: 'Error!',
                 template: '{{"Update_Error" | translate}}'
                                });
          }
        );
      };


      var afterUpdateAndUploadFinished = function() {


        $ionicLoading.hide();
        
        var alertPopup = $ionicPopup.alert({
              title: 'Success!',
              template: 'Update OK'
        });
        alertPopup.then(function(res) {
              console.log('Thank you for not eating my delicious ice cream cone');
              Profile.clear();
      
              EventService.sendEvent(Events.PROFILE_UPDATED);
              /* after update profile and message Ok go to home */
               $scope.editMode = false;
        });
      $state.go('tab.profile');
      };

      $scope.editCancelBtn = function() {
        $scope.editMode = false;
        getUser();
      };

      $scope.addChild = function(){
      //  console.log("addChild");
        var l = $scope.childrenVariableNames.length;
        if (l<10)
          $scope.childrenVariableNames.push( "child" + (l+1) +"Birthday" );
      }

      var converToTalosUser = function(user) {

        var talosUser = angular.copy(user);

        if (talosUser.birthday && talosUser.birthday != null) {
          talosUser.birthday = convertToTalosDate(talosUser.birthday);
        }
        if (talosUser.nameday && talosUser.nameday != null) {
          talosUser.nameday = convertToTalosDate(talosUser.nameday);
        }


        for ( var i=1 ; i<11 ; i++ ) {
          var date = talosUser["child" + i +"Birthday"];
          talosUser["child" + i +"Birthday"] = convertToTalosDate(date);
        }

        talosUser.customUserProfileFieldList = [];
        angular.forEach(
          customFieldNames,
          function(val, key) {
            var value = talosUser[val];
            delete talosUser[val];
            if (value) {
              //if (key.indexOf("profession")>-1) {
              //value = parseInt(value) || "";
              //}
              // if (key.indexOf("state")>-1) {
              //  val = value.id || "";
              // }
              talosUser.customUserProfileFieldList.push({ key: "Personal." + val, value: value });
            }
          }
        )

        talosUser.nickname = talosUser.username;

        delete talosUser['contact.email'];
        delete talosUser['postCode'];

        return talosUser;
      }

      var customFieldNames = [
        "birthday",
        "nameday",
        "gender",
        "idNumber",
        "name",
        "profession",
        "surname",
        "mobileNumber",
        "deviceType",
        "postalCode",
        "maritalStatus",
        "childrenStatus",
        "child1Birthday",
        "child2Birthday",
        "child3Birthday",
        "child4Birthday",
        "child5Birthday",
        "child6Birthday",
        "child7Birthday",
        "child8Birthday",
        "child9Birthday",
        "child10Birthday",
        "code",
        "state",
      ]

      $scope.selectImage = function() {
        if ($rootScope.isMobile) {
          navigator.camera.getPicture(
            onSuccess, onFail,
            {
              quality: 90,
              destinationType: Camera.DestinationType.DATA_URL,
              sourceType:0,
              encondingType:0
            }
          );
        } else {
          /* add listener for the input file only in web mode */
          $("#userAvatarFile")
            .change(function (e) {
            //  console.log("onchange");
              var thefile;
              if ((thefile = this.files[0])) {
                var fileReader = new FileReader();
                fileReader.onload = function(fileLoadedEvent) {
                  userChangeAvatarBase64 = fileLoadedEvent.target.result; // <--- data: base64

                  //var image = new Image();
                  //image.src = imageBase64;
                  //
                  //var canvas = document.createElement("canvas");
                  //canvas.width = image.width;
                  //canvas.height = image.height;
                  //canvas.getContext("2d").drawImage(image, 0, 0);
                  //
                  //userChangeAvatarBase64 = canvas.toDataURL("image/jpeg");

                  var userAvatar = document.getElementById('userImage');
                  userAvatar.src = userChangeAvatarBase64;
                  userChangeAvatarBase64 = userChangeAvatarBase64.substring(userChangeAvatarBase64.indexOf("base64,") + ("base64,").length );

                }
                fileReader.readAsDataURL(thefile);
              }
            });
          $("#userAvatarFile").trigger("click");
        }
      }

      function onSuccess(imageData) {
        var userAvatar = document.getElementById('userImage');
        userAvatar.src = "data:image/jpeg;base64," + imageData;
        userChangeAvatarBase64 = imageData;
      }

      function onFail(message) {
        userChangeAvatarBase64 = null;
        GenericDialogService
          .showPlain('','BasicData_UploadAvatarError', '');
      }

    }
  ]);
