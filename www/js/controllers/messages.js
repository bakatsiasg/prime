'use strict';
controllers.controller('MessagesCtrl',['$scope','$rootScope','$location','EventService','Events',
    '$state','Constants','Utils','TalosValues','localStorageService','Campaigns','Messages','MyMessages',function($scope,$rootScope,$location,EventService,Events,$state,
        Constants,Utils, TalosValues,localStorageService, Campaigns, Messages, MyMessages) {

 
  $scope.campaigns = Campaigns.all();
  $scope.messages = MyMessages.all();

  // $scope.campaigns.activatedFromMessages=false;
  $scope.selectTabWithIndex = function(id) {
    // $scope.campaigns.activatedFromMessages=true;
    $scope.campaigns.id=id;
    $location.path('/tab/campaigns');
  };

 
//$scope.removeCampaign = function(campaign) {
  // Campaigns.remove(campaign);
  // }

// $scope.removeMessage = function(message) {
  // Messages.remove(message);
  // }
  
}]);