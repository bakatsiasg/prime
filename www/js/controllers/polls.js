'use strict';

/**
 * @ngdoc object
 * @name core.Controllers.LotteriesCtrl
 * @description LotteriesCtrl
 * @requires ng.$scope
*/
controllers.controller('PollsCtrl', [
        '$scope', '$filter', 'Constants', 'Resources', 'Utils', '$state', '$stateParams', 'TalosValues', 'Categories', 'GlobalData', 'Metadata', 'Events', 'EventService', '$sce', 'Polls', '$translate', '$ionicPopup',
        function($scope, $filter, Constants, Resources, Utils, $state, $stateParams, TalosValues, Categories, GlobalData, Metadata, Events, EventService, $sce, Polls, $translate, $ionicPopup ) {
            //console.log('LotteriesCtrl')
             
            $scope.polls = [];
            
            $scope.showView = false;
            $scope.showViewStep = 0;
            $scope.showViewNSteps = 2;
            
            var _allItemsLoaded = true;
            $scope.items = [];
            $scope.itemsSize = 0;
            
            $scope.pollID = '';
            $scope.item = {};
            $scope.pollUrl = '';
            $scope.showPoll = true;
            $scope.listIsLoading = false;
            if ($state.current.name == 'tab.poll') {
                $scope.pollID = $stateParams.pollID;
                if (!$scope.pollID) 
                    $state.go('tab.home');
            }
            
            $scope.category = Constants.CATEGORY_POLLS;
            
            var _tmpIds = [];
            
            Categories.get($scope.category, -1, true).then(function(data) {
                angular.forEach(data.data[0].children, function(children, leafKey) {
                    $scope.polls.push({'id': children.id});
                    _tmpIds.push(parseInt(children.id));
                });
                if ($state.current.name == 'tab.poll') {
                    if (_tmpIds.indexOf(parseInt($scope.pollID)) > -1) {
                        $scope.item = {'id': $scope.pollID};
                        $scope.getResources([$scope.item]);
                        Utils.trackScreen("poll id:"+$scope.pollID);
                    } else 
                        $state.go('tab.home');                    
 
                }   
                else   
                 $scope.addMoreItems();
              
            }, function() {
                
                    $scope.addMoreItems();
             
            });            
            
            $scope.addMoreItems = function(){
                //console.log('addMoreItems in LotteriesCtrl')
                /* disable scrolling until data received */
                _allItemsLoaded = true;
                $scope.listIsLoading = true;
                var from = $scope.items.length;
                var to = from + Constants.PAGE_SIZE; /* PAGGING */
                to = (to > $scope.polls.length)? $scope.polls.length: to;
                var data = [], 
                    j; 
                if (from != to) {
                    for (j = from; j < to; j++)
                        data[j - from] = $scope.polls[j];
                    $scope.getResources(data);
                    $scope.items = $scope.items.concat(data);
                    $scope.itemsSize = $scope.polls.length;
                    if ($scope.itemsSize > $scope.items.length)
                        _allItemsLoaded = false;                                                          
                    $scope.checkShowView(1);
                } else 
                    $scope.checkShowView(2);
            };

            $scope.loadMore = function() {
                return _allItemsLoaded;
            };
            
          
            $scope.getItemImageResourceUrl = function(item) {
                try {
                    return Resources.getResourceUrl(item.id, Constants.ITEM_TYPE_CATEGORY, Constants.RESOURCE_TYPE_IMAGE, Constants['LANGUAGE_ID_' + TalosValues.getLanguage()]);
                } catch(error) { return ""; }
            }            
            
            $scope.getResources = function(items) {
                var resource = {},
                    resources = Array(), 
                    input = {}, 
                    property = "", 
                    j;
                
                try {
                    angular.forEach(items, function(item, key) {
                        item.resources = {};
                        angular.forEach(Constants.LANGUAGE_IDS, function(language, languageKey) {
                            item.resources[Constants['LANGUAGE_CODE_' + language]] = {
                                "name": "",
                                "description": "", 
                                "href": "", 
                                "reward": "",
                                "allowGuest":"",
                                "TWITTER_POST_TITLE":"",
                                "POST_TO_WALL_TITLE":"",
                                "POST_TO_WALL_PIC_URL":"",
                                "POST_TO_WALL_TEXT":""
                            };
                        });
                        
                        resource = {};
                        resource.itemId = item.id;
                        resource.itemTypeId = Constants.ITEM_TYPE_CATEGORY;
                        resource.resourceTypeId = Constants.RESOURCE_TYPE_NAME;
                        resources.push(resource);
                        
                        resource = {};
                        resource.itemId = item.id;
                        resource.itemTypeId = Constants.ITEM_TYPE_CATEGORY;
                        resource.resourceTypeId = Constants.RESOURCE_TYPE_DESCRIPTION;
                        resources.push(resource);
                        
                        resource = {};
                        resource.itemId = item.id;
                        resource.itemTypeId = Constants.ITEM_TYPE_CATEGORY;
                        resource.resourceTypeId = Constants.RESOURCE_TYPE_HREF;
                        resources.push(resource);                                                
                        
                        resource = {};
                        resource.itemId = item.id;
                        resource.itemTypeId = Constants.ITEM_TYPE_CATEGORY;
                        resource.resourceTypeId = Constants.RESOURCE_TYPE_REWARD_DESCRIPTION;
                        resources.push(resource);

                        resource = {};
                        resource.itemId = item.id;
                        resource.itemTypeId = Constants.ITEM_TYPE_CATEGORY;
                        resource.resourceTypeId = Constants.RESOURCE_TYPE_ALLOW_GUEST;
                        resources.push(resource);
                    });
                    
                    input.languageIds = Constants.LANGUAGE_IDS;
                    input.resources = resources;
                    Resources.getMultipleResources(0, -1, input).then(function(data) {
                        var item;
                        var lang;
                        angular.forEach(data.data, function(resource, resourceKey) {
                            if (resource.itemTypeTO.itemTypeId == Constants.ITEM_TYPE_CATEGORY) {

                                lang  = Constants["LANGUAGE_CODE_" + resource.languageTO.languageId];

                                for (j = 0; j < items.length; j++) {
                                    item = items[j];
                                    if (item.id == resource.itemId) {
                                        switch (resource.resourceTypeTO.resourceTypeId) {
                                            case Constants.RESOURCE_TYPE_NAME:
                                                property = "name";
                                                item.resources[lang]['TWITTER_POST_TITLE'] = resource.textResource;
                                                item.resources[lang]['POST_TO_WALL_TITLE'] = resource.textResource;
                                                item.resources[lang]['POST_TO_WALL_PIC_URL'] = Resources.getResourceUrl(item.id, Constants.ITEM_TYPE_CATEGORY, Constants.RESOURCE_TYPE_IMAGE, lang);
                                                break;
                                            case Constants.RESOURCE_TYPE_DESCRIPTION:
                                                property = "description";
                                                item.resources[lang]['POST_TO_WALL_TEXT'] = resource.textResource;
                                                break;
                                            case Constants.RESOURCE_TYPE_HREF:
                                                property = "href";
                                                break;
                                            case Constants.RESOURCE_TYPE_REWARD_DESCRIPTION:
                                                property = "reward";
                                                break;
                                            case Constants.RESOURCE_TYPE_ALLOW_GUEST:
                                                property = "allowGuest";
                                                break;
                                        }
                                        item.resources[lang][property] = resource.textResource;
                                    }

                                }
                            }
                        });
                        
                        if ($state.current.name == 'tab.poll') {
                            try {
                                $scope.pollUrl = $sce.trustAsResourceUrl(items[0].resources[TalosValues.getLanguage()]["href"]);
                            } catch(e) { $state.go('tab.home'); }
                            
                            if ($scope.pollUrl) {
                                Polls.start(items[0].id).then(function(data) {
                                    console.log('starting...' + JSON.stringify(data.data));
                                    try {
                                        if (data.data.tokenId.length)
                                            $scope.pollUrl += (($scope.pollUrl.indexOf("?") < 0)? '?': '&') + 't=' + data.data.tokenId;
                                        else {
                                            $scope.showPoll = false;
                                            Utils.errorPopUp($translate.instant('Poll_AlreadyCompleted'));
                                            $state.go( 'tab.campaigns');
                                        }
                                    } catch(e) {
                                        //$state.go('polls');
                                        $scope.showPoll = false;
                                        Utils.errorPopUp($translate.instant('Poll_AlreadyCompleted'));
                                        $state.go('tab.campaigns');
                                    }
                                }, function() {
                                    //$state.go('polls');
                                    $scope.showPoll = false;
                                    Utils.errorPopUp($translate.instant('Poll_AlreadyCompleted'));
                                    $state.go('tab.campaigns');
                                });
                            }
                        } 
 
                    }, function() {
                        if ($state.current.name == 'tab.poll') {
                            $state.go('tab.home');
                        }
 
                    });
                } catch (error) {
                    if ($state.current.name == 'tab.poll') {
                        $state.go('tab.home');
                    }                    
 
                }
            }            

            $scope.join = function(item) {

                var user = GlobalData.user;
                var canJoin = true;
                var msg = "";

                var s = $scope.getResource(item,'allowGuest')
                var allowgest = s != "" ?parseInt(s):1;
                if(allowgest==0)
                {
                    if(Utils.checkIsGuestAndLogin('Login_Enter'))
                    return;

                }

                if (!canJoin) {
                        Utils.errorPopUp($translate.instant('Poll_join_error'));            
                        return;
                } else { 
                       var confirmPopup = $ionicPopup.confirm({
                                 title: '',
                                 template: '{{"EnterPoll_ConfirmationBody" |translate }}?'
                       });

                       confirmPopup.then(function(res) {
                                 if(res) {
                                         console.log('You are sure');
                                         $state.go('tab.poll', {'pollID': item.id});
                                 }
                                 else {
                                         console.log('You are not sure');
                                         return;
                                }
                       }, function() {}); 
                    
               }
          }            
            $scope.itemId = $stateParams.pollID;
            
            $scope.getResource = function(item, resource) {
                try {
                    return item.resources[TalosValues.getLanguage()][resource];
                } catch(error) {
                    return "";
                }
            }                                                            
            
            $scope.checkShowView = function(steps) {
                $scope.showViewStep += steps;
                $scope.listIsLoading = false;
                if ($scope.showViewStep == $scope.showViewNSteps)
                    $scope.showView = true;
            };                        
        }
]);
