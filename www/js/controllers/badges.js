'use strict';

/**
 * @ngdoc object
 * @name core.Controllers.BadgesCtrl
 * @description BadgesCtrl
 * @requires ng.$scope
*/
controllers.controller('BadgesCtrl', [
        '$scope',  'Constants', 'Achievements', 'Resources', 'TalosValues', '$state', '$stateParams', 'User', '$translate','Utils','$filter',
        function($scope,  Constants, Achievements, Resources, TalosValues, $state, $stateParams, User, $translate,Utils,$filter) {
            console.log('BadgesCtrl')

            $scope.isChecked = true;
            $scope.getAchievements = function(userId) {
                Achievements.get(userId).then(function(data) {
                    //console.log(JSON.stringify(data.data));

                    var length = data.data.length;
                    var item;
                    for(var i= 0 ; i < length ; i++)
                    {
                        item = data.data[i];
                        item.percentage = $scope.getCompletionPercentage(item);
                         if (item.percentage>=100)
                          $scope.mybadgesSize++;
                    }

                    $scope.badges = $filter('orderBy')(data.data,"percentage",true);
                    $scope.badgesSize = parseInt(data.headers(Constants.HEADER_XTalosItemCount));
                
                    $scope.getResources($scope.badges);
                }, function() {
                    $scope.showView = true;
                });            
            }            

            var _allItemsLoaded = false;
            $scope.listIsLoading = false;
            $scope.showView = false;
            $scope.badges = [];
            $scope.badgesSize = 0;
            $scope.mybadgesSize = 0;            
            $scope.friendId = null;
            if ($state.current.name == 'friendbadges') {
                if (angular.isDefined($stateParams.friendID)) 
                    $scope.friendId = $stateParams.friendID;
                if ($scope.friendId == null)
                    $state.go('friends');            
                
                User.getFriends(0, -1).then(function(data) {
                    var l, j, found = false;
                    l = data.data.length;
                    for (j = 0; j < l; j++) {
                        if (data.data[j].userId == $scope.friendId) {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                        $state.go('friends');
                    $scope.getAchievements($scope.friendId);
                }, function() {
                    $state.go('friends');
                });                            
            } else {
                $scope.getAchievements(null);
            }
            
            $scope.getResources = function(items) {
                var resource = {},
                    resources = Array(), 
                    input = {}, 
                    property = "", 
                    j;
                
                try {
                    angular.forEach(items, function(item, key) {
                        item.resources = {};
                        angular.forEach(Constants.LANGUAGE_IDS, function(language, languageKey) {
                            item.resources[Constants['LANGUAGE_CODE_' + language]] = {
                                "name": "",
                                "description": ""
                            };
                        });
                        
                        resource = {};
                        resource.itemId = item.achievementTypeId;
                        resource.itemTypeId = Constants.ITEM_TYPE_ACHIEVEMENT_TYPE;
                        resource.resourceTypeId = Constants.RESOURCE_TYPE_NAME;
                        resources.push(resource);
                        
                        resource = {};
                        resource.itemId = item.achievementTypeId;
                        resource.itemTypeId = Constants.ITEM_TYPE_ACHIEVEMENT_TYPE;
                        resource.resourceTypeId = Constants.RESOURCE_TYPE_DESCRIPTION;
                        resources.push(resource);                        
                    });
                    
                    input.languageIds = Constants.LANGUAGE_IDS;
                    input.resources = resources;
                    Resources.getMultipleResources(0, -1, input,Constants.APPLICATION_SETTING_BADGES_RESOURCES_VERSION).then(function(data) {
                        angular.forEach(data.data, function(resource, resourceKey) {
                            if (resource.itemTypeTO.itemTypeId == Constants.ITEM_TYPE_ACHIEVEMENT_TYPE) {
                                for (j = 0; j < items.length; j++) 
                                    if (resource.itemId == items[j].achievementTypeId) break;                                                            
                                switch (resource.resourceTypeTO.resourceTypeId) {
                                    case Constants.RESOURCE_TYPE_NAME: 
                                        property = "name";
                                        break;
                                    case Constants.RESOURCE_TYPE_DESCRIPTION: 
                                        property = "description";
                                        break;                                        
                                }
                                items[j].resources[Constants["LANGUAGE_CODE_" + resource.languageTO.languageId]][property] = resource.textResource;
                            }                         
                        });
                        console.log(Constants.RESOURCE_TYPE_NAME);
                        console.log(Constants.RESOURCE_TYPE_DESCRIPTION);
                        $scope.showView = true;
                    }, function() {
                        $scope.showView = true;
                    });
                } catch (error) {
                    $scope.showView = true;
                }
            }            
            
            $scope.getLogoResourceUrl = function(item) {
                try {
                    return item.percentage >=100? Resources.getResourceUrl(item.achievementTypeId, Constants.ITEM_TYPE_ACHIEVEMENT_TYPE, Constants.RESOURCE_TYPE_IMAGE, Constants['LANGUAGE_ID_' + TalosValues.getLanguage()])
                            :'img/badge_empty.png';
                } catch (error) {
                    return "";
                }                    
            }            
            
            $scope.getResource = function(item, resource) {
                try {
                    return item.resources[TalosValues.getLanguage()][resource];
                } catch(error) {
                    return "";
                }
            }                                                                                    
            $scope.getReward = function (item){
                try {
                    var amount = item.amountUnitRewards[0].amount;
                    return Utils.numberFormat((amount.value / Math.pow(10, amount.scale)), 0, ',', '.');
                }
                catch(error) {
                    return "0";
                }
            }
            $scope.getCompletionPercentage = function(item) {
                try {
                    return (parseInt(100 * item.progressSoFar / item.completionValue));
                } catch(error) {
                    return 0;
                }                    
            }
            
            $scope.addMoreItems = function() {
              _allItemsLoaded = true;            
            }
            
            $scope.getTitle = function() {
                return (($state.current.name == 'friendbadges')? $translate.instant('FriendBadges_Title'): $translate.instant('Badges_Title'));
            }
            
            $scope.loadMore = function() {
              return _allItemsLoaded;
            };    
            
           $scope.getProgress = function(item) {
      
           var percent = parseFloat(item.progressSoFar / item.completionValue);
           //console.log(percent);
           var width = percent * 146;
           var style = width + 'px';
           style = (style=='NaNpx'?'0px':style);
           return style;
           };        
        }
        

]);
