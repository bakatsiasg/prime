/**
 * Created by georgefloros on 5/4/15.
 */
 'use strict';
 controllers.controller('CompetitionDetailsCtrl',['$scope','$rootScope','$state', '$stateParams', '$ionicPopup', 'TalosValues', 'Constants', 'Tournaments', 'Resources', 'GlobalData', 'Utils', '$filter', 'EventService', 'Events', '$translate','localStorageService',
     function($scope, $rootScope, $state, $stateParams, $ionicPopup, TalosValues, Constants, Tournaments, Resources, GlobalData, Utils, $filter, EventService, Events, $translate, localStorageService) {

        $scope.participationToggle=true;
        // $scope.participationToggle = localStorageService.get($scope.participationToggle);
        // localStorageService.set($scope.participationToggle,true)
        
        $scope.itemId = $stateParams.competitionID;
          
          
         $scope.getTournamentResources = function(tournament) {
                 var resource = {},
                  resources = [],
                  input = {},
                  property = "",
                  j;
   
                        try {
                          //angular.forEach(products, function(product, key) {
                            tournament.resources = {};
                            angular.forEach(Constants.LANGUAGE_IDS, function(language, languageKey) {
                              tournament.resources[Constants['LANGUAGE_CODE_' + language]] = {
                                "name": "",
                                "description": "",
                                "brandName": "",
                                "content1":"",
                                "content2":"",
                                "POST_TO_WALL_PIC_URL":"",
                                "POST_TO_WALL_TEXT":"",
                                "POST_TO_WALL_TITLE":"",
                                "TWITTER_POST_TITLE": ""
                              };
                            });
                
                            resource = {};
                            resource.itemId = tournament.itemIdForResourceLookup;
                            resource.itemTypeId = Constants.ITEM_TYPE_TOURNAMENT_RESOURCES;
                            resource.resourceTypeId = Constants.RESOURCE_TYPE_NAME;
                            resources.push(resource); 

                            resource = {};
                            resource.itemId = tournament.itemIdForResourceLookup;
                            resource.itemTypeId = Constants.ITEM_TYPE_TOURNAMENT_RESOURCES;
                            resource.resourceTypeId = Constants.RESOURCE_TYPE_DESCRIPTION;
                            resources.push(resource);

                            resource = {};
                            resource.itemId = tournament.itemIdForResourceLookup;
                            resource.itemTypeId = Constants.ITEM_TYPE_TOURNAMENT_RESOURCES;
                            resource.resourceTypeId = Constants.RESOURCE_TYPE_TOURNAMENT_WINNERS_URL;
                            resources.push(resource);
                 
                
                            input.languageIds = Constants.LANGUAGE_IDS;
                            input.resources = resources;
                            Resources.getMultipleResources(0, -1, input,Constants.APPLICATION_SETTING_TOURNAMENT_RESOURCES_VERSION).then(function(data) {
                                angular.forEach(data.data, function(resource, resourceKey) {
                                // if (resource.itemTypeTO.itemTypeId == Constants.ITEM_TYPE_BRAND_PRODUCT) {
                                    //for (j = 0; j < products.length; j++)
                                    //if (resource.itemId == coupons[j].brandProductId) break;
                                    console.log("");
                                    switch (resource.resourceTypeTO.resourceTypeId) {
                                        case Constants.RESOURCE_TYPE_NAME:
                                        {
                                            property = "name";
                                            break;
                                        }
                                        case Constants.RESOURCE_TYPE_DESCRIPTION:
                                        {
                                            property = "description";
                                            break;
                                        }
                                        case Constants.RESOURCE_TYPE_TOURNAMENT_WINNERS_URL:
                                        {
                                            property = "winners_url";
                                            break;
                                        }
                                         
                                    }
                                     tournament.resources[Constants["LANGUAGE_CODE_" + resource.languageTO.languageId]][property] = resource.textResource;
                          })}, function() { 
                          });
                        } catch (error) {   
                        }
      }     
          
          
          
        Tournaments.getTournament($scope.itemId, function(data){
                
                if (data!=null){
                        $scope.tournament = data;
                        console.log("rtefvtr");
                        $scope.getTournamentResources($scope.tournament);
                }
        })
        
        
       $scope.getImageResourceUrl = function(tournament) {  
              try {
                return Resources.getResourceUrl(tournament.itemIdForResourceLookup, Constants.ITEM_TYPE_TOURNAMENT_RESOURCES, Constants.RESOURCE_TYPE_IMAGE, Constants['LANGUAGE_ID_' + TalosValues.getLanguage()]);
              } catch (error) {
                return "";
              } 
       }
       
       $scope.getTourResource = function(tour, resource){
            try {
                return tour.resources[TalosValues.getLanguage()][resource];
                } catch(error) {
                return "";
             }
       }
       
        $scope.join = function(tournament,$event) {
                if($scope.itemUserParticipationStatus(tournament, 'finished'))
                {
                    $event.stopImmediatePropagation();
                    return;
                }

                var user = GlobalData.user;
                var canJoin = true;
                var msg = "";

                if(Utils.checkIsGuestAndLogin('Login_Enter'))
                    return;

                //if (canJoin && user.isGuest) {
                //    canJoin = false;
                //    msg = $scope.msgs['Enter_Login'];
                //}

                if (canJoin && tournament.participationStatus) {
                    // if ($state.current.name == 'tab.competition') {
                        // $state.go('tab.leaderboards', {'tournamentID': tournament.tournamentId});
                        // return;
                    // }
                    canJoin = false;
                    $ionicPopup.alert({
                        title: '',
                        template: '{{"AlreadyJoined"|translate}}'
                        })
                        //msg = $scope.msgs['Enter_AlreadyJoined'];
                        $scope.participationToggle = !$scope.participationToggle;
                }

                if (canJoin && tournament.level.length) {
                    if (!user.currentLevel) canJoin = false;
                    else {
                        if (parseInt(tournament.level) > parseInt(user.currentLevel))
                            canJoin = false;

                        if (!canJoin)
                        $ionicPopup.alert({
                                title: '',
                                template: '{{"EnterLottery_LevelNotSufficient"|translate}}'
                        })
                            //msg = $filter('translate')('EnterLottery_LevelNotSufficient');
                            $scope.participationToggle=true;
                    }
                }

                if (canJoin && tournament.entryFeeAmount) {
                    if (tournament.entryFeeAmount > user.points) {
                        canJoin = false;
                        $ionicPopup.alert({
                                title: '',
                                template: '{{"EnterLottery_BalanceNotSufficient"|translate}}'
                        })
                        // msg = $filter('translate')('EnterLottery_BalanceNotSufficient');
                        $scope.participationToggle=true;
                    }
                }

                if (!canJoin) {
                    $ionicPopup.alert({
                                title: '',
                                template: '{{"Error_Ocured_Cannot_Join"|translate}}'
                        })
                        $scope.participationToggle=false;
                    return;
                } else {
                    var confirmPopup = $ionicPopup.confirm({
                        title: '',
                        template: '{{"TakePart_Confirmation"|translate}}'
                   });    
                        
                   confirmPopup.then(function() {
                        try {
                            Tournaments.join(tournament.tournamentId).then(function(data) {
                                EventService.sendEvent(Events.BALLANCE_UPDATED);

                                //Utils.trackScreen(($state.current.name == 'missions.lotteries'?"lottery":"competion")+" id:"+item.tournamentId)

                                tournament.participationStatus = Constants.TOURNAMENT_PARTICIPATION_STATUS_REGISTERED; //todo: use constant

                                $ionicPopup.alert({
                                        title: '',
                                        template: '{{"Joined_Success"|translate}}'
                                })
                                $scope.participationToggle=false;
                                
                            }, function(data) {
                                //var errorMsg = $scope.msgs['Enter_Error'];
                                /*
                                try {
                                    if (data.data.code == Constants.COUPON_PURCHASE_USER_SESSION_PERMISSION_ERROR)
                                        errorMsg = $filter('translate')('Coupon_NotPurchased_Invalid');
                                    else if (data.data.code == Constants.COUPON_PURCHASE_INSUFFICIENT_BALANCE_FOR_PURCHASE_ERROR)
                                        errorMsg = $filter('translate')('Coupon_NotPurchased_Balance');
                                } catch(error1) {}
                                */
                               $ionicPopup.alert({
                                        title: '',
                                        template: '{{"Enter_ErrorAlreadyJoined"|translate}}'
                                })
                                $scope.participationToggle=false;
                            });
                        } catch(error) {
                                console.log("exeption");
                            // ngDialog.open({
                                // template: 'templates/dialogs/generic_dialog.html',
                                // className: 'ngdialog-theme-mall',
                                // data: angular.toJson({
                                //     'body': $filter('translate')('Coupon_NotPurchased'),
                                //     'status': 'error'
                                // }),
                            // });
                        }
                    });
                }
            }          
            
                       
                         
                         
            $scope.itemUserParticipationStatus = function(tournament, status) {
                var d1 = tournament.endsOn;
                var d2 = new Date().getTime();
                var s = "";

                if (d1 < d2) s = "finished";
                else if (tournament.participationStatus) s = "joined";
                else s = "open";
                return (s == status);
            }
  
          $scope.getExpirationTimeCounter = function(tournament) {
                var d = "00:00";
                try {
                    var d1 = tournament.endsOn;
                    var d2 = new Date().getTime();
                    return ((d1 > d2)? Utils.getDateDif(d1, d2): d);
                } catch(e) { return d; }
            }
      

 	 

 }]);