'use strict';

/**
 * @ngdoc object
 * @name core.Controllers.LeaderboardsCtrl
 * @description LeaderboardsCtrl
 * @requires ng.$scope
*/
controllers.controller('LeaderboardsCtrl', [
        '$scope', '$filter', 'Constants', 'Resources', 'Utils', '$state', '$stateParams', 'TalosValues', 'Categories', 'Tournaments', '$translate', 'GlobalData', 'Profile',
        function($scope, $filter, Constants, Resources, Utils, $state, $stateParams, TalosValues, Categories, Tournaments, $translate, GlobalData, Profile) {
            //console.log('LeaderboardsCtrl')
 
            $scope.leaderDetails = function(item){
                $state.go('tab.leaderboard',{'tournamentID':item.tournamentId});
            }
            $scope.tournaments = [];
            var i, x;
            
            /* Leaderboard state */
            $scope.tournamentId = '';
            $scope.item = {};
            $scope.rankings = [];
            $scope.rankingsSize = 0;
            $scope.listIsLoading = false;
            $scope.tournExist = false;
            $scope.showView = false;
            
            if ($state.current.name == 'tab.leaderboard') { 
                $scope.tournamentId = $stateParams.tournamentID;
                if (!$scope.tournamentId) {
                    $state.go('tab.leaderboards');
                    return;
                }
                else{
                    Utils.trackScreen("leaderboard id:"+$scope.tournamentId);
                   
                }    

            }
 
            
            var _allItemsLoaded = true;
            $scope.items = [];
            $scope.itemsSize = 0;
            
            $scope.category = Constants.TOURNAMENT_TYPE_LEADERBOARD;
            
            Categories.get($scope.category, -1, true).then(function(data) {
                var tmp = [], 
                    j;
                angular.forEach(data.data[0].leaves, function(leaf, leafKey) {
                     if ($state.current.name == 'tab.leaderboard') {
                        if (leaf.itemId == $scope.tournamentId)
                            tmp.push(leaf.itemId);
                    } else 
                        tmp.push(leaf.itemId);
                });
                Tournaments.get(0, -1, true, Constants.TOURNAMENT_TYPE_LEADERBOARD, Constants.GAME_TYPE, function(data){
                    var tmpTournamentIds = [];
                    var tmpDataIds = {};
                    for (j = 0; j < data.length; j++) {
                        tmpTournamentIds.push(data[j].tournamentId);
                        tmpDataIds[data[j].tournamentId] = j;
                    }
                    
                     
                    Tournaments.getUserParticipation(tmpTournamentIds).then(function(data1) { 
                        angular.forEach(data1.data, function(item, key) { 
                            //&& (tmp.indexOf(item.tournamentId) > -1)
                            if (((item.participationStatusId == Constants.TOURNAMENT_PARTICIPATION_STATUS_REGISTERED) || (item.participationStatusId == Constants.TOURNAMENT_PARTICIPATION_STATUS_AUTO_REGISTERED))) {
                                $scope.tournaments.push(data1.data[tmpDataIds[item.tournamentId]]);
                                $scope.showView = true;
                                $scope.tournExist  = true;
                            }
                        });                        
                        
                        $scope.getUserRankings($scope.tournaments);
                        if ($state.current.name == 'tab.leaderboard') {
                            // if ($scope.tournaments.length != 1) {
                            //     $state.go('home.leaderboards');
                            //     return;                            
                            // } else {
                            x=$scope.tournaments.length;
                            for (i=0; i<x; i++){
                                    if ($scope.tournaments[i].tournamentId==$scope.tournamentId){
                                        $scope.item = $scope.tournaments[i];
                                    }
                            }
                                
                            // }
                        }                   
                        if ($state.current.name == 'tab.leaderboards') 
                            $scope.addMoreItems();
                        else {
                            $scope.getResources(Array($scope.item));
                            $scope.addMoreRankingItems();
                        }
                       
                    }, function() {
                         
                       if ($state.current.name == 'tab.leaderboard') {
                            $state.go('home.leaderboards');
                            return;                            
                        }
                        $scope.addMoreItems();
                    });
                }, function() {         
                    if ($state.current.name == 'tab.leaderboard') {
                        $state.go('leaderboards');
                        return;                            
                    }                    
                    $scope.addMoreItems();
                });
            }, function() {
                               
               if ($state.current.name == 'tab.leaderboard') {
                        $state.go('leaderboards');
                        return;                            
                    }                    
                    $scope.addMoreItems();
            });     
            
            $scope.addMoreItems = function(){
                //console.log('addMoreItems in LeaderboardsCtrl')
                /* disable scrolling until data received */
                _allItemsLoaded = true;
                $scope.listIsLoading = true;
                var from = $scope.items.length;
                var to = from + Constants.PAGE_SIZE; /* PAGGING */
                to = (to > $scope.tournaments.length)? $scope.tournaments.length: to;
                var data = [], 
                    j; 
                if (from != to) {
                    for (j = from; j < to; j++)
                        data[j - from] = $scope.tournaments[j];
                    $scope.getResources(data);
                    $scope.items = $scope.items.concat(data);
                    $scope.itemsSize = $scope.tournaments.length;
                    
                    
                    
                    if ($scope.itemsSize > $scope.items.length)
                        _allItemsLoaded = false;                                      
                    $scope.checkShowView(1);
                } else 
                    $scope.checkShowView(2);
            };

            $scope.loadMore = function() {
                return _allItemsLoaded;
            };  
            
            $scope.addMoreRankingItems = function(){
                //console.log('addMoreRankingItems in LeaderboardsCtrl')
                /* disable scrolling until data received */
                _allItemsLoaded = true;
                $scope.listIsLoading = true;
                var from = $scope.rankings.length;
                from = from || 0;
                var to = from + Constants.PAGE_SIZE; /* PAGGING */
                Tournaments.getRankings(from, to, $scope.tournamentId).then(function(data) {
                    $scope.rankings = $scope.rankings.concat(data.data);
                    $scope.rankingsSize = parseInt(data.headers(Constants.HEADER_XTalosItemCount));
                    if (!$scope.rankingsSize) $scope.rankings = [];
                    //console.log(JSON.stringify($scope.rankings[0]))
                    if ($scope.rankingsSize > $scope.rankings.length)
                        _allItemsLoaded = false;                  
                    $scope.checkShowView(1);
                }, function() {
                    $scope.checkShowView(1);
                });
            };

            $scope.loadMoreRankings = function() {
                return _allItemsLoaded;
            };              
            
            $scope.getItemImageResourceUrl = function(item) {
                try {
                    return Resources.getResourceUrl(item.tournamentId, Constants.ITEM_TYPE_TOURNAMENT_RESOURCES, Constants.RESOURCE_TYPE_IMAGE, Constants['LANGUAGE_ID_' + TalosValues.getLanguage()]);
                } catch(error) { return ""; }
            }            
            
            $scope.getItemThumbnailResourceUrl = function(item) {
                try {
                    return Resources.getResourceUrl(item.tournamentId, Constants.ITEM_TYPE_TOURNAMENT_RESOURCES, Constants.RESOURCE_TYPE_THUMBNAIL, Constants['LANGUAGE_ID_' + TalosValues.getLanguage()]);
                } catch(error) { return ""; }
            }                        
            
            $scope.getItemStartDate = function(item) {
                try {
                    var date = new Date(item.startsOn);
                    return date.getDate() + ' ' + $filter('translate')('Month_' + date.getMonth()) + ' ' + date.getFullYear();
                } catch(error) {
                    return "";
                }
            }                                    
            
            $scope.getItemEndDate = function(item) {
                try {
                    var date = new Date(item.endsOn);
                    return date.getDate() + ' ' + $filter('translate')('Month_' + date.getMonth()) + ' ' + date.getFullYear();
                } catch(error) {
                    return "";
                }
            }                                    
            
            $scope.getResources = function(items) {
                var resource = {},
                    resources = Array(), 
                    input = {}, 
                    property = "", 
                    j;
                
                try {
                    angular.forEach(items, function(item, key) {
                        item.resources = {};
                        angular.forEach(Constants.LANGUAGE_IDS, function(language, languageKey) {
                            item.resources[Constants['LANGUAGE_CODE_' + language]] = {
                                "name": "",
                                "description": ""
                            };
                        });
                        
                        resource = {};
                        resource.itemId = item.tournamentId;
                        resource.itemTypeId = Constants.ITEM_TYPE_TOURNAMENT_RESOURCES;
                        resource.resourceTypeId = Constants.RESOURCE_TYPE_NAME;
                        resources.push(resource);
                        
                        resource = {};
                        resource.itemId = item.tournamentId;
                        resource.itemTypeId = Constants.ITEM_TYPE_TOURNAMENT_RESOURCES;
                        resource.resourceTypeId = Constants.RESOURCE_TYPE_DESCRIPTION;
                        resources.push(resource);                        
                    });
                    
                    input.languageIds = Constants.LANGUAGE_IDS;
                    input.resources = resources;
                    Resources.getMultipleResources(0, -1, input).then(function(data) {
                        angular.forEach(data.data, function(resource, resourceKey) {
                            if (resource.itemTypeTO.itemTypeId == Constants.ITEM_TYPE_TOURNAMENT_RESOURCES) {
                                for (j = 0; j < items.length; j++) 
                                    if (resource.itemId == items[j].tournamentId) break;                                                            
                                switch (resource.resourceTypeTO.resourceTypeId) {
                                    case Constants.RESOURCE_TYPE_NAME: 
                                        property = "name";
                                        break;
                                    case Constants.RESOURCE_TYPE_DESCRIPTION: 
                                        property = "description";
                                        break;                 
                                }
                                items[j].resources[Constants["LANGUAGE_CODE_" + resource.languageTO.languageId]][property] = resource.textResource;
                            } 
                        });
                        
                        $scope.checkShowView(1);
                    }, function() {
                        $scope.checkShowView(1);
                    });
                } catch (error) {
                    $scope.checkShowView(1);
                }
            }            
            
            $scope.getResource = function(item, resource) {
                try {
                    return item.resources[TalosValues.getLanguage()][resource];
                } catch(error) {
                    return "";
                }
            }                                                            
            
            $scope.checkShowView = function(steps) {
                $scope.showViewStep += steps;
                $scope.listIsLoading = false;
                if ($scope.showViewStep == $scope.showViewNSteps)
                    $scope.showView = true;
            };            
            
            $scope.getUserRankings = function(items) {
                angular.forEach(items, function(item, key) {
                    item.score = '';
                    item.ranking = '';
                    
                    Tournaments.getUserRanking(item.tournamentId).then(function(data) {
                        //console.log('res = ' + JSON.stringify(data));
                        try {
                            item.score = (data.data.score? Utils.numberFormat(data.data.score, 0, ',', '.'): 0);
                            item.ranking = data.data.ranking; 
                            if (data.data.userId.toUpperCase() ==Profile.profile.userId.toUpperCase()){
                                $scope.profile = Profile.profile;
                                $scope.profile.score = item.score;
                                $scope.profile.ranking = item.ranking;
                            }
                        } catch(e) {}
                        $scope.checkShowView(1);
                    }, function() {
                        $scope.checkShowView(1);
                    });
                });
            }
            
            $scope.getPosition = function(item) {
                if (!item.ranking) 
                    return $translate.instant('Not_Ranked');
                else if (item.ranking == 1)
                    return item.ranking + $translate.instant('First_Suffix') + ' ' + $translate.instant('Position');
                else if (item.ranking == 2)
                    return item.ranking + $translate.instant('Second_Suffix') + ' ' + $translate.instant('Position');
                else if (item.ranking == 3)
                    return item.ranking + $translate.instant('Third_Suffix') + ' ' + $translate.instant('Position');
                else 
                    return item.ranking + $translate.instant('Fourth_Suffix') + ' ' + $translate.instant('Position');                
            }
            
            $scope.getScore = function(item) {
                try {
                    return (item.score? Utils.numberFormat(item.score, 0, ',', '.'): 0);
                } catch(e) { return 0; }
            }
            
            $scope.getPicUrl = function() {
                var _user = GlobalData.user;
                $scope.picUrl = (_user.picUrl? _user.picUrl: Constants.DEFAULT_AVATAR);
                return $scope.picUrl;
            }            
            
            $scope.getRankingItemPicUrl = function(item) {
                try {
                    return (item.profilePicUrl? item.profilePicUrl: Constants.DEFAULT_AVATAR);
                } catch(e) { return Constants.DEFAULT_AVATAR; }
            }                        
        }
]);
