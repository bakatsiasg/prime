/**
 * Created by georgefloros on 4/14/15.
 */
'use strict';
controllers.controller('HomeCtrl',['$scope','$rootScope','Events',
    '$log','$ionicHistory','$ionicSideMenuDelegate','$animate',"$timeout",'$translate',
    function($scope,$rootScope,Events,$log,$ionicHistory,$ionicSideMenuDelegate,$animate,$timeout,$translate) {
        $scope.english = true;
        var appContainer = document.getElementById('appContainer');
        var appElement = angular.element(appContainer);
        $scope.click = function (){
            $log.debug('click');
            $log.debug('$scope.english = '+$scope.english);

            var listener = function(e) {
                switch(e.type) {
                    case "animationstart":
                    {
                        $timeout(function(){
                            $scope.english = ! $scope.english;
                            var lang = $scope.english ?'en':'ar';
                            $translate.use(lang);
                            moment.locale(lang);
                        },300)
                        $log.debug("animationstart");
                    }break;
                    case "animationend":
                    {
                        appContainer.removeEventListener("animationstart", listener, false);
                        appContainer.removeEventListener("animationend", listener, false);
                        appContainer.removeEventListener("animationiteration", listener, false);
                        appElement.removeClass('rotate');
                        $log.debug("animationend");
                    }break;
                }
            }
            appContainer.addEventListener("animationstart", listener, false);
            appContainer.addEventListener("animationend", listener, false);
            $animate.addClass(appElement,'rotate');
        }

        $scope.$on(Events.LANGUAGE_SET,function(){
            $animate.addClass(appContainer,'rotate', function(){
                $animate.removeClass(appContainer,'rotate');
            });
        })
}]);