/**
 * Created by georgefloros on 4/16/15.
 */
'use strict';
directives.directive('passwordValidator',['$compile','Constants',function($compile,Constants) {
    return {
        restrict: 'A',
        require: "ngModel",
        scope: {
            values: "=passwordValidator",
            editMode:"=editMode"
        },
        link: function (scope, element, attributes, ngModel) {
            ngModel.$validators.compareTo = function (modelValue) {
                var i,length,value;
                length = scope.values.length;

                var isvalid = angular.isDefined(modelValue) && modelValue != null && (modelValue.length >= 8)
                if(isvalid)
                {
                    isvalid = modelValue.match(/[a-z|A-Z]/i) != null&&
                             modelValue.match(/[0-9]/i) !=null &&
                    modelValue.match(/qwerty|wertyu|ertyui|rtyuio|tyuiop|asdfgh|sdfghj|dfghjk|fghjkl|zxcvbn|xcvbnm|123456|234567|345678|456789|567890/i) == null;

                    if(isvalid) {
                        for (i = 0; i < length; i++) {
                            value = scope.values[i];
                            if (value != null && angular.isDefined(value)&&value.length > 0) {
                                isvalid = modelValue.search(value) == -1
                                if (!isvalid) {
                                    break;
                                }
                            }
                        }
                    }

                }

                if(scope.editMode == true)
                    isvalid = modelValue == Constants.DEFAULT_PASSWORD;


                return isvalid;
            };
            scope.$watchCollection("values", function () {
                ngModel.$validate();
            });
        }
    }
}]);