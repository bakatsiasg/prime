/**
 * Created by georgefloros on 5/27/15.
 */
'use strict';
directives.directive('closeKeyboardOnNext',['$rootScope','$log','$cordovaKeyboard','$timeout',function($rootScope,$log,$cordovaKeyboard,$timeout) {
    return {
        restrict: 'A',
        scope: true,
        link: function($scope, element, attrs) {

            if(ionic.Platform.isIOS())
            return;

            var close = function(){
                ionic.trigger('resetScrollView', {
                    target:  element[0]
                }, true);
            };

            element[0].addEventListener("blur",function(event){
                   close();
            },false);

            element[0].addEventListener("keydown",function(event){
                if(event.keyCode == 9)
                {
                    close();
                }
            },false);

        }
    }}]);