/**
 * Created by georgefloros on 12/3/14.
 */
directives.directive('sbDateSelect', [function () {

  var template = [
    '<div class="birthday row" >',
    '<label class="item item-input item-select prime-input prime-select-item-input"><select ng-disabled="isDisable" class="" ng-class="selectClass" ng-model="val.date" ng-options="d for d in dates">',
    '<option value="" disabled selected>{{"DAY"|translate}}</option>',
    '</select></label>',
    '<label class="item item-input item-select prime-input prime-select-item-input"><select ng-disabled="isDisable" class="" ng-class="selectClass" ng-model="val.month" ng-options="m.value as m.name for m in months">',
    '<option value="" disabled>{{"MONTH"|translate}}</option>',
    '</select></label>',
    '<label ng-show="showYears" class="item item-input item-select prime-input prime-select-item-input"><select  ng-disabled="isDisable" ng-if="showYears" class="" ng-class="selectClass" ng-model="val.year" ng-options="y for y in years">',
    '<option value="" disabled selected>{{"YEAR"|translate}}</option>',
    '</select></label>',
    '</div>'
  ];

  return {
    restrict: 'A',
    replace: true,
    template: template.join(''),
    require: 'ngModel',
    scope: {
      selectClass: '@sbSelectClass',
      showYears:'=showYears',
      isDisable :'=isDisable'
    },
    link: function(scope, elem, attrs, model) {
      scope.val = {};

      moment().format('DD/MM/YYYY');
      var min = scope.min = moment(attrs.min || '1910-01-01');
      var max = scope.max = moment(attrs.max); // Defaults to now

      //attrs.$observe('showYears', function() {
      //
      //  scope.showYears = attrs.showyears ? JSON.parse(attrs.showyears.toLowerCase()):true;
      //
      //});

      scope.showYears;// =  attrs.showyears ?attrs.showyears : true;
      scope.years = [];

      var i = max.year();
      var lenght = min.year();

        for (; i >= lenght; i--) {
          scope.years.push(i);
        }

      if(!scope.showYears)scope.val.year = '2000';

      scope.$watch('val.year', function () {
        updateMonthOptions();
      });

      scope.$watchCollection('[val.month, val.year]', function () {
        updateDateOptions();
      });

      scope.$watchCollection('[val.date, val.month, val.year]', function () {
        if (scope.val.year && scope.val.month && scope.val.date) {
          var m = moment([scope.val.year, scope.val.month-1, scope.val.date]);
          model.$setViewValue(m.format('DD/MM/YYYY'));
        }
        else {
          model.$setViewValue();
        }
      });

      function updateMonthOptions () {

        scope.months = [];

        var minMonth = scope.val.year && min.isSame([scope.val.year], 'year') ? min.month() : 0;
        var maxMonth = scope.val.year && max.isSame([scope.val.year], 'year') ? max.month() : 11;

        var monthNames = moment.months();

        for (var j=minMonth; j<=maxMonth; j++) {
          scope.months.push({
            name: monthNames[j],
            value: j+1
          });
        }

        if (scope.val.month-1 > maxMonth || scope.val.month-1 < minMonth) delete scope.val.month;
      }

      function updateDateOptions (year, month) {
        var minDate, maxDate;

        if (scope.val.year && scope.val.month && min.isSame([scope.val.year, scope.val.month-1], 'month')) {
          minDate = min.date();
        } else {
          minDate = 1;
        }

        if (scope.val.year && scope.val.month && max.isSame([scope.val.year, scope.val.month-1], 'month')) {
          maxDate = max.date();
        } else if (scope.val.year && scope.val.month) {
          maxDate = moment([scope.val.year, scope.val.month-1]).daysInMonth();
        } else {
          maxDate = 31;
        }

        scope.dates = [];

        for (var i=minDate; i<=maxDate; i++) {
          scope.dates.push(i);
        }
        if (scope.val.date < minDate || scope.val.date > maxDate) delete scope.val.date;
      }

      // model -> view
      model.$render = function() {
        if (!model.$viewValue) return;

        var m = moment(model.$viewValue);

        scope.val = {
          year: m.year(),
          month: m.month()+1,
          date: m.date()
        };
      };
    }
  };
}]);
