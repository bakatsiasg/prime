/**
 * Created by georgefloros on 12/22/14.
 */
directives.directive('htmlEvents', function() {
        return {
            restrict: 'A',
            scope: { events: '=' },
            link:function(scope, element, attrs) {
                var i,l1;
                var obj;
                l1 = scope.events.length;
                for(i=0;i<l1;i++)
                {
                    obj = scope.events[i];
                    element.bind(obj.event, function() {
                        //call the function that was passed
                        scope.$apply(obj.method(obj.args));
                    });
                }
            }}
        });