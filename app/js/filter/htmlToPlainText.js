
/**
 * Created by georgefloros on 5/25/15.
 */
'use strict';
filters.filter('htmlToPlaintext', function() {
        return function(text) {
            return String(text).replace(/<[^>]+>/gm, '');
        }
    }
);