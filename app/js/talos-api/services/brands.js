'use strict';

/**
 * @ngdoc service
 * @name talos-api.Services.Brands
 * @description Brands Service
 */
talosApi.factory('Brands', [
        'TalosCore', 'TalosValues', 'Constants','ApplicationSettings','localStorageService',
        function( TalosCore, TalosValues, Constants, ApplicationSettings,localStorageService) {
            var Brands = {};
            Brands.brandsFilter = null;
            Brands.brandtags = null;

            //TODO ADD PAGING SUPPORT AND resourceTypeIds
            //works only for  Brands.get(0, -1, Constants.BRAND_STATUS_PUBLISHED, Constants.LANGUAGE_IDS, Constants.RESOURCE_TYPE_TITLE)
            Brands.get = function(from, to, statusIds, languageIds, resourceTypeIds,callback,forceDownload) {
                if(Brands.brandsFilter&&!forceDownload)
                {
                    callback(Brands.brandsFilter);
                }
                else{
                    var appsettingValue = ApplicationSettings.getValue(Constants.APPLICATION_SETTING_BRAND_FILTER_VERSION);
                    var storeVersion = localStorageService.get(Constants.APPLICATION_SETTING_BRAND_FILTER_VERSION);

                    if(appsettingValue == storeVersion&&!forceDownload) {
                        Brands.brandsFilter = localStorageService.get('brandsFilter');
                        callback(Brands.brandsFilter);
                    }
                    else
                    {

                        TalosCore.http2Talos(
                            'GET', '/brands',
                            { 'rangeFrom': from, 'rangeTo': to, 'statusIds': statusIds, 'languageIds': languageIds, 'resourceTypeIds': resourceTypeIds },
                            null).then(function(success) {
                                Brands.brandsFilter = success.data;
                                if(!forceDownload) {
                                    var l, j;
                                    l = Brands.brandsFilter.length;
                                    for (j = 0; j < l; j++) {
                                        Brands.brandsFilter[j].names = {};
                                        angular.forEach(Constants.LANGUAGE_IDS, function (language, languageKey) {
                                            Brands.brandsFilter[j].names[Constants['LANGUAGE_CODE_' + language]] = "";
                                        });
                                        angular.forEach(Brands.brandsFilter[j].resources, function (resource, resourceKey) {
                                            Brands.brandsFilter[j].names[resource.languageIsoCode] = resource.textResource;
                                        });
                                    }
                                    localStorageService.set('brandsFilter', Brands.brandsFilter);
                                    localStorageService.set(Constants.APPLICATION_SETTING_BRAND_FILTER_VERSION, appsettingValue);
                                }
                                callback(Brands.brandsFilter);
                            },function(error){
                                callback(null);
                            });
                    }


                }

            };

            //TODO ADD PAGING SUPPORT  AND resourceTypeIds

            Brands.getTags = function(from, to, languageIds, resourceTypeIds,callback,forceDownload) {

                if(Brands.brandtags&&!forceDownload)
                {
                    callback(Brands.brandtags);
                }
                else {                  
                        TalosCore.http2Talos(
                            'GET', '/brands/tags',
                            { 'rangeFrom': from, 'rangeTo': to, 'languageIds': languageIds, 'resourceTypeIds': resourceTypeIds },
                            null).then(function(success){
                                Brands.brandtags = success.data;
                                if(!forceDownload) {
                                    var l, j;
                                    l = Brands.brandtags.length;
                                    for (j = 0; j < l; j++) {
                                        Brands.brandtags[j].names = {};
                                        angular.forEach(Constants.LANGUAGE_IDS, function (language, languageKey) {
                                            Brands.brandtags[j].names[Constants['LANGUAGE_CODE_' + language]] = "";
                                        });
                                        angular.forEach( Brands.brandtags[j].resources, function (resource, resourceKey) {
                                            Brands.brandtags[j].names[resource.languageIsoCode] = resource.textResource;
                                        });
                                    }

                                    localStorageService.set('brandtags', Brands.brandtags);
                                }
                                callback(Brands.brandtags);
                            },function(error){
                                callback(null);
                            });
                    }                
            };
            return Brands;
        }
    ]
);
