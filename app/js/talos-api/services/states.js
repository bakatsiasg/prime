'use strict';

/**
 * @ngdoc service
 * @name talos-api.Services.Coupons
 * @description Coupons Service
 */
talosApi.factory('States',  [
    'TalosCore', 'TalosValues','localStorageService','ApplicationSettings','Constants','Resources','$filter',
    function( TalosCore, TalosValues,localStorageService,ApplicationSettings,Constants,Resources,$filter){
  
            
                
            var States = {};
        
            States.get = function(byCountry, includeResources){
                return TalosCore.requireSession(function() {
                    return TalosCore.http2Talos(
                        'GET', '/reference/states',
                            {'byCountry': byCountry, 'includeResources': includeResources}, 
                        null);                                                            
                });
            }; 
            
            States.setLocaleNames = function(lang)
            {
                    if( localStorageService.get('statesLang') == lang || States.states == null)
                        return;
                    angular.forEach(States.states, function (state, key) {
                    state.localeName  = state.resources[lang]['name'] || state.description;
                    });
                    localStorageService.set('state',States.states);
                    localStorageService.set('statesLang',lang);
                    
             }
                          
            return States;
                        
        }
    ]
);