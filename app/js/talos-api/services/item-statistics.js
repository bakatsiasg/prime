'use strict';

/**
 * @ngdoc service
 * @name talos-api.Services.ItemStatistics
 * @description Comments Service
 */
talosApi.factory('ItemStatistics', [
        'TalosCore', 'TalosValues', 
        function( TalosCore, TalosValues ) {
            var ItemStatistics = {};
            ItemStatistics.getSingle = function(itemTypeId, itemId, statisticTypeId) {
                return TalosCore.requireSession(function() {
                    return TalosCore.http2Talos(
                        'GET', '/item/' + itemTypeId + '/' + itemId + '/itemStatistics/' + statisticTypeId,
                        null,
                        null);                    
                });
            };
            
            ItemStatistics.getMultiple = function(input) {
                return TalosCore.requireSession(function() {
                    return TalosCore.http2Talos(
                        'POST', '/item/itemStatistics',
                        null,
                        {'statistics': input});                                    
                });
            }
            
            return ItemStatistics;
        }
    ]
);
