'use strict';

/**
 * @ngdoc service
 * @name talos-api.Services.Rating
 * @description Rating Service
 */
talosApi.factory('Rating', [
        'TalosCore', 'TalosValues', 
        function( TalosCore, TalosValues ) {
            var Rating = {};
            
            Rating.add = function(itemTypeId, itemId, ratingValue) {
                return TalosCore.requireSession(function() {
                    return TalosCore.http2Talos(
                        'POST', '/item/' + itemTypeId + '/' + itemId + '/rating/' + ratingValue,
                        null,
                        null);                                    
                });
            }

            Rating.getRatings =  function(itemTypeId) {
                return TalosCore.requireSession(function() {
                    return TalosCore.http2Talos(
                        'POST', '/item/' + itemTypeId + '/ratings/' + TalosValues.getUserId(),
                        null,
                        null);
                });
            }


            Rating.addMultiple = function(input) {
                return TalosCore.requireSession(function() {
                    return TalosCore.http2Talos(
                        'POST', '/item/ratings/' + TalosValues.getUserId(),
                        null,
                        input);                                    
                });
            }            
            
            return Rating;
        }
    ]
);
