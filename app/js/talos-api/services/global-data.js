'use strict';

/**
 * @ngdoc service
 * @name talos-api.Services.GlobalData
 * @description GlobalData Service
 */
talosApi.factory('GlobalData', [
        function() {
            var GlobalData = {
                'user': {}
            };
            
            return GlobalData;
        }
    ]
);
