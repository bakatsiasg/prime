'use strict';

/**
 * @ngdoc service
 * @name talos-api.Services.Metadata
 * @description Metadata Service
 */
talosApi.service('Metadata', [

        'Constants', 'TalosCore',

        function(Constants, TalosCore) {

            var getMetadata = this.getMetadata = function(itemId, itemTypeId) {
                return TalosCore.http2Talos(
                    'GET', '/item/' + itemTypeId + '/' + itemId + '/metadata',
                    {},
                    null);
            };
            
            var getMultipleMetadata = this.getMultipleMetadata = function(itemTypeId, itemIds) {
                return TalosCore.http2Talos(
                    'POST', '/item/metadata/search/' + itemTypeId,
                    null,
                    itemIds);
            };            
        }
    ]);
