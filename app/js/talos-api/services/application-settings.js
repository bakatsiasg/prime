'use strict';

/**
 * @ngdoc service
 * @name talos-api.Services.ApplicationSettings
 * @description ApplicationSettings Service
 */

talosApi.service('ApplicationSettings', [
        'TalosCore', 'TalosValues', 'Constants',
        function( TalosCore, TalosValues, Constants ) {
            var ApplicationSettings = {};

            ApplicationSettings.settings = {};
            ApplicationSettings.scopeTypes = Constants.APPLICATION_SETTING_SCOPE_TYPE_GAMETYPE;

           ApplicationSettings.getValue = function (key)
          {
            return ApplicationSettings.settings[key];
          }

          //TODO Add suport for settngs;
            ApplicationSettings.getApplication = function(applicationId, scopeTypes) {
                return TalosCore.http2Talos(
                    'GET', '/appsettings/app/' + applicationId,
                    { 'scopeTypes': scopeTypes },
                    null);
            };

            ApplicationSettings.get = function(scopeTypes,callback) {

              if(ApplicationSettings.settings[scopeTypes])
              {
                callback(ApplicationSettings.settings[scopeTypes])
              }
              else
              {
                TalosCore.requireSession(function() {
                  return TalosCore.http2Talos(
                    'GET', '/appsettings',
                    { 'scopeTypes': scopeTypes },
                    null);
                }).then(function (success)
                {
                  //TODO NA FIGEI
                  ApplicationSettings.settings[scopeTypes] = success;

                  var j = 0;
                  var l = success.data.length;
                  for (j; j < l; j++) {
                          ApplicationSettings.settings[success.data[j].key] = success.data[j].value;
                  }
                  callback(ApplicationSettings.settings[scopeTypes]);
                },function(error){
                  return callback(null);
                });
              }


            };

            return ApplicationSettings;
        }
    ]
);
