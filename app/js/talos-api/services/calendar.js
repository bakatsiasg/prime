'use strict';

/**
 * @ngdoc service
 * @name talos-api.Services.Calendar
 * @description Calendar Service
 */
talosApi.factory('Calendar', [
        'TalosCore', 'TalosValues', 'Constants',
        function( TalosCore, TalosValues, Constants ) {
            var Calendar = {};
            Calendar.get = function(from, to, eventCalendarIds, eventDateFrom, eventDateTo, includeStatistics) {
                return TalosCore.requireSession(function() {
                    return TalosCore.http2Talos(
                        'GET', '/eventCalendar/' + TalosValues.getUserId(),
                        { 'rangeFrom': from, 'rangeTo': to, 'eventCalendarIds': eventCalendarIds, 'eventDateFrom': eventDateFrom, 'eventDateTo': eventDateTo, 'includeStatistics': includeStatistics },
                        null);                    
                });
            };            
                                    
            return Calendar;
        }
    ]
);
