/**
 * Created by georgefloros on 11/28/14.
 */
talosApi.factory('UploadExperience', [
    'TalosCore', 'TalosValues', 'Constants',
    function( TalosCore, TalosValues, Constants ) {
      var UploadExperience = {}

      UploadExperience.upload = function(title,comment,filetype,file) {
        return TalosCore.requireSession(function() {
          return TalosCore.http2Talos(
            'POST', '/uploadedExperiences/' + TalosValues.getUserId(),
            null,
            {'title': title, 'comment': comment, 'file': file, 'fileType': filetype});
        });
      };

      UploadExperience.get = function(from, to, uploadedExperienceIds, includeStatistics) {
        return TalosCore.requireSession(function() {
          return TalosCore.http2Talos(
            'GET', '/uploadedExperiences/' + TalosValues.getUserId(),
            {'rangeFrom': from, 'rangeTo': to,'friendsOnly':false,'active':true, 'includeStatistics': includeStatistics, 'uploadedExperienceIds': uploadedExperienceIds},
            null);
        });
      };


      return UploadExperience;
    }
  ]
);
