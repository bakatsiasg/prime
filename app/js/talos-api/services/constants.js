'use strict';


talosApi.constant('Constants', {
      /* SOS always on top for grunt  change*/
        // 'CLIENT_TYPE': 6,//clientType for grunt to replace.
        // 'APPLICATION_ID':"09A0226E-A2B0-4300-D255-F01AA5F0A49F",//appId for grunt to replace.
        // 'MAGIC_KEY':"eGtJTmjaUJgDGxgVCMa+HyB7oG2s1u3YMmlIeyf5rfc=",//magicKey for grunt to replace.
        // 'OS_TYPE':"android",//OS_TYPE for grunt to replace.
        'CLIENT_TYPE': "",//clientType for grunt to replace.
        'APPLICATION_ID':"",//appId for grunt to replace.
        'MAGIC_KEY':"",//magicKey for grunt to replace.
        'OS_TYPE':"",//OS_TYPE for grunt to replace.
        //'Google_Analytics_ID':"UA-57562076-3",//analytics id for grunt to replace.
        /* SOS always on top for grunt  change*/

        'SERVER':'',
        'API_VERSION':'',
        'RESOURCE_URL':'',
        'USER_AVATAR_IMAGE_URL':'',
        'TRANSLATE_URL':'',
        'TRANSLATE_URL_SUFFIX':'',
        'TERMS_URL':'',
        "FB_APP_REQUEST_URI":"",
        "HOW_TO_URL":"",
        "FLOOR_IMAGE_URL":"",
        "APP_STORE_URL":"",
        "GOOGLE_PLAY_URL":"",
        ///Prod
        //'SERVER' :"https:\/\/mallbg.saicongames.com/talos/v1",
        //'RESOURCE_URL':'https:\/\/mallbg.saicongames.com/Game_Server_Web/getResource',
        //'USER_AVATAR_IMAGE_URL' :"https:\/\/mallbg.saicongames.com/pub/avatars/$USERID.jpg",
        //// DEV
        //'SERVER':"https:\/\/dev.saicongames.com/talos/v1",
        //'RESOURCE_URL':'https:\/\/dev.saicongames.com/Game_Server_Web/getResource',
        //'USER_AVATAR_IMAGE_URL':"https:\/\/dev.saicongames.com/pub/avatars/$USERID.jpg",
        //
         //"09A022'$clientTypeId'E-A2B0-4300-D255-F01AA5F0A49F",

        //'USERGROUP_ID': "B5F8DC38-973E-45FD-DAE1-D1357442AFBE", mall
        'USERGROUP_ID': "43A0A196-6944-4EE7-EA72-F02EC001A18C",
        //'GAME_TYPE': "1B5C85C5-BE78-432E-C258-131FE6828889",
        'GAME_TYPE': "E9998FE4-6A4F-45DA-B4BD-7569488D6053",
        'TOKEN_EXPIRATION_SEC': "2678400",
        'PAGE_SIZE': 8,
        'COMMENTS_SIZE': 20,

        'HEADER_XTalosItemCount': "X-Talos-Item-Count",
        'DEFAULT_BRAND_SERVICE_ID': "5A28B062-5043-47AF-D8A4-729BB0D73A7C",
        'WEB_APP_BODY_HEIGHT': 720,//should be 872
        'DEFAULT_AVATAR':'img/avatar.jpg',
         /*Notifications*/
        'MESSAGES_URL_TYPE': 0,
        'MESSAGES_IMAGE_TYPE': 1,
        'MESSAGES_READ_STATUS': 1,
        'MESSAGES_UNREAD_STATUS': 0,
        /* Item Types */
        //'ITEM_TYPE_CLIENT_APPLICATION':5, mall
        'ITEM_TYPE_CLIENT_APPLICATION':145,
        'ITEM_TYPE_BRAND': 106,

        'ITEM_TYPE_BRAND_STORE': 108,
        'ITEM_TYPE_BRAND_PRODUCT': 107,
        'ITEM_TYePE_OFFER': 133,
        'ITEM_TYPE_CRITERION': 75,
        'ITEM_TYPE_CATEGORY': 67,
        'ITEM_TYPE_TOURNAMENT_RESOURCES': 30,
        'ITEM_TYPE_ACHIEVEMENT_TYPE': 47,
        'ITEM_TYPE_CUSTOM_USER_PROFILE': 56,
        'ITEM_TYPE_EVENT_CALENDAR': 135,
        'ITEM_TYPE_UPLOADED_EXPERIENCES': 137,
        'ITEM_TYPE_BRAND_PRODUCT_COUPON': 121,
        'ITEM_TYPE_BRAND_FEED': 141,
        'ITEM_TYPE_COUNTRY':110,
        'ITEM_TYPE_PREDEFINED_VALUE':78,
        /* Resource types */
        'RESOURCE_TYPE_IMAGE':4,
        'RESOURCE_TYPE_TITLE':250,
        'RESOURCE_TYPE_NAME':1,
        'RESOURCE_TYPE_IMAGE_FEATURED':231,
        'RESOURCE_TYPE_IMAGE_LARGE':212,
        'RESOURCE_TYPE_IMAGE_LARGE_SELECTED':222,
        'RESOURCE_TYPE_IMAGE_WEB':240,
        'RESOURCE_TYPE_IMAGE_URL':220,
        'RESOURCE_TYPE_DESCRIPTION':2,
        'RESOURCE_TYPE_HREF': 34,
        'RESOURCE_TYPE_THUMBNAIL':196,
        'RESOURCE_TYPE_THUMBNAIL_URL':3,
        'RESOURCE_TYPE_BRAND_STORE_WORKING_HOURS': 257,
        "RESOURCE_TYPE_COLOR": 256,
        "RESOURCE_TYPE_REWARD_DESCRIPTION": 207,
        "RESOURCE_TYPE_SCRIPT": 35,
        "RESOURCE_TYPE_PRICE":9,
        "RESOURCE_TYPE_STORY_TEXT":96,
        "RESOURCE_TYPE_POST_TO_WALL_TEXT":30,
        "RESOURCE_TYPE_POST_TO_WALL_TITLE":36,
        "RESOURCE_TYPE_POST_TO_WALL_PIC_URL":185,
        "RESOURCE_TYPE_POST_TO_TWITTER_MSG":187,
        "RESOURCE_TYPE_TOURNAMENT_WINNERS_URL":68,
        "RESOURCE_TYPE_ALLOW_GUEST":258,
        "RESOURCE_TYPE_ACTION":173,
        "RESOURCE_TYPE_VITEM_ID":98,
        "POST_TO_TWITTER_SHORT_URL":186,
        "POST_TO_TWITTER_THUMBNAIL":196,
        "RESOURCE_TYPE_TEXT":29,
        /* Metadata keys */
        'METADATA_KEY_NAME_BRAND_STORE_EMAIL': "BrandStore.email",
        'METADATA_KEY_NAME_CONTACT_DETAILS_EMAIL': "YD.contactDetailsEmail",
        'METADATA_KEY_NAME_REQUIRED_LEVEL': "requiredLevel",
        'METADATA_KEY_PERSONAL_PARKING_PLACE': "Personal.parkingPlace",
        'METADATA_KEY_PROFESSIONS':"B3142BC9-51EE-4DE0-E074-633BD16C4F02",
        'APP_LANGUAGE_IDS': 31,

        /*
        FACEBOOK APP ID MUST ALSO SET UP :
            android : mallMore/plugins/android.json
            web : mallMore/app/js/facebookConnectPlugin.js
            routes.js
        */
        'FACEBOOK_APP_ID' : '1541556079413093',
        'TWITTER_APP_ID' : '190650911',
        'EXTERNAL_SYSTEM_FACEBOOK':1,
        'EXTERNAL_SYSTEM_TWITTER':2,
        'TWITTER_PROFILE_URL': 'https:\/\/api.twitter.com/1.1/account/verify_credentials.json',
        'FACEBOOK_PROFILE_URL': '',
        /* Languages */
        'LANGUAGE_IDS': Array(1, 2),
        'LANGUAGE_CODE_2': 'en',
        'LANGUAGE_CODE_1': 'el',
        'LANGUAGE_ID_en': 2,
        'LANGUAGE_ID_el': 1,
        /* Unit types */
        'UNIT_TYPE_SCORE_POINTS':49,
        // 'UNIT_TYPE_POINTS': 56, mall
        // 'UNIT_TYPE_LEVEL_POINTS': 57, mall
        'UNIT_TYPE_POINTS': 74,
        'UNIT_TYPE_LEVEL_POINTS': 75,
        //
        'UNIT_TYPE_MORE_GAME_COINS':55,
        'UNIT_TYPE_MORE_CHECKINS':58,
        /* Statistics Types */
        "STATISTIC_TYPE_ITEM_WISHLIST_ADDED": 1,
        "STATISTIC_TYPE_ITEM_WISHLIST_REMOVED": 2,
        "STATISTIC_TYPE_ITEM_DISLIKE": 3,
        "STATISTIC_TYPE_ITEM_PURCHASED": 4,
        "STATISTIC_TYPE_COUPON_RATED": 5,
        "STATISTIC_TYPE_MERCHANT_RATED": 6,
        "STATISTIC_TYPE_ITEM_FAVORITES_ADDED": 7,
        "STATISTIC_TYPE_ITEM_FAVORITES_REMOVED": 8,
        "STATISTIC_TYPE_ITEM_LIKE": 9,
        "STATISTIC_TYPE_ITEM_RATED": 10,
        "STATISTIC_TYPE_ITEM_COMMENTED": 11,
        "STATISTIC_TYPE_BRAND_STORE_PER_COUPON_RATED": 12,
        "STATISTIC_TYPE_BRAND_PRODUCTS_PER_BRAND_STORE" : 13,
        /* Coupon Purchase Error Codes */
        "COUPON_PURCHASE_USER_SESSION_PERMISSION_ERROR": "1048",
        "COUPON_PURCHASE_NOT_ENOUGH_BALANCE_FOR_PURCHASE_ERROR": "2006",
        "COUPON_PURCHASE_INSUFFICIENT_BALANCE_FOR_PURCHASE_ERROR": "6404",
        /* Maps */
        "MALL_MAP_LAT": "42.66057",
        "MALL_MAP_LONG": "23.38314",
        /* Coupon status */
        "COUPON_STATUS_PURCHASED": 2,
        "COUPON_STATUS_EXPIRED": 5,
        "COUPON_STATUS_EXPIRED_CLEARED": 13,
        "COUPON_STATUS_EXPIRED_CLEARED_FAILED": 15,
        "COUPON_STATUS_REDEEMED": 3,
        "COUPON_STATUS_REDEEMED_CLEARED": 10,
        "COUPON_STATUS_REDEEMED_CLEARED_FAILED": 16,
        /* Categories */
        "CATEGORY_COUPONS":222,
        //"CATEGORY_HOME":221, //Mallbg
        "CATEGORY_HOME":379, 
        "CATEGORY_THE_MALL":225,
        "CATEGORY_BLOGS":235,
        /* Categories for feeds ,no cms related*/
        "CATEGORY_LOTTERIES": 203,
        "CATEGORY_COMPETITIONS": 452,
        "CATEGORY_POLLS": 451,
        "CATEGORY_GAMES": 209,

        //Alfapastry
        //  "CATEGORY_LOTTERIES": 359,
        //  "CATEGORY_COMPETITIONS": 357,
        // "CATEGORY_POLLS": 356,
        // "CATEGORY_GAMES": 209,
        
        "TOURNAMENT_TYPE_COMPETITION":1,
        "TOURNAMENT_TYPE_MISSION":2,
        "TOURNAMENT_TYPE_LOTTERY":3,
        "TOURNAMENT_TYPE_LEADERBOARD":0,
        
          
        "CATEGORY_FASHION": 222,
        "CATEGORY_FOOD": 223,
        "CATEGORY_ENTERTAINMENT": 224,
        "CATEGORY_BEAUTY": 225,
        "CATEGORY_OFFERS": 226,
         
        "PRODUCTS_CATEGORY_ID" : 301,


        "GAMES_STATUS_ALL":0,
        "GAMES_STATUS_ACTIVE":1,
        "GAMES_STATUS_EXPIRED":2,
        "TOURNAMENT_PARTICIPATION_STATUS_REGISTERED": 2,
        "TOURNAMENT_PARTICIPATION_STATUS_AUTO_REGISTERED": 6,
        "FAVORITES_STATUS_ACTIVE": 1,
        "OAUTH_KEY": "bZGKcaMYLzHKrMuw9c15oC6secM",
        "EXTERNAL_POST_RESULT_OK": 0,
        "EXTERNAL_POST_RESULT_NO_PERMISSION_TO_POST": 2,
        "LATEST_NOTIFICATION_MAX_LENGTH": 80,
        "USER_QR_CODE_WIDTH": 250,
        "USER_QR_CODE_HEIGHT": 250,
         "RANGING_TYPE":1,
        "BRAND_PRODUCT_TYPE_DEAL": "43C0B40B-CA17-423E-98B8-F43461932010",
        "BRAND_STATUS_PUBLISHED": "6785EDE3-E3A6-4C5D-A12B-D364EB060FD4",
        "BRAND_STORE_STATUS_PUBLISHED": "DD9F38D2-8572-427E-A0C6-15B818BCB312",
        "BRAND_PRODUCT_STATUS_PUBLISHED": "55FD490E-EBA9-4EFC-ECCD-98319CDACEBC",
        "APPLICATION_SETTING_SCOPE_TYPE_GAMETYPE": 6,
        "APPLICATION_SETTING_BRAND_TAG_IDS": "BrandTagIds",
        "APPLICATION_SETTING_COUNTRIES_VERSION":"MallBGCountriesVersion",
        "APPLICATION_SETTING_PROFESSION_VERSION":"MallBGProfessionVersion",
        "APPLICATION_SETTING_MALL_COUNTRIES":"includedCountriesForMall",
        "APPLICATION_SETTING_COUPONS_RESOURCES_VERSION":"PrimeCouponsResourcesVersion",
        "APPLICATION_SETTING_TOURNAMENT_RESOURCES_VERSION":"PrimeTournamentResourcesVersion",
        "APPLICATION_SETTING_BRAND_FILTER_VERSION":"PrimeBrandFilterVersion",
        "APPLICATION_SETTING_HOME_CATEGORIES_VERSION":"PrimeHomeCategoriesVersion",
        
        "USER_STATS_UNREAD_USER_MESSAGES": "unreadUserMessages",
        "EVENT_TYPE_FRIEND_INVITATION_BY_EMAIL": 62,
        "EVENT_TYPE_CHECKIN_SUBMITTED": 90,
        "EVENT_TYPE_BRAND_STORE_SHARE_BY_EMAIL": 13040,
        "EVENT_TYPE_BRAND_PRODUCT_SHARE_BY_EMAIL": 13041,
        "EVENT_TYPE_EVENT_CALENDAR_SHARE_BY_EMAIL": 13042,
        "EVENT_TYPE_CATEGORY_ITEM_SHARE_BY_EMAIL": 13043,
        "EVENT_TYPE_LOTTERY_ITEM_SHARE_BY_EMAIL": 13044,

        "EVENT_TYPE_GAME_ITEM_SHARE_BY_EMAIL": 13049,

        "EVENT_TYPE_COMPETITION_ITEM_SHARE_BY_EMAIL": 13053,
        "EVENT_TYPE_POLL_ITEM_SHARE_BY_EMAIL": 13052,

        "EVENT_TYPE_SEND_COUPON_AS_GIFT": 13045,
        "EVENT_TYPE_OFFER_SHARE_BY_EMAIL": 13046,
        "EVENT_TYPE_EXPRERIENCE_SHARE_BY_EMAIL": 13047,
        "EVENT_TYPE_RECOMMEND_COUPON": 13048,
        "CLIENT_TYPE_ANDROID": 6,
         "CLIENT_TYPE_IOS":3,
        "CLIENT_TYPE_PORTAL":4,
        "MALL_BRAND_STORE_ID": "C075376B-8A44-45A4-9AD7-E469B4F79495",
        "APPLICATION_SETTING_PARKING_FLOORS": "ParkingFloors",
        "APPLICATION_SETTING_PARKING_FLOOR_ZONES": "ParkingFloorZones_",
        "Google_Project_ID":"685991175651",
        
        "stateProvider" : ""

    });
