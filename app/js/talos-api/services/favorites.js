'use strict';

/**
 * @ngdoc service
 * @name talos-api.Services.Favorites
 * @description Favorites Service
 */
talosApi.factory('Favorites', [
        'TalosCore', 'TalosValues',
        function( TalosCore, TalosValues ) {
            var Favorites = {};
            Favorites.get = function(from, to, userId, statusIds, itemTypeIds) {
                return TalosCore.requireSession(function() {
                    return TalosCore.http2Talos(
                        'GET', '/favorites/' + ((userId == null)? TalosValues.getUserId(): userId),
                        {'rangeFrom': from, 'rangeTo': to, 'itemTypeIds': itemTypeIds, 'statusIds': statusIds},
                        null);
                });
            };
            
            Favorites.add = function(itemTypeId, itemId) {
                console.log("Add Favorite");
                return TalosCore.requireSession(function() {
                    return TalosCore.http2Talos(
                        'POST', '/favorites/' + TalosValues.getUserId() + '/add/' + itemTypeId + '/' + itemId,
                        null,
                        null);
                });
            }

            Favorites.remove = function(favoriteId) {
                return TalosCore.requireSession(function() {
                    return TalosCore.http2Talos(
                        'POST', '/favorites/' + TalosValues.getUserId() + '/remove/' + favoriteId,
                        null,
                        null);
                });
            }

            return Favorites;
        }
    ]
);
