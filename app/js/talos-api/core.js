'use strict';

/**
 * @ngdoc service
 * @name talos-api.Services.Taloscore
 * @description Taloscore Service
 */
String.prototype.getBytes = function() {
    var bytes = [];
    for (var i = 0; i < this.length; i++) {
        var charCode = this.charCodeAt(i);
        var cLen = Math.ceil(Math.log(charCode) / Math.log(256));
        for (var j = 0; j < cLen; j++) {
            bytes.push((charCode << (j * 8)) & 0xFF);
        }
    }
    return bytes;
}

talosApi.constant('NetworkCodes', {
    "OK":200,
    "ERROR":500,
    "NO_CONNECTION":0,
    "Service_UNAVAILABLE":503,
    "USER_NOT_FOUND":1057,
    "GUEST_EXISTS":1051,
    "MISSING_PARAMETER":1052,
    "FAR_FROM_DEPARTMENT":10001,
    "EXCEEDS_DAILY_LIMIT_OF_CHECKINS":10002,
    "PRIZE_NOT_FOUND_OR_MORE_ONE_FOUND":10003,
    "USER_NOT_IN_AVAILABLE_UNIVERSITY_POD":10004,
    "PRIZE_WON":1,
    "PRIZE_NOT_WON":2,
    "PRIZE_ALREADY_WON":3,
    "NICKNAME_ALREADY_USED":1050,
    "USERNAME_ALREADY_USED":1014,
    "EMAIL_ALREADY_USED": 1020,
    "LOCKED_USER":1055,
    "USER_PASSWORD_INVALID":1056,
    "USER_MUST_BE_ACTIVATED":1059,
     "WRONG_USERNAME_OR_PASSWORD":1049,
    "USER_USERNAME_INVALID":1022
});
/*
 *
 *http:\/\/blog.overzealous.com/post/55829457993/natural-sorting-within-angular-js
 */
talosApi.factory("naturalService", ["$locale", function($locale) {
    "use strict";
    // the cache prevents re-creating the values every time, at the expense of
    // storing the results forever. Not recommended for highly changing data
    // on long-term applications.
    var natCache = {},
    // amount of extra zeros to padd for sorting
        padding = function(value) {
            return "00000000000000000000".slice(value.length);
        },

    // Converts a value to a string.  Null and undefined are converted to ''
        toString = function(value) {
            if(value === null || value === undefined) return '';
            return ''+value;
        },

    // Calculate the default out-of-order date format (dd/MM/yyyy vs MM/dd/yyyy)
        natDateMonthFirst = $locale.DATETIME_FORMATS.shortDate.charAt(0) === "M",
    // Replaces all suspected dates with a standardized yyyy-m-d, which is fixed below
        fixDates = function(value) {
            // first look for dd?-dd?-dddd, where "-" can be one of "-", "/", or "."
            return toString(value).replace(/(\d\d?)[-\/\.](\d\d?)[-\/\.](\d{4})/, function($0, $m, $d, $y) {
                // temporary holder for swapping below
                var t = $d;
                // if the month is not first, we'll swap month and day...
                if(!natDateMonthFirst) {
                    // ...but only if the day value is under 13.
                    if(Number($d) < 13) {
                        $d = $m;
                        $m = t;
                    }
                } else if(Number($m) > 12) {
                    // Otherwise, we might still swap the values if the month value is currently over 12.
                    $d = $m;
                    $m = t;
                }
                // return a standardized format.
                return $y+"-"+$m+"-"+$d;
            });
        },

    // Fix numbers to be correctly padded
        fixNumbers = function(value) {
            // First, look for anything in the form of d.d or d.d.d...
            return value.replace(/(\d+)((\.\d+)+)?/g, function ($0, integer, decimal, $3) {
                // If there's more than 2 sets of numbers...
                if (decimal !== $3) {
                    // treat as a series of integers, like versioning,
                    // rather than a decimal
                    return $0.replace(/(\d+)/g, function ($d) {
                        return padding($d) + $d;
                    });
                } else {
                    // add a decimal if necessary to ensure decimal sorting
                    decimal = decimal || ".0";
                    return padding(integer) + integer + decimal + padding(decimal);
                }
            });
        },

    // Finally, this function puts it all together.
        natValue = function (value) {
            if(natCache[value]) {
                return natCache[value];
            }
            natCache[value] = fixNumbers(fixDates(value));
            return natCache[value];
        };

    // The actual object used by this service
    return {
        naturalValue: natValue,
        naturalSort: function(a, b) {
            a = natValue(a);
            b = natValue(b);
            return (a < b) ? -1 : ((a > b) ? 1 : 0);
        }
    };
}])
/* End * /

 /*
 *
 *https:\/\/github.com/yrezgui/kodigon/
 *
 * */

talosApi.factory('base64', ['$window', function($window) {
    return {

        name: 'Base64',
        readonly: false,

        encode: function(input) {
            return $window.btoa(input);
        },

        decode: function(input) {
            return $window.atob(input);
        }

    };
}]);

talosApi.factory('sha1', function() {
    return {

        name: 'SHA1',
        readonly: true,

        encode: function(input) {
            function rotate_left(n,s) {
                var t4 = ( n<<s ) | (n>>>(32-s));
                return t4;
            };

            function lsb_hex(val) {
                var str="";
                var i;
                var vh;
                var vl;

                for( i=0; i<=6; i+=2 ) {
                    vh = (val>>>(i*4+4))&0x0f;
                    vl = (val>>>(i*4))&0x0f;
                    str += vh.toString(16) + vl.toString(16);
                }
                return str;
            };

            function cvt_hex(val) {
                var str="";
                var i;
                var v;

                for( i=7; i>=0; i-- ) {
                    v = (val>>>(i*4))&0x0f;
                    str += v.toString(16);
                }
                return str;
            };


            function Utf8Encode(input) {
                input = input.replace(/\r\n/g,"\n");
                var utftext = "";

                for (var n = 0; n < input.length; n++) {

                    var c = input.charCodeAt(n);

                    if (c < 128) {
                        utftext += String.fromCharCode(c);
                    }
                    else if((c > 127) && (c < 2048)) {
                        utftext += String.fromCharCode((c >> 6) | 192);
                        utftext += String.fromCharCode((c & 63) | 128);
                    }
                    else {
                        utftext += String.fromCharCode((c >> 12) | 224);
                        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                        utftext += String.fromCharCode((c & 63) | 128);
                    }

                }

                return utftext;
            };

            var blockstart;
            var i, j;
            var W = new Array(80);
            var H0 = 0x67452301;
            var H1 = 0xEFCDAB89;
            var H2 = 0x98BADCFE;
            var H3 = 0x10325476;
            var H4 = 0xC3D2E1F0;
            var A, B, C, D, E;
            var temp;

            input = Utf8Encode(input);

            var input_len = input.length;

            var word_array = new Array();
            for( i=0; i<input_len-3; i+=4 ) {
                j = input.charCodeAt(i)<<24 | input.charCodeAt(i+1)<<16 |
                input.charCodeAt(i+2)<<8 | input.charCodeAt(i+3);
                word_array.push( j );
            }

            switch( input_len % 4 ) {
                case 0:
                    i = 0x080000000;
                    break;
                case 1:
                    i = input.charCodeAt(input_len-1)<<24 | 0x0800000;
                    break;

                case 2:
                    i = input.charCodeAt(input_len-2)<<24 | input.charCodeAt(input_len-1)<<16 | 0x08000;
                    break;

                case 3:
                    i = input.charCodeAt(input_len-3)<<24 | input.charCodeAt(input_len-2)<<16 | input.charCodeAt(input_len-1)<<8	| 0x80;
                    break;
            }

            word_array.push( i );

            while( (word_array.length % 16) != 14 ) word_array.push( 0 );

            word_array.push( input_len>>>29 );
            word_array.push( (input_len<<3)&0x0ffffffff );


            for ( blockstart=0; blockstart<word_array.length; blockstart+=16 ) {

                for( i=0; i<16; i++ ) W[i] = word_array[blockstart+i];
                for( i=16; i<=79; i++ ) W[i] = rotate_left(W[i-3] ^ W[i-8] ^ W[i-14] ^ W[i-16], 1);

                A = H0;
                B = H1;
                C = H2;
                D = H3;
                E = H4;

                for( i= 0; i<=19; i++ ) {
                    temp = (rotate_left(A,5) + ((B&C) | (~B&D)) + E + W[i] + 0x5A827999) & 0x0ffffffff;
                    E = D;
                    D = C;
                    C = rotate_left(B,30);
                    B = A;
                    A = temp;
                }

                for( i=20; i<=39; i++ ) {
                    temp = (rotate_left(A,5) + (B ^ C ^ D) + E + W[i] + 0x6ED9EBA1) & 0x0ffffffff;
                    E = D;
                    D = C;
                    C = rotate_left(B,30);
                    B = A;
                    A = temp;
                }

                for( i=40; i<=59; i++ ) {
                    temp = (rotate_left(A,5) + ((B&C) | (B&D) | (C&D)) + E + W[i] + 0x8F1BBCDC) & 0x0ffffffff;
                    E = D;
                    D = C;
                    C = rotate_left(B,30);
                    B = A;
                    A = temp;
                }

                for( i=60; i<=79; i++ ) {
                    temp = (rotate_left(A,5) + (B ^ C ^ D) + E + W[i] + 0xCA62C1D6) & 0x0ffffffff;
                    E = D;
                    D = C;
                    C = rotate_left(B,30);
                    B = A;
                    A = temp;
                }

                H0 = (H0 + A) & 0x0ffffffff;
                H1 = (H1 + B) & 0x0ffffffff;
                H2 = (H2 + C) & 0x0ffffffff;
                H3 = (H3 + D) & 0x0ffffffff;
                H4 = (H4 + E) & 0x0ffffffff;
            }

            var temp = cvt_hex(H0) + cvt_hex(H1) + cvt_hex(H2) + cvt_hex(H3) + cvt_hex(H4);

            return temp.toLowerCase();
        }
    };
});

/* END */
/*
 https://gist.github.com/macton/1743087
 */
talosApi.factory('hmac_sha1',['sha1',function(sha1) {
    var hexcase = 0;  /* hex output format. 0 - lowercase; 1 - uppercase        */
    var b64pad  = "="; /* base-64 pad character. "=" for strict RFC compliance   */

    function sha1_vm_test()
    {
        return hex_sha1("abc") == "a9993e364706816aba3e25717850c26c9cd0d89d";
    }

    /*
     * Calculate the SHA1 of a raw string
     */
    function rstr_sha1(s)
    {
        return binb2rstr(binb_sha1(rstr2binb(s), s.length * 8));
    }

    /*
     * Calculate the HMAC-SHA1 of a key and some data (raw strings)
     */
    function rstr_hmac_sha1(key, data)
    {
        var bkey = rstr2binb(key);
        if(bkey.length > 16) bkey = binb_sha1(bkey, key.length * 8);

        var ipad = Array(16), opad = Array(16);
        for(var i = 0; i < 16; i++)
        {
            ipad[i] = bkey[i] ^ 0x36363636;
            opad[i] = bkey[i] ^ 0x5C5C5C5C;
        }

        var hash = binb_sha1(ipad.concat(rstr2binb(data)), 512 + data.length * 8);
        return binb2rstr(binb_sha1(opad.concat(hash), 512 + 160));
    }

    /*
     * Convert a raw string to a hex string
     */
    function rstr2hex(input)
    {
        var hex_tab = hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
        var output = "";
        var x;
        for(var i = 0; i < input.length; i++)
        {
            x = input.charCodeAt(i);
            output += hex_tab.charAt((x >>> 4) & 0x0F)
            +  hex_tab.charAt( x        & 0x0F);
        }
        return output;
    }

    /*
     * Convert a raw string to a base-64 string
     */
    function rstr2b64(input)
    {
        var tab = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
        var output = "";
        var len = input.length;
        for(var i = 0; i < len; i += 3)
        {
            var triplet = (input.charCodeAt(i) << 16)
                | (i + 1 < len ? input.charCodeAt(i+1) << 8 : 0)
                | (i + 2 < len ? input.charCodeAt(i+2)      : 0);
            for(var j = 0; j < 4; j++)
            {
                if(i * 8 + j * 6 > input.length * 8) output += b64pad;
                else output += tab.charAt((triplet >>> 6*(3-j)) & 0x3F);
            }
        }
        return output;
    }

    /*
     * Convert a raw string to an arbitrary string encoding
     */
    function rstr2any(input, encoding)
    {
        var divisor = encoding.length;
        var remainders = Array();
        var i, q, x, quotient;

        /* Convert to an array of 16-bit big-endian values, forming the dividend */
        var dividend = Array(Math.ceil(input.length / 2));
        for(i = 0; i < dividend.length; i++)
        {
            dividend[i] = (input.charCodeAt(i * 2) << 8) | input.charCodeAt(i * 2 + 1);
        }

        /*
         * Repeatedly perform a long division. The binary array forms the dividend,
         * the length of the encoding is the divisor. Once computed, the quotient
         * forms the dividend for the next step. We stop when the dividend is zero.
         * All remainders are stored for later use.
         */
        while(dividend.length > 0)
        {
            quotient = Array();
            x = 0;
            for(i = 0; i < dividend.length; i++)
            {
                x = (x << 16) + dividend[i];
                q = Math.floor(x / divisor);
                x -= q * divisor;
                if(quotient.length > 0 || q > 0)
                    quotient[quotient.length] = q;
            }
            remainders[remainders.length] = x;
            dividend = quotient;
        }

        /* Convert the remainders to the output string */
        var output = "";
        for(i = remainders.length - 1; i >= 0; i--)
            output += encoding.charAt(remainders[i]);

        /* Append leading zero equivalents */
        var full_length = Math.ceil(input.length * 8 /
        (Math.log(encoding.length) / Math.log(2)))
        for(i = output.length; i < full_length; i++)
            output = encoding[0] + output;

        return output;
    }

    /*
     * Encode a string as utf-8.
     * For efficiency, this assumes the input is valid utf-16.
     */
    function str2rstr_utf8(input)
    {
        var output = "";
        var i = -1;
        var x, y;

        while(++i < input.length)
        {
            /* Decode utf-16 surrogate pairs */
            x = input.charCodeAt(i);
            y = i + 1 < input.length ? input.charCodeAt(i + 1) : 0;
            if(0xD800 <= x && x <= 0xDBFF && 0xDC00 <= y && y <= 0xDFFF)
            {
                x = 0x10000 + ((x & 0x03FF) << 10) + (y & 0x03FF);
                i++;
            }

            /* Encode output as utf-8 */
            if(x <= 0x7F)
                output += String.fromCharCode(x);
            else if(x <= 0x7FF)
                output += String.fromCharCode(0xC0 | ((x >>> 6 ) & 0x1F),
                    0x80 | ( x         & 0x3F));
            else if(x <= 0xFFFF)
                output += String.fromCharCode(0xE0 | ((x >>> 12) & 0x0F),
                    0x80 | ((x >>> 6 ) & 0x3F),
                    0x80 | ( x         & 0x3F));
            else if(x <= 0x1FFFFF)
                output += String.fromCharCode(0xF0 | ((x >>> 18) & 0x07),
                    0x80 | ((x >>> 12) & 0x3F),
                    0x80 | ((x >>> 6 ) & 0x3F),
                    0x80 | ( x         & 0x3F));
        }
        return output;
    }

    /*
     * Encode a string as utf-16
     */
    function str2rstr_utf16le(input)
    {
        var output = "";
        for(var i = 0; i < input.length; i++)
            output += String.fromCharCode( input.charCodeAt(i)        & 0xFF,
                (input.charCodeAt(i) >>> 8) & 0xFF);
        return output;
    }

    function str2rstr_utf16be(input)
    {
        var output = "";
        for(var i = 0; i < input.length; i++)
            output += String.fromCharCode((input.charCodeAt(i) >>> 8) & 0xFF,
                input.charCodeAt(i)        & 0xFF);
        return output;
    }

    /*
     * Convert a raw string to an array of big-endian words
     * Characters >255 have their high-byte silently ignored.
     */
    function rstr2binb(input)
    {
        var output = Array(input.length >> 2);
        for(var i = 0; i < output.length; i++)
            output[i] = 0;
        for(var i = 0; i < input.length * 8; i += 8)
            output[i>>5] |= (input.charCodeAt(i / 8) & 0xFF) << (24 - i % 32);
        return output;
    }

    /*
     * Convert an array of little-endian words to a string
     */
    function binb2rstr(input)
    {
        var output = "";
        for(var i = 0; i < input.length * 32; i += 8)
            output += String.fromCharCode((input[i>>5] >>> (24 - i % 32)) & 0xFF);
        return output;
    }

    /*
     * Calculate the SHA-1 of an array of big-endian words, and a bit length
     */
    function binb_sha1(x, len)
    {
        /* append padding */
        x[len >> 5] |= 0x80 << (24 - len % 32);
        x[((len + 64 >> 9) << 4) + 15] = len;

        var w = Array(80);
        var a =  1732584193;
        var b = -271733879;
        var c = -1732584194;
        var d =  271733878;
        var e = -1009589776;

        for(var i = 0; i < x.length; i += 16)
        {
            var olda = a;
            var oldb = b;
            var oldc = c;
            var oldd = d;
            var olde = e;

            for(var j = 0; j < 80; j++)
            {
                if(j < 16) w[j] = x[i + j];
                else w[j] = bit_rol(w[j-3] ^ w[j-8] ^ w[j-14] ^ w[j-16], 1);
                var t = safe_add(safe_add(bit_rol(a, 5), sha1_ft(j, b, c, d)),
                    safe_add(safe_add(e, w[j]), sha1_kt(j)));
                e = d;
                d = c;
                c = bit_rol(b, 30);
                b = a;
                a = t;
            }

            a = safe_add(a, olda);
            b = safe_add(b, oldb);
            c = safe_add(c, oldc);
            d = safe_add(d, oldd);
            e = safe_add(e, olde);
        }
        return Array(a, b, c, d, e);

    }

    /*
     * Perform the appropriate triplet combination function for the current
     * iteration
     */
    function sha1_ft(t, b, c, d)
    {
        if(t < 20) return (b & c) | ((~b) & d);
        if(t < 40) return b ^ c ^ d;
        if(t < 60) return (b & c) | (b & d) | (c & d);
        return b ^ c ^ d;
    }

    /*
     * Determine the appropriate additive constant for the current iteration
     */
    function sha1_kt(t)
    {
        return (t < 20) ?  1518500249 : (t < 40) ?  1859775393 :
            (t < 60) ? -1894007588 : -899497514;
    }

    /*
     * Add integers, wrapping at 2^32. This uses 16-bit operations internally
     * to work around bugs in some JS interpreters.
     */
    function safe_add(x, y)
    {
        var lsw = (x & 0xFFFF) + (y & 0xFFFF);
        var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
        return (msw << 16) | (lsw & 0xFFFF);
    }

    /*
     * Bitwise rotate a 32-bit number to the left.
     */
    function bit_rol(num, cnt)
    {
        return (num << cnt) | (num >>> (32 - cnt));
    }

    return {
        name: 'HMAC_SHA1',
        readonly: true,

        b64_sha1:function(s){
            return rstr2b64(rstr_sha1(str2rstr_utf8(s)));
        },
        str_hmac_sha1: function (key,message){
            return rstr_hmac_sha1(key, message)
        }
        ,
        b64_hmac_sha1: function (key,message){
            return rstr2b64(rstr_hmac_sha1(str2rstr_utf8(key), str2rstr_utf8(message)));
        }
    }
}]);


talosApi.service('TalosCore', [

    'Constants', 'TalosValues', '$http', '$rootScope','naturalService','sha1','base64','hmac_sha1',

    function( Constants, TalosValues, $http, $rootScope,naturalService,sha1,base64,hmac_sha1) {

        var authenticate = this.authenticate = function(username, password) {
            console.log('authenticate');
            return http2Talos('POST', '/users/authenticate',
                null,
                {
                    "userGroupId":Constants.USERGROUP_ID,
                    "username":username,
                    "password":password
                }
            ).then(function(authData) {
                    //console.log('authenticate : ' + authData)
                    handleSessionData(authData);
                    return getAuthKey()
                        .then(function (data) {
                            return authData;
                        })
                }, function(errorResponse) {
                    // propagate the error JSON for handling in UI layer
                    return errorResponse.data;
                });
        };



        var guest = this.guest = function() {
            console.log('guest');
            return http2Talos('POST', '/users/guest/getSession',
                null,
                {
                    "username":"",
                    "password":"",
                    "languageCode":TalosValues.getLanguage(),
                    "info":"",
                    "latitude":0,
                    "longitude":0,
                    "trackingInfo":null,
                    //"userGroupId":Constants.USERGROUP_ID,
                    "clientIPAddress":"",
                    "nationalityIsoCode":TalosValues.getLanguage(),
                    "clientTypeId":$rootScope.clientType
                }
            ).then(function(data) {
                    console.log('Guest : ' + data)
                    handleSessionData(data);
                    return data;
                });
        };
        
        

        var authenticateKey = this.authenticateKey  = function(authKey) {
            console.log('authenticateKey : ' + authKey);
            return http2Talos('POST', '/users/authenticateKey',
                null,
                {"authenticatedKey":authKey}
            ).then(function(data) {
                    //console.log('authenticateKey KEY : ' + data)
                    handleSessionData(data);
                    return data;
                }, function(data) {

                    handleSessionData(data);
                });
        };

        var handleSessionData = function (data) {
            console.log('handle Session Data : ' + JSON.stringify(data));
            if (data.status === 200) {
                var newUserId = data.data.userId
                var newUserEvent = !(newUserId.toUpperCase() === TalosValues.getUserId().toUpperCase());
                TalosValues.setUserId(newUserId);
                TalosValues.setUserSessionId(data.data.userSessionId);
                if (newUserEvent) {
                    $rootScope.$broadcast('NEW_USER');
                };
            } else {
                TalosValues.setUserId('');
                TalosValues.setUserSessionId('');
                TalosValues.setAuthKey('');
                guest();
            }
        }
        
        
        
        
        var getAuthKey = this.getAuthKey = function() {
            console.log('getAuthKey');
            return requireSession(function() {
                return http2Talos('POST', '/users/' + TalosValues.getUserId() + '/authKey',
                    null,
                    {"expiresSeconds":Constants.TOKEN_EXPIRATION_SEC}
                ).then(function(data) {
                        //console.log('authorization KEY : ' + data.data.authorizationKey)
                        TalosValues.setAuthKey(data.data.authorizationKey);
                        return data;
                    });
            });
        };

        var requireSession = this.requireSession = function ( actionRequireSession ) {

            if (TalosValues.getUserSessionId()) {
                console.log("Has Session");
                return actionRequireSession();
            } else {
//                    return this.authenticate('skataNaFas', '1qazXSW@')
//                        .then(function (data) {
//                            return actionRequireSession();
//                        })
                if (TalosValues.getAuthKey()) {
                    //console.log("TalosValues.getAuthKey() : " + TalosValues.getAuthKey());
                    return this.authenticateKey(TalosValues.getAuthKey())
                        .then(function (data) {
                            return actionRequireSession();
                        })
                } else {
                    var talosCore = this;
                    return this.guest()
                        .then(function (data) {
                            return talosCore.getAuthKey()
                                .then(function (data) {
                                    return actionRequireSession();
                                })
                        })
                }
            }

        }

        var getHeaders = function(params, data,path) {
            var currentTime = moment(new Date()).format("YYYY-MM-DTHH:mm:ss.SSSZZ");
            /* TODO : UPDATE SIGNATURE  */
            var values = new Array();
            values.push("uri=/"+Constants.API_VERSION+path);
            values.push("ts="+ encodeURIComponent(currentTime));

            if(params) {
                angular.forEach(params, function (item, key) {
                    console.log(item);
                    if(item == null || item == undefined)return;


                    if(item.constructor === Array)
                    {
                        var i = 0;
                        var l =  item.length;
                        for(;i<l;i++){
                            values.push(key + "=" + encodeURIComponent(item[i]));
                        }
                    }
                    else
                    {
                        values.push(key + "=" + encodeURIComponent(item));
                    }
                });
            }

            if(data){
                var json = JSON.stringify(data);
                var sha1_ = hmac_sha1.b64_sha1(json);
                //var base64_ = base64.encode(sha1_);
                var urlEnc = encodeURIComponent(sha1_);
                values.push("b=" +urlEnc);
                console.log(urlEnc);
            }


            values.sort();

            var finalString = "";
            var i = 0;
            var l = values.length;
            for(i;i<l;i++){
                finalString+=values[i]+"&";
            }
            finalString+="k="+Constants.MAGIC_KEY;
            console.log(finalString);
            var signature = hmac_sha1.b64_hmac_sha1(Constants.MAGIC_KEY,finalString);
            //signature = base64.encode(signature);
            console.log(signature);

            return {
                'X-Talos-Application-Id' : Constants.APPLICATION_ID,
                'X-Talos-UserGroup-Id' : Constants.USERGROUP_ID,
                'X-Talos-Timestamp' : currentTime,
                'X-Talos-Signature' : signature,
                'X-Talos-Session-Id' : TalosValues.getUserSessionId(),
                'Accept-Language': TalosValues.getLanguage(),
                'Content-Type': 'application/json;charset=utf-8'
            }
        }

        var http2Talos = this.http2Talos = function (method, path, params, data) {
            console.log("");

            var headers = getHeaders(params, data, path);

            console.log(headers);
            return $http({
                method: method,
                url: Constants.SERVER + path,
                headers: headers,
                params: params,
                data : data
            });
        }

    }
]);
