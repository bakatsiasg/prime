// Ionic Starter App
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
var app = angular.module('prime', ['ionic', 'prime.controllers', 'prime.services','prime.directives','prime.talos-api','prime.filters','pascalprecht.translate','LocalStorageModule','ngCordova','ngPatternRestrict']);
var controllers = angular.module('prime.controllers', []);
var services  = angular.module('prime.services', []);
var directives  = angular.module('prime.directives', []);
var talosApi  = angular.module('prime.talos-api',[]);
var filters = angular.module('prime.filters',[]);

app.run(['$ionicPlatform','$cordovaKeyboard','$cordovaStatusbar','$cordovaSplashscreen',function($ionicPlatform,$cordovaKeyboard,$cordovaStatusbar,$cordovaSplashscreen) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    ionic.Platform.fullScreen();
    ionic.Platform.isFullScreen = true;
    if (window.cordova && window.cordova.plugins.Keyboard) {
      $cordovaKeyboard.hideAccessoryBar(false);
      $cordovaKeyboard.disableScroll(ionic.Platform.isIOS());
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      $cordovaStatusbar.hide();
    }

    setTimeout(function() {
      if(navigator.splashscreen) {
        $cordovaSplashscreen.hide();
      }
    }, 500);


  });
  
}])
    .config(['$stateProvider','$urlRouterProvider','localStorageServiceProvider','$translateProvider','$ionicConfigProvider','$sceProvider','$logProvider',function($stateProvider, $urlRouterProvider,localStorageServiceProvider,$translateProvider,$ionicConfigProvider,$sceProvider,$logProvider) {


      localStorageServiceProvider.setPrefix('prime');
      $translateProvider.useLoader('translateLoader');
      $ionicConfigProvider.tabs.position("bottom");
      $ionicConfigProvider.navBar.alignTitle('center');
      $ionicConfigProvider.views.swipeBackEnabled(false);

      //if(ionic.Platform.isAndroid()) {
      //    //$ionicConfigProvider.scrolling.jsScrolling(false);
      //}
      $logProvider.debugEnabled(true);

      $sceProvider.enabled(false);
      $stateProvider
          
          // .state('ServerSelection',
          // {
          //   cache: false,
          //   url:'/',
          //   templateUrl:"templates/serverSelection.html",
          //   controller:'ServerSelectionCtrl'
          // })
          
          .state('loading',
          {
              cache: false,
              url:"/",
              templateUrl:"templates/loading.html",
              controller:'LoadingCtrl'
          })
          
          .state('login',
          {
              cache: false,
              url:"/login",
              templateUrl:"templates/loginMain.html",
              controller:'LoginCtrl'
          })
          

          .state('tab',
          {
            cache: false,
            url: '/tab',
            abstract: true,
            templateUrl:"templates/tabs.html"
          })
          .state('tab.home',
          {
            cache: false,
            url: '/home',
             views: {
              'tab-home': {
              templateUrl: 'templates/tab-home.html',
              controller: 'NewsCtrl'
              }
            }
           })
                   
          
          .state('tab.gifts', {
              url: '/gifts',
              views: {
                'tab-gifts': {
                  templateUrl: 'templates/tab-gifts-coupons.html',
                  controller: 'CouponsCtrl'
                }
              }
            })
      
          .state('tab.giftDetail', {
              url: '/gifts/:couponID',
              views: {
                'tab-gifts': {
                  templateUrl: 'templates/gifts-detail-coupon.html',
                  controller: 'CouponDetailsCtrl'
                }
              }
            })
          
          .state('tab.missions',
          {
            cache: false,
            url:"/missions",
             views: {
              'tab-missions': {
            templateUrl:"templates/tab-missions.html",
            //Dokimastika pernw ta badges gia thn wra anti missions
            controller:'BadgesCtrl'
            }
           }
          })
          
              
          .state('tab.campaigns',
          {
            cache: false,
            url:"/campaigns",
             views: {
              'tab-campaigns': {
            templateUrl:"templates/tab-campaigns.html",
            controller:'CampaignsCtrl'
            }
           }
          })
          
          
          .state('tab.poll',
          {
            url: '/campaigns/:pollID',
            views: {
              'tab-campaigns': {
                templateUrl: 'templates/poll.html',
                controller: 'PollsCtrl' 
              }
            }
          }) 
          
        .state('tab.competition',
          {
            url: '/campaigns/:competitionID',
            views: {
              'tab-campaigns': {
                templateUrl: 'templates/competition-details.html',
                controller: 'CompetitionDetailsCtrl'

              }
            }
          })  
          
           .state('tab.prime',
          {
            cache: false,
            url:"/prime",
             views: {
              'tab-prime': {
            templateUrl:"templates/tab-prime.html",
            controller:'PrimeCtrl'
            }
           }
          })
          
          .state('tab.information',
          {
            cache: false,
            url:"/information",
             views: {
              'tab-prime': {
            templateUrl:"templates/primeInfo.html",
            controller:'PrimeInfoCtrl'
            }
           }
          })
          
           .state('tab.training',
          {
            cache: false,
            url:"/training",
             views: {
              'tab-prime': {
            templateUrl:"templates/primeTraining.html",
            controller:'PrimeTrainingCtrl'
            }
           }
          })
          
           .state('tab.events',
          {
            cache: false,
            url:"/events",
             views: {
              'tab-prime': {
            templateUrl:"templates/primeEvents.html",
            controller:'PrimeEventsCtrl'
            }
           }
          })
          
           .state('tab.updates',
          {
            cache: false,
            url:"/updates",
             views: {
              'tab-prime': {
            templateUrl:"templates/primeUpdates.html",
            controller:'PrimeUpdatesCtrl'
            }
           }
          })
         
          .state('tab.howTo&Redeem',
          {
            cache: false,
            url:"/howTo&Redeem",
             views: {
              'tab-sideMenu': {
            templateUrl:"templates/howTo&Redeem.html",
            controller:'HowTo&RedeemCtrl',
            }
           }
          })
          
          .state('tab.profile',
          {
            cache: false,
            url:"/profile",
             views: {
              'tab-sideMenu': {
            templateUrl:"templates/profile.html",
            controller:'ProfileCtrl'
            }
           }
          })
          
           .state('tab.profileEdit',
          {
            cache: false,
            url:"/profile/profileEdit",
             views: {
              'tab-sideMenu': {
            templateUrl:"templates/profile-edit.html",
            controller:'ProfileEditCtrl'
            }
           }
          })
          
          .state('tab.messages',
          {
            cache: false,
            url:"/messages",
             views: {
              'tab-sideMenu': {
            templateUrl:"templates/messagesNotifications.html",
            controller:'NotificationsCtrl'
            }
           }
          })
          
          
          .state('tab.messagesDetail',
          {
            cache: false,
            url:"/messages/:messageId",
             views: {
              'tab-sideMenu': {
            templateUrl:"templates/message-detail.html",
            controller:'MessagesDetailCtrl'
            }
           }
          })
          
          
          .state('tab.favorites',
          {
            cache: false,
            url:"/favorites",
             views: {
              'tab-sideMenu': {
            templateUrl:"templates/favorites.html",
            controller:'FavoritesCtrl'
            }
           }
          })
          
          .state('tab.redeemed',
          {
            cache: false,
            url:"/redeemed/:redeemId",
             views: {
              'tab-sideMenu': {
            templateUrl:"templates/redeemed.html",
            controller:'CouponsCtrl'
            }
           }
          })
          
          .state('tab.leaderboards',
          {
            cache: false,
            url:"/leaderboards",
             views: {
              'tab-sideMenu': {
            templateUrl:"templates/leaderboards.html",
            controller:'LeaderboardsCtrl'
            }
           }
          })
          
          .state('tab.leaderboard',
          {
            url: '/leaderboard/:tournamentID', 
            views: {
              'tab-sidemenu': {
                templateUrl: 'templates/leaderboard.html',
                controller: 'LeaderboardsCtrl',
              }
            }
          })
          
          .state('tab.achievements',
          {
            cache: false,
            url:"/achievements",
             views: {
              'tab-sideMenu': {
            templateUrl:"templates/achievements.html",
            controller:'BadgesCtrl'
            }
           }
          })
          
          .state('tab.register',
          {
            cache: false,
            url:"/register",
             views: {
              'tab-sideMenu': {
            templateUrl:"templates/register.html",
            controller:'RegisterCtrl'
            }
           }
          })
          
          .state('register',
          {
            cache: false,
            url:"/register_main",
            templateUrl:"templates/registerMainPage.html",
            controller:'RegisterCtrl'
            
           
          })
          
          
// if none of the above states are matched, use this as the fallback
      $urlRouterProvider.otherwise('/');


    }]).factory('translateLoader', ['$q', '$translate','Constants','$http', function ($q, $translate,Constants,$http) {
      return function (options) {
//            if (!options || (!angular.isString(options.prefix) || !angular.isString(options.suffix))) {
//                throw new Error('Couldn\'t load static files, no prefix or suffix specified!');
//            }
        var deferred = $q.defer();
        $http.get(Constants.TRANSLATE_URL+options.key+Constants.TRANSLATE_SUFFIX)
            .success(function (data) {
              deferred.resolve(data);
            }).error(function (data) {
              return null;
            });
        return deferred.promise;
      }
    }]);
