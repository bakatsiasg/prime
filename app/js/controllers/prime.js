'use strict';
controllers.controller('PrimeCtrl',['$scope','$rootScope','EventService','Events',
    '$state','Constants','Utils','TalosValues','localStorageService','$location',function($scope,$rootScope,EventService,Events,$state,
        Constants,Utils, TalosValues,localStorageService,$location) {

 $scope.myTitle = 'Template';
  $scope.items = [
    {img:"img/pic1.jpg", descr:"ΠΛΗΡΟΦΟΡΗΣΗ", linkTo:'/tab/information'},
    {img:"img/pic2.jpg", descr:"ΕΚΠΑΙΔΕΥΣΗ", linkTo:'/tab/training'},
    {img:"img/pic3.jpg", descr:"ΔΡΩΜΕΝΑ", linkTo:'/tab/events'},
    {img:"img/pic4.jpg", descr:"ΕΝΗΜΕΡΩΣΗ", linkTo:'/tab/updates'}
  ];
  
 $scope.goToState = function(path) {
    $location.path(path);
  }

}]);