


'use strict';
controllers.controller('ServerSelectionCtrl',['$state','$scope','$rootScope','Constants','ListFactory','$ionicModal',function( $state,$scope,$rootScope,Constants,ListFactory,$ionicModal) {


      // Get list from storage
      $scope.list = ListFactory.getList();
      
      var listT = ListFactory.getList();
      console.log(listT);

      
      
      

      

      function getDefault() {
        //Remove existing default
        for (var i = 0; i < $scope.list.length; i++) {
          if ($scope.list[i].useAsDefault == true) {
            $scope.list[i].useAsDefault = false;
          }
        }
      }

      $scope.addItem = function(form) {
        var newItem = {};
        // Add values from form to object
        newItem.server = form.server.$modelValue;
        newItem.resources = form.resources.$modelValue;
        newItem.avatar = form.avatar.$modelValue;
        newItem.useAsDefault = form.useAsDefault.$modelValue;
        // If this is the first item it will be the default item
        if ($scope.list.length == 0) {
          newItem.useAsDefault = true;
        } else {
          // Remove old default entry from list	
          if (newItem.useAsDefault) {
            removeDefault();
          }
        }
        // Save new list in scope and factory
        $scope.list.push(newItem);
        ListFactory.setList($scope.list);
      };



      $scope.removeItem = function(item) {
        // Search & Destroy item from list
        $scope.list.splice($scope.list.indexOf(item), 1);
        // If this item was the Default we set first item in list to default
        if (item.useAsDefault == true && $scope.list.length != 0) {
          $scope.list[0].useAsDefault = true;
        }
        // Save list in factory
        ListFactory.setList($scope.list);
      }



      $scope.makeDefault = function(item) {
        removeDefault();
        var newDefaultIndex = $scope.list.indexOf(item);
        $scope.list[newDefaultIndex].useAsDefault = true;
        ListFactory.setList($scope.list);
      }

      function removeDefault() {
        //Remove existing default
        for (var i = 0; i < $scope.list.length; i++) {
          if ($scope.list[i].useAsDefault == true) {
            $scope.list[i].useAsDefault = false;
          }
        }
      }
        
        var toLoading = function(){
            $state.go('loading')
        }

        var i;
        var findDefault = function(obj){
          
          for (i = 0; i < $scope.list.length; i++) {
          if ($scope.list[i].useAsDefault == true) {
            return i;
            
          }
        }
          
        };

        


        $scope.launch = function(form){
        

        findDefault(listT);
        
        Constants.SERVER = listT[i].server;
        console.log(Constants.SERVER);
        
        Constants.RESOURCE_URL = listT[i].resources;
        console.log(Constants.RESOURCE_URL);
        
        Constants.USER_AVATAR_IMAGE_URL = listT[i].avatar;
        console.log(Constants.USER_AVATAR_IMAGE_URL);
        
        toLoading();
        };
    }
    
    
  ]);
