

'use strict';
controllers.controller('LoginCtrl',['$scope','$rootScope','User','EventService','Events',
    '$state','Constants','Utils','TalosValues','localStorageService','Profile','TalosCore','$location','$ionicLoading','$ionicPopup','$translate','$filter','GlobalData','States', function($scope,$rootScope,User,EventService,Events,$state,
        Constants,Utils, TalosValues,localStorageService,Profile,TalosCore,$location,$ionicLoading,$ionicPopup, $translate,$filter,GlobalData, States) {

console.log("Hello Login Controller!");
        
          $scope.user = {'username':"",'password':""};
            
           
           
                $scope.today = $filter('date')(new Date(), 'yyyy-MM-dd');

            $scope.login = function() {
             
                console.log("LoginCtrl login");  
                console.log($scope.user.username);
                console.log($scope.user.password);
                
                if (($scope.user.username) && ($scope.user.password)) {
                
                        console.log("You entered name and password");
                    
                                $ionicLoading.show({
                                template: 'Loading...'
                                    
                                });       
              
                                
                         if (angular.isDefined(GlobalData.user)) {
                        var _user = GlobalData.user;
                        if (angular.isDefined(_user.isGuest)) {
                            if (!_user.isGuest)
                                $state.go('tab.home');
                            }
                        }


                         TalosCore.authenticate($scope.user.username, $scope.user.password)
                            .then(function(data) {
                               // check for error codes
                               if(data == null )
                               {
                                   $ionicLoading.hide();
                                   return;
                               }
                               if (data.errorId && data.code) {
                                
                               
                                    $ionicLoading.hide();
                                    
                                                                    
                                     var alertPopup = $ionicPopup.alert({
                                              title: 'Sorry',
                                              template: '{{"Wrong username or password" | translate }}'
                                    });
                                
                                
                                switch (data.code) {
                                    case 1059:
                                        
                                                                          
                                         var alertPopup = $ionicPopup.alert({
                                              title: 'Sorry!',
                                              template: '{{"Please activate your account" | translate}}!'
                                              });
                                        

                               
                                        break;
                                    case 1055:
                                         
                                                                          
                                         var alertPopup = $ionicPopup.alert({
                                              title: 'Sorry!',
                                              template: '{{"Your account is locked" | translate}}!'
                                              });
                                         
                                        
                                        
                                        break;
                                }

//                              Utils.errorPopUp($translate.instant(message))

                            }
                            else {
                                $scope.hide = function(){
                                $ionicLoading.hide();
                                };
                                $scope.hide();
                                
                                EventService.sendEvent(Events.HIDE_OR_SHOW_LOGIN,{'show':false});
                                $state.go('tab.home');
                            }
                        },function(error) { console.log("Network ERROR!")});
                }
                
                else {
                    
                          console.log("Else Statement!");              

                           
                          var alertPopup = $ionicPopup.alert({
                                     title: 'Error!',
                                     template: '{{"Popup sidemenu validation1" | translate }}'
                          });
                          

                          
                    

                }
            };

                        $scope.forgotPassword = function() {
                            $scope.data = {
                                email:''
                            };
                             $scope.Registration_EmailIsInvalid = '';
                            //$scope.formInvalid = false;
                                    var myPopup = $ionicPopup.show({
                                    title: 'Enter email',
                                    template:'<input type="email" placeholder="Email" ng-model="data.email"><br><p style="text-align:center">{{Registration_EmailIsInvalid | translate}}</p>',
                                    scope: $scope,
                                    buttons: [
                                      { text: 'Cancel' },
                                      { text: '<b>Reset</b>',
                                        type: 'button-positive',
                                        onTap:function(email) {
                                        
                                        if (!$scope.data.email) {
                                            //don't allow the user to close unless he enters wifi password
                                            email.preventDefault();
                                            //Until we add translation.......
                                            $scope.Registration_EmailIsInvalid = "Please enter a valid email";
                                        }
                                        else{
                                          //Utils.loadingPopup('LOADING_FORGOT_PASS')
                                            User.resetPasswordRequest($scope.data.email);
                                            var myPopup = $ionicPopup.show({
                                                templateUrl: 'templates/forgot_password.html',
                                                buttons: [
                                                  { text: 'OK',
                                                  onTap: function(e) {
                                                  //add code
                                                  console.log('email send');
                                                    }
                                               },]
                                            })
                                        }
                                      }
                                    }
                                ]                                 
                            });
                        }
        
                  
                  }]);
