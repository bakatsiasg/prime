/**
 * Created by georgefloros on 5/4/15.
 */

'use strict';
controllers.controller('ProfileCtrl',['$scope','$rootScope','Constants','Profile','$translate','$ionicPopup','$cordovaCamera','$timeout','$log','User','$ionicLoading','EventService','Events','$ionicActionSheet',
                    function($scope,$rootScope, Constants, Profile, $translate, $ionicPopup, $cordovaCamera,$timeout,$log,User,$ionicLoading,EventService, Events, $ionicActionSheet) {
    
        
        
        $scope.showEdit = true;
    
        $scope.editprofile = function(){    
            $scope.showEdit = !$scope.showEdit;
            
        };
        
        var imageToUploadBase64 = null;
 
                         
                         
             $scope.$on("$ionicView.beforeEnter",function(){
              
              
              var _user = Profile.profile;
              
              $scope.picUrl = (_user.picUrl? _user.picUrl: Constants.DEFAULT_AVATAR);
              $scope.nickname = _user.nickname;
          
           });
            
          
           var afterUpdateAndUploadFinished = function() {
                
                                    $ionicLoading.hide();
                                    var alertPopup = $ionicPopup.alert({
                                                title: 'Success!',
                                                template: '{{"Profile picture updated" | translate}}'
                                     });
                                     
                                    alertPopup.then(function(res) {
                                           EventService.sendEvent(Events.PROFILE_UPDATED);
                                        /* after update profile and message Ok go to home */
                                    }); 
                      };
 
                
      
                
         $scope.chooseFileToupload = function(){
                   
               if (ionic.Platform.isAndroid()||ionic.Platform.isIOS()){

                   
                  $ionicActionSheet.show({
                    buttons: [
                        { text: $translate.instant('Camera Roll') }, // Camera.PictureSourceType.PHOTOLIBRARY = 0
                        { text:  $translate.instant('Take Photo') } //Camera.PictureSourceType.CAMERA = 1
                    ],
                    titleText: $translate.instant('Choose'),
                    cancelText: $translate.instant('Cancel'),
                    cancel: function() {
                        // add cancel code..
                    },
                    buttonClicked: function(index) {
                        var options = {
                            quality: 80,
                            destinationType: Camera.DestinationType.DATA_URL,
                            sourceType: index,
                            allowEdit: true,
                            encodingType: Camera.EncodingType.JPEG,
                            targetWidth: 300,
                            targetHeight: 300,
                            popoverOptions:null,
                            saveToPhotoAlbum: false
                        };
                        $cordovaCamera.getPicture(options).then(function(imageData) {
//                            $scope.imgChoosed = true;
                            var image = document.getElementById('myImage');
                            image.src = "data:image/jpeg;base64," + imageData;
                        }, function(err) {
                             //error
                        });
                        return true;
                    }
                });

                   
                   
                   
                 }
                      
               else{    
                          var element = document.getElementById('chooseImage');
                          var imgChoosed = function (event){
                          $log.debug('change');
                          var thefile;
                          if ((thefile = event.target.files[0])) {
                           var fileReader = new FileReader();
                                    
                           $ionicLoading.show(); 
                                    
                           fileReader.onload = function(fileLoadedEvent) {
                                 imageToUploadBase64 = fileLoadedEvent.target.result; // <--- data: base64
                    
                    
                                var imghold= document.getElementById('imgToupload');
                                imghold.src =  imageToUploadBase64;
                                imageToUploadBase64  =  imageToUploadBase64.substring( imageToUploadBase64.indexOf("base64,") + ("base64,").length );
                                 $scope.imageToUploadChoosed = true;
       
                                     if (imageToUploadBase64) {
                                             User.uploadAvatar(imageToUploadBase64)
                                                  .then(
                                                     function(data){
                                                      Profile.profile.picUrl = imageToUploadBase64;
                                                      afterUpdateAndUploadFinished();
                                                          
                                                          
                                           },
                                          function(data){
                                                         
                                                   $ionicLoading.hide();
                                                   $ionicPopup.alert({
                                                            title: 'Error!',
                                                            template: '{{"Profile picture not updated" | translate}}'
                                                     });
                                                        }
                                                     );
                                                 } else {
                                                         $ionicLoading.hide();
                                                 }
                                             
                            
                                    };
                                    fileReader.readAsDataURL(thefile);
                                  }
                                    element.removeEventListener('change',imgChoosed);
                                };
                             
                                       
                                element.addEventListener('change',imgChoosed);
                             
 
                                $timeout(function() {
                                           element.click();
                                     }, 0);
                    }    
 
            };   

     
    }]);