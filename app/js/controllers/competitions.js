'use strict';

/**
 * @ngdoc object
 * @name core.Controllers.LotteriesCtrl
 * @description LotteriesCtrl
 * @requires ng.$scope
*/
controllers.controller('CompetitionsCtrl', [
        '$scope', '$filter', 'Constants', 'Resources', 'Utils', '$state', '$stateParams', 'TalosValues', 'Categories', 'Tournaments', 'GlobalData', 'Metadata', 'Events', 'EventService',
        function($scope, $filter, Constants, Resources, Utils, $state, $stateParams, TalosValues, Categories, Tournaments, GlobalData, Metadata, Events, EventService) {
            //console.log('LotteriesCtrl')
   $scope.tournaments = [];
            
            $scope.competitionDet = function(competition){ 
               $state.go('tab.competition',{'competitionID': competition.tournamentId});
           }

            $scope.showView = false;
            $scope.showViewStep = 0;
            $scope.showViewNSteps = 3;
            $scope.listIsLoading = false;
            var _allItemsLoaded = true;
            $scope.items = [];
            $scope.itemsSize = 0;

            $scope.msgs = {
                'Enter_AlreadyJoined': '',
                'Enter_Success': '',
                'Enter_Error': '',
                'Enter_ConfirmationBody': ''
            };

            // $scope.category = '';
            // if ($state.current.name == 'missions.lotteries') {
                // $scope.category = Constants.CATEGORY_LOTTERIES;
                // $scope.msgs['Enter_AlreadyJoined'] = $filter('translate')('EnterLottery_AlreadyJoined');
                // $scope.msgs['Enter_Success'] = $filter('translate')('EnterLottery_Success');
                // $scope.msgs['Enter_Error'] = $filter('translate')('EnterLottery_Error');
                // $scope.msgs['Enter_ConfirmationBody'] = $filter('translate')('EnterLottery_ConfirmationBody');
                // $scope.msgs['Enter_ConfirmationBody1'] = $filter('translate')('EnterLottery_ConfirmationBody1');
                // $scope.msgs['Enter_Login'] = $filter('translate')('EnterLottery_Login');
            // } else if ($state.current.name == 'missions.competitions') {
                // $scope.category = Constants.CATEGORY_COMPETITIONS;
                // $scope.msgs['Enter_AlreadyJoined'] = $filter('translate')('EnterCompetition_AlreadyJoined');
                // $scope.msgs['Enter_Success'] = $filter('translate')('EnterCompetition_Success');
                // $scope.msgs['Enter_Error'] = $filter('translate')('EnterCompetition_Error');
                // $scope.msgs['Enter_ConfirmationBody'] = $filter('translate')('EnterCompetition_ConfirmationBody');
                // $scope.msgs['Enter_ConfirmationBody1'] = '';
                // $scope.msgs['Enter_Login'] = $filter('translate')('EnterCompetition_Login');
            // }

            $scope.category = Constants.CATEGORY_COMPETITIONS;     

            Categories.get($scope.category, -1, true).then(function(data) {
                var tmp = [],
                    j;
                angular.forEach(data.data[0].leaves, function(leaf, leafKey) {
                    tmp.push(leaf.itemId);
                });
                Tournaments.get(0, -1, true, Constants.TOURNAMENT_TYPE_COMPETITION, Constants.GAME_TYPE, function(data){
                     
                    var tmpIds = [];
                    var dataIds = {};
                    var x = data.length; 
                    for (j = 0; j < x; j++) {
                        // if (tmp.indexOf(data[j].tournamentId) > -1) {
                            data[j].level = '';
                            dataIds[data[j].tournamentId] = $scope.tournaments.length;
                            $scope.tournaments.push(data[j]);
                            tmpIds.push(data[j].tournamentId);
                        // }
                    }
                    Metadata.getMultipleMetadata(Constants.ITEM_TYPE_TOURNAMENT_RESOURCES, tmpIds).then(function(data1) {
                        angular.forEach(data1.data, function(metadata, metadataKey) {
                            if (metadata.metadata[0].key == Constants.METADATA_KEY_NAME_REQUIRED_LEVEL) {
                                $scope.tournaments[dataIds[metadata.metadata[0].itemId]].level = metadata.metadata[0].value;
                            }
                        });

                        $scope.addMoreItems();
                    }, function() {
                        $scope.addMoreItems();
                    });
                }, function() {
                    $scope.addMoreItems();
                });
            }, function() {
                $scope.addMoreItems();
            });

            $scope.addMoreItems = function(){
                //console.log('addMoreItems in LotteriesCtrl')
                /* disable scrolling until data received */
                _allItemsLoaded = true;
                $scope.listIsLoading = true;
                var from = $scope.items.length;
                var to = from + Constants.PAGE_SIZE; /* PAGGING */
                to = (to > $scope.tournaments.length)? $scope.tournaments.length: to;
                var data = [],
                    j;
                if (from != to) {
                    for (j = from; j < to; j++)
                        data[j - from] = $scope.tournaments[j];
                    $scope.getResources(data);
                    $scope.getUserParticipation(data);
                    $scope.items = $scope.items.concat(data);
                    $scope.itemsSize = $scope.tournaments.length;
                    if ($scope.itemsSize > $scope.items.length)
                        _allItemsLoaded = false;
                    
                }   
 
            };

            $scope.loadMore = function() {
                return _allItemsLoaded;
            };

            $scope.getItemImageResourceUrl = function(item) {
                try {
                    return Resources.getResourceUrl(item.tournamentId, Constants.ITEM_TYPE_TOURNAMENT_RESOURCES, Constants.RESOURCE_TYPE_IMAGE, Constants['LANGUAGE_ID_' + TalosValues.getLanguage()]);
                } catch(error) { return ""; }
            }

            $scope.getItemEndDate = function(item) {
                try {
                    var date = new Date(item.activeTo);
                    return date.getDate() + ' ' + $filter('translate')('Month_' + date.getMonth()) + ' ' + date.getFullYear();
                } catch(error) {
                    return "";
                }
            }

            $scope.getResources = function(items) {
                var resource = {},
                    resources = Array(),
                    input = {},
                    property = "",
                    j;

                try {
                    angular.forEach(items, function(item, key) {
                        item.resources = {};
                        angular.forEach(Constants.LANGUAGE_IDS, function(language, languageKey) {
                            item.resources[Constants['LANGUAGE_CODE_' + language]] = {
                                "name": "",
                                "description": "",
                                "winnersUrl":"",
                                "TWITTER_POST_TITLE":"",
                                "POST_TO_WALL_TITLE":"",
                                "POST_TO_WALL_PIC_URL":"",
                                "POST_TO_WALL_TEXT":""

                            };
                        });

                        resource = {};
                        resource.itemId = item.tournamentId;
                        resource.itemTypeId = Constants.ITEM_TYPE_TOURNAMENT_RESOURCES;
                        resource.resourceTypeId = Constants.RESOURCE_TYPE_NAME;
                        resources.push(resource);

                        resource = {};
                        resource.itemId = item.tournamentId;
                        resource.itemTypeId = Constants.ITEM_TYPE_TOURNAMENT_RESOURCES;
                        resource.resourceTypeId = Constants.RESOURCE_TYPE_DESCRIPTION;
                        resources.push(resource);

                        resource = {};
                        resource.itemId = item.tournamentId;
                        resource.itemTypeId = Constants.ITEM_TYPE_TOURNAMENT_RESOURCES;
                        resource.resourceTypeId = Constants.RESOURCE_TYPE_TOURNAMENT_WINNERS_URL;
                        resources.push(resource);
                    });

                    input.languageIds = Constants.LANGUAGE_IDS;
                    input.resources = resources;
                    Resources.getMultipleResources(0, -1, input).then(function(data) {
                        var item;
                        var lang;
                        angular.forEach(data.data, function(resource, resourceKey) {
                            if (resource.itemTypeTO.itemTypeId == Constants.ITEM_TYPE_TOURNAMENT_RESOURCES) {

                                lang  = Constants["LANGUAGE_CODE_" + resource.languageTO.languageId];
                                for (j = 0; j < items.length; j++) {
                                    item = items[j];
                                    if(item.tournamentId == resource.itemId){
                                    switch (resource.resourceTypeTO.resourceTypeId) {
                                        case Constants.RESOURCE_TYPE_NAME:
                                            property = "name";
                                            item.resources[lang][property] = resource.textResource;
                                            item.resources[lang]['TWITTER_POST_TITLE'] = resource.textResource;
                                            item.resources[lang]['POST_TO_WALL_TITLE'] = resource.textResource;
                                            item.resources[lang]['POST_TO_WALL_PIC_URL'] = Resources.getResourceUrl(item.tournamentId, Constants.ITEM_TYPE_TOURNAMENT_RESOURCES, Constants.RESOURCE_TYPE_IMAGE,lang);
                                            break;
                                        case Constants.RESOURCE_TYPE_DESCRIPTION:
                                            property = "description";
                                            item.resources[lang]['POST_TO_WALL_TEXT'] = resource.textResource;
                                            break;
                                        case Constants.RESOURCE_TYPE_TOURNAMENT_WINNERS_URL:
                                            property = "winnersUrl";
                                            break;
                                    }
                                    item.resources[lang][property] = resource.textResource;
                                }

                                }
                            }
                        });

                       ;
                    }, function() {
                        
                    });
                } catch (error) {
                     
                }
            }

            $scope.getUserParticipation = function(items) {
                try {
                    var ids = [];
                    var dataIds = {};
                    angular.forEach(items, function(item, key) {
                        item.participationStatus = "";
                        ids.push(item.tournamentId);
                        dataIds[item.tournamentId] = key;
                    });

                    Tournaments.getUserParticipation(ids).then(function(data) {
                        angular.forEach(data.data, function(item, key) {
                            items[dataIds[item.tournamentId]].participationStatus = item.participationStatusId;
                        });

                        //console.log('tournaments: ' + JSON.stringify(items))

                        
                    }, function() {
                        
                    });
                } catch (error) {
                    
                }
            }

            $scope.getResource = function(item, resource) {
                try {
                    return item.resources[TalosValues.getLanguage()][resource];
                } catch(error) {
                    return "";
                }
            }

            $scope.getParticipationFees = function(item) {
                var s = "";
                try {
                    if (!item.level.length && !item.entryFeeAmount)
                        return $filter('translate')('NeedEnterTournament_Free');
                    else {
                        if (item.level.length) {
                            s = item.level + " " + $filter('translate')('NeedEnterTournament_Level');
                        }
                        if (item.entryFeeAmount) {
                            s += (s.length? ", ": "") + item.entryFeeAmount + " " + $filter('translate')('NeedEnterTournament_Points');
                        }

                        return s;
                    }
                } catch(e) { return $filter('translate')('NeedEnterTournament_Free'); }
            }

            

            $scope.hasResource = function(item,resource)
            {
                return (($scope.getResource(item, resource) == "")? false: true);
            }


            // $scope.itemUserParticipationStatus = function(item, status) {
                // var d1 = item.endsOn;
                // var d2 = new Date().getTime();
                // var s = "";

                // if (d1 < d2) s = "finished";
                // else if (item.participationStatus) s = "joined";
                // else s = "open";
                // return (s == status);
            // }

            $scope.itemUserParticipationStatusText = function(item) {
                var d1 = item.endsOn;
                var d2 = new Date().getTime();

                if (d1 < d2) return $filter('translate')('MissionStatus_Finished');
                if (item.participationStatus) return $filter('translate')('MissionStatus_Entered');
                return $filter('translate')('MissionStatus_Running');
            }


            $scope.getExpirationTimeCounter = function(item) {
                var d = "00:00";
                try {
                    var d1 = item.endsOn;
                    var d2 = new Date().getTime();
                    return ((d1 > d2)? Utils.getDateDif(d1, d2): d);
                } catch(e) { return d; }
            }

            $scope.openUrl = function(item,resource,$event)
            {
                $event.stopPropagation();
                var link = $scope.getResource(item,resource);
                window.open("http:\/\/"+link, '_blank', 'location=yes');
            }


        }
]);
