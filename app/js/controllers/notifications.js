'use strict';

/**
 * @ngdoc object
 * @name core.Controllers.NotificationsCtrl
 * @description NotificationsCtrl
 * @requires ng.$scope
*/
controllers.controller('NotificationsCtrl', [
        '$scope', '$rootScope', 'Constants', 'Messages','EventService','Events', 'filterFilter', '$filter', '$translate',  'Utils','$state','$ionicLoading','$ionicPopup',
        function($scope, $rootScope, Constants, Messages, EventService,Events, filterFilter, $filter, $translate,  Utils, $state, $ionicLoading,$ionicPopup) {
            console.log('NotificationsCtrl')
 

            var _allItemsLoaded = false;
            $scope.showView = false;
            $scope.messages = [];
            $scope.messagesSize = 0;


            $scope.showBanner = true;
            
            
            $scope.removeBan = function(){
                
                $scope.showBanner = false;
                $rootScope.hasNotif = !$rootScope.hasNotif;
                       
            };



            $scope.addMoreItems = function(){
 
                console.log('addMoreItems')
                _allItemsLoaded = true;                
                var from = $scope.messages.length;
                from = from || 0;
                var to = from + Constants.PAGE_SIZE; /* PAGGING for Notifications */
                Messages.get(from, to).then(function(data) {
                    $scope.messages = $scope.messages.concat(data.data);
                    $scope.messagesSize = data.headers(Constants.HEADER_XTalosItemCount);
                    if (!$scope.messagesSize) $scope.messages = [];
                    for (from = 0; from < $scope.messages.length; from++)
                        $scope.messages[from].exists = true;
                    
                    if ($scope.messagesSize > $scope.messages.length)
                        _allItemsLoaded = false;                  


                    $scope.showView = true;
                    
                     
                });
                
                
            };
            
            
           
            $scope.loadMore = function() {
                return _allItemsLoaded;
            };
            
            $scope.getMessageImage = function(message) {
                try {
                    var url = '';
                    var urls = filterFilter(message.properties, {type: Constants.MESSAGES_URL_TYPE});
                    var images = filterFilter(message.properties, {type: Constants.MESSAGES_IMAGE_TYPE});
                    if (urls.length) {
                        url = urls[0].value;
                        if (url.substring(0, 4) != 'http') url = '';
                    }
                    if (images.length && !url.length) {
                        url = images[0].value;
                        if (url.substring(0, 4) != 'http') url = '';
                    }                    
                    if (!url.length) url = Constants.DEFAULT_AVATAR;

                    return url;
                } catch(e) { return Constants.DEFAULT_AVATAR; }
            }
            
            $scope.getMessageDate = function(message) {
                try {
                    var date = new Date(message.messageDate);
                    return date.getDate() + ' ' + $filter('translate')('Month_' + date.getMonth()) + ' ' + date.getFullYear() + ', ' + ((date.getHours().length == 1)? '0' + date.getHours(): date.getHours()) + ':' + ((date.getMinutes().length == 1)? '0' + date.getMinutes(): date.getMinutes());
                } catch(e) {
                    return "";
                }
            }             
            
            $scope.isUnread = function(message) {
                try {
                    return !message.readFlag;
                } catch(e) { return false; }
            }
            
            $scope.read = function(message) {
                if (message.readFlag) return;
                try {
                    Messages.status(Array(message.id), Constants.MESSAGES_READ_STATUS).then(function(data) {
                        message.readFlag = true;
                        EventService.sendEvent(Events.NOTIFICATION_READ);
                    });
                } catch(e) {}
            }
            
            $scope.delete = function(message) {
                    
 var confirmPopup = $ionicPopup.confirm({
                     title: 'Delete message',
                     template: '{{"Are you sure you want to delete this message?" | translate}}'
                 });
                 
                 confirmPopup.then(function() {
                    Utils.loadingPopup(null);
                    Messages.delete(Array(message.id)).then(function(data) {
                        message.exists = false;
                        $scope.messagesSize--;
                        var newId = '';
                        var newText = '';
                        var l = $scope.messages.length, j = 0;
                        for (j = 0; j < l; j++) {
                            if ($scope.messages[j].exists) {
                                newId = $scope.messages[j].id;
                                newText = $scope.messages[j].content;
                                break;
                            }
                        }
                        EventService.sendEvent(Events.NOTIFICATION_REMOVED, {id: message.id, newId: newId, newText: newText});
                        Utils.closeLoadingPopUp('');
                        //Utils.getGenericDialog($translate.instant('Notifications_Removed'), 'success');                                                
                        /*
                        ngDialog.open({ 
                            template: 'templates/dialogs/generic_dialog.html', 
                            className: 'ngdialog-theme-mall', 
                            data: angular.toJson({
                                'body': $translate.instant('Notifications_Removed'),
                                'status': 'success'
                            }),
                        });                         
                        */
                    }, function() {
                        Utils.closeLoadingPopUp('');
                        //Utils.getGenericDialog($translate.instant('Notifications_NotRemoved'), 'error');                                                
                        /*
                        ngDialog.open({ 
                            template: 'templates/dialogs/generic_dialog.html', 
                            className: 'ngdialog-theme-mall', 
                            data: angular.toJson({
                                'body': $translate.instant('Notifications_NotRemoved'),
                                'status': 'error'
                            }),
                        });                                                 
                        */
                        $scope.addMoreItems();
                    });                    
                });             
            }    
            
            
            // var confirmPopup = $ionicPopup.confirm({
                //      title: 'Logout',
                //      template: '{{"Are you sure you want to logout?" | translate}}'
                //  });
                 
                //  confirmPopup.then(function(res) {
                //      if(res) {
                //              console.log('You are sure');
                //              $ionicLoading.show({
                //                         template: 'Logout...'       
                //              }); 
                //              TalosValues.clearUserData();
                //              Profile.clear();
                //              $scope.showLogin = false;
                            
                //              TalosCore.guest().then(function(data)
                //              {
                //                      Profile.get(function(profile){
                                         
                //                           $ionicLoading.hide();
         
                //                      },true);
                                  
                                 
                //              });        
            
            $scope.$on(Events.LANGUAGE_SET, function () {
                var _allItemsLoaded = false;
                $scope.showView = false;
                $scope.messages = [];
                $scope.messagesSize = 0;
                $scope.addMoreItems();
            });            
        }
]);
