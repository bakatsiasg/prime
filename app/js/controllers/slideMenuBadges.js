/**
 * Created by georgefloros on 1/26/15.
 */
controllers.controller('SlideMenuBadges', [
        '$scope','$rootScope', 'Achievements', 'Resources','Constants','TalosValues','$filter',
        function($scope, $rootScope, Achievements,  Resources,Constants,TalosValues,$filter) {

            $scope.isLoadingBadges = false;
//            $rootScope.$on(ToggleHelper.events.toggleableToggled,function(event,id,toggleState){
//              if(id == 'mainSidebar')
//              {
//                $scope.isLoadingBadges = toggleState;
                  $scope.isLoadingBadges = true ; 
//                  if(toggleState)
                  if (true)
                  {
                              Achievements.get().then(function(data) {
                              //console.log(JSON.stringify(data.data));
                              var length = data.data.length;
                              var item;
                              for(var i= 0 ; i < length ; i++)
                              {
                                  item = data.data[i];
                                  item.percentage = $scope.getCompletionPercentage(item);
                              }

                              $scope.badges = $filter('orderBy')(data.data,"percentage",true);

                              if($scope.isLoadingBadges)
                                  $scope.isLoadingBadges = false;
                          },  function() {
                          });
                  }
//              }

//            });
            $scope.getCompletionPercentage = function(item) {
               
                try {
                    return (parseInt(100 * item.progressSoFar / item.completionValue));
                } catch(error) {
                    return 0;
                }
            }
            
            $scope.getLogoResourceUrl = function(item) {
                
                try {

                    return item.percentage >=100? Resources.getResourceUrl(item.achievementTypeId, Constants.ITEM_TYPE_ACHIEVEMENT_TYPE, Constants.RESOURCE_TYPE_IMAGE, Constants['LANGUAGE_ID_' + TalosValues.getLanguage()])
                        :'img/badge_empty.png';
                } catch (error) {
                    return "";
                }
            }

        }]);
