'use strict';

/**
 * @ngdoc object
 * @name core.Controllers.FriendsCtrl
 * @description FriendsCtrl
 * @requires ng.$scope
*/
controllers.controller('FriendsCtrl', [
        '$scope',  'Constants', 'User', '$log', '$filter',   'TalosEvents', 'Utils', 'TalosValues', '$translate','$rootScope',
        function($scope,   Constants, User, $log, $filter, TalosEvents, Utils, TalosValues, $translate,$rootScope) {
            
            var _allItemsLoaded = false;
            $scope.showView = false;
            $scope.items = [];
            $scope.itemsSize = 0;
//          OAuth.initialize(Constants.OAUTH_KEY);



            $scope.addMoreItems = function() {
                //console.log('addMoreItems in FriendsCtrl')
                $scope.listIsLoading = true;
                _allItemsLoaded = true;
                var from = $scope.items.length;
                from = from || 0;
                var to = from + Constants.PAGE_SIZE;
                User.getFriends(from, to).then(function(data) {
                    $scope.items = $scope.items.concat(data.data);
                    $log.log(data.data);
                    $scope.itemsSize = data.headers(Constants.HEADER_XTalosItemCount);
                    if (!$scope.itemsSize) $scope.items = [];
                    $scope.showView = true;
                    $scope.listIsLoading = false;
                    if ($scope.itemsSize > $scope.items.length)
                        _allItemsLoaded = false;
                }, function() {
                    $scope.showView = true;
                    $scope.listIsLoading = false;
                });
            };

            $scope.loadMore = function() {
                return _allItemsLoaded;
            };

            $scope.getPicUrl = function(item) {
                var tmp = null;
                try {
                    tmp = item.picUrl;
                } catch(e) {}
                //var tmp = (item && item.customUserProfileFieldList) ?  $filter('filter')(item.customUserProfileFieldList, {key: 'picUrl'})[0].value : null;
                return ((tmp == null)? Constants.DEFAULT_AVATAR: tmp);
            }

            $scope.getEmail = function(item) {
                if (angular.isDefined(item.emailAddress))
                    return (item.emailAddress.length? item.emailAddress: '-');
                return '-';
            }
            $scope.inviteFacebook = function ()
            {
                // Business logic ,  no social-talos user needed
                //OAuth.popup('facebook',{cache: true})
                //  .done(function(result) {

                    var ref = window.open('https:\/\/www.facebook.com/dialog/apprequests?' +
                    'app_id=' + Constants.FACEBOOK_APP_ID +
                    "&filters=['app_non_users']"+
                    '&message='+$translate.instant('FB_APP_REQUEST_MESSAGE')+
                    '&redirect_uri='+Constants.FB_APP_REQUEST_URI, '_blank', 'location=no');
                    var loadStopClose = function (event) {
                      if(event.url.indexOf("submit") > -1 ) {
                        ref.removeEventListener('loadstop', loadStopClose);
                          console.log(event);
                        ref.close();
                      }
                    }
                    var loadStop = function (event) {
                      ref.removeEventListener('loadstop', loadStop);
                      ref.addEventListener('loadstop', loadStopClose);
                    }
                    ref.addEventListener('loadstop', loadStop);
                  //})
                  //.fail(function (err) {
                  //  //handle error with err
                  //});
              }

            $scope.inviteEmail = function() {
                ngDialog.open({
                    template: 'templates/dialogs/generic_email_invitation.html',
                    className: 'ngdialog-theme-mall',
                    scope: $scope,
                    data: angular.toJson({
                        'title': $translate.instant('InviteFriend_Title') + '<br /><br />',
                        'body': '',
                        'emailPlaceholder': $translate.instant('InviteFriend_EmailPlaceholder'),
                        'inviteButtonText': $translate.instant('InviteFriend_Button')
                    }),
                    controller: ['$scope', 'User', '$translate', 'GenericDialogService', function($scope, User, $translate, GenericDialogService) {
                        $scope.email = '';
                        $scope.formInvalid = false;
                        $scope.invite = function () {
                            if ($scope.emailForm.$valid) {
                                Utils.loadingPopup(null);
                                var eventTypeId = Constants.EVENT_TYPE_FRIEND_INVITATION_BY_EMAIL;
                                var input = {
                                    'userId': TalosValues.getUserId(),
                                    'applicationId': Constants.APPLICATION_ID,
                                    'gameTypeId': Constants.GAME_TYPE,
                                    'eventTypeId': eventTypeId,
                                    'eventProperties': 'email=' + $scope.email,
                                    'clientTypeId': $rootScope.clientType,
                                    'withNoRewards': false
                                }
                                TalosEvents.add(input).then(function(data1) {
                                    Utils.closeLoadingPopUp('');
                                    Utils.getGenericDialog($translate.instant('InviteFriend_Success'), 'success');
                                }, function() {
                                    Utils.closeLoadingPopUp('');
                                    Utils.getGenericDialog($translate.instant('InviteFriend_Error'), 'error');
                                });

                            } else {
                                $scope.formInvalid = true;
                            }
                        }
                    }]
                });
            }
        }
]);
