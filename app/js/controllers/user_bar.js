'use strict';

/**
 * @ngdoc object
 * @name core.Controllers.UserBarController
 * @description UserBarController
 * @requires ng.$scope
 */
controllers.controller('UserBarController', [ 
    '$scope', '$state', '$rootScope', 'Profile', 'User', 'Constants', '$log', 'Events', 'GlobalData', '$filter', 'Levels', '$translate', 'TalosValues', 'TalosCore', 'Messages','EventService', '$ionicPopup','$ionicLoading', 
    function( $scope, $state, $rootScope, Profile, User, Constants, $log, Events, GlobalData, $filter, Levels, $translate, TalosValues, TalosCore, Messages, EventService ,$ionicPopup,$ionicLoading ) {
      console.log('UserBarController');

      var updateBallanceCallBack = null;

      $scope.isGuest = true;
      $scope.nickname = '';
      $scope.picUrl = Constants.DEFAULT_AVATAR;
      $scope.points = 0;
      $scope.levelPoints = 0;
      $scope.levels = [];
      $scope.currentLevel = '';
      $scope.currentLevelPercent = 0;
      $scope.latestNotificationId = '';
      $scope.latestNotification = '';
      $scope.unreadNotifications = 0;
      $scope.checkIns = 0;

       // $rootScope.isLoadingBadges = false; //shared property for topbar and side menu

      $scope.isFacebookUser = false;
      $scope.isTwitterUser = false;
      $scope.appstore = Constants.APP_STORE_URL;
      $scope.playstore = Constants.GOOGLE_PLAY_URL;
      $scope.selected = 'tab';
      $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
          if( $rootScope.dialogIsOpen){ //$rootScope.isAndroid ,ithink that web needed to.
              event.preventDefault();
              return;
          }
          if (angular.isDefined(toState.data) && angular.isDefined(toState.data.userBarItem)) {
            $log.debug('changing state: ' + toState.data.userBarItem);
            $scope.selected = toState.data.userBarItem;
        }
        else {
          $log.warn("toState did not have any data associated");
        }
      });
      
 
      Profile.get(function(data) {
       $log.log(' Profile.get in UserBarController ');
       $scope.isGuest = data.guest;
       $scope.nickname = data.nickname;
       $scope.picUrl = data.picUrl? data.picUrl: Constants.DEFAULT_AVATAR;
       $scope.isFacebookUser = (data.extAppSourceIds) && (data.extAppSourceIds.indexOf(Constants.EXTERNAL_SYSTEM_FACEBOOK) != -1);
       $scope.isTwitterUser = (data.extAppSourceIds) && (data.extAppSourceIds.indexOf(Constants.EXTERNAL_SYSTEM_TWITTER) != -1);
       $scope.updateUserBalances();
       $scope.updateLatestNotification();
       $scope.getUnreadNotifications();
     
      }, true);
      function playSound(sound){
       // ngAudio.play("img/sounds/"+sound);

      }

      $scope.getUnreadNotifications = function() {
          User.stats({'statKeys': [Constants.USER_STATS_UNREAD_USER_MESSAGES]}).then(function(data) {
              console.log('messages: ' + JSON.stringify(data.data));
              try {
                  $scope.unreadNotifications = data.data.statistics[0].value;
                  angular.copy($scope.getUser(), Profile.profile.guest);
                  //if($scope.unreadNotifications > 0)playSound('note.mp3');
              } catch(e) {
                  $scope.unreadNotifications = 0;
                  //angular.copy($scope.getUser(), Profile.profile.guest);
              }
          }, function() {
              $scope.unreadNotifications = 0;
              angular.copy($scope.getUser(), Profile.profile.guest);
          });
      }

      $scope.updateUserBalances = function() {
        //console.log('On updating user balances...');
        User.balances(Array(Constants.UNIT_TYPE_POINTS, Constants.UNIT_TYPE_LEVEL_POINTS,Constants.UNIT_TYPE_MORE_GAME_COINS,Constants.UNIT_TYPE_MORE_CHECKINS)).then(function(data) {
          var j;
          for (j = 0; j < data.data.length; j++) {
            if (data.data[j].unitTypeId == Constants.UNIT_TYPE_POINTS)
              $scope.points = data.data[j].balance;
            if (data.data[j].unitTypeId == Constants.UNIT_TYPE_LEVEL_POINTS)
              $scope.levelPoints = data.data[j].balance;
            if (data.data[j].unitTypeId == Constants.UNIT_TYPE_MORE_GAME_COINS)
              $scope.gameCoins = data.data[j].balance;
            if (data.data[j].unitTypeId == Constants.UNIT_TYPE_MORE_CHECKINS)
              $scope.checkIns = data.data[j].balance;
          }
          $scope.setLevel();
        });
      };

      $scope.setLevel = function() {
        var j;
        var level = 0;
        var previousThreshold = 0;
        Levels.get(Constants.GAME_TYPE).then(function(data) {
          $scope.levels = data.data;
          if ($scope.levels && $scope.levels.levels) {
              $scope.levels.levels.sort(function(a, b) {
                  return a.index - b.index;
              });

              if ($scope.levelPoints) {
                  for (j = 0; j < $scope.levels.levels.length; j++) {
                      if ($scope.levels.levels[j].threshold >= $scope.levelPoints) {
                          if ($scope.levels.levels[j].threshold == $scope.levelPoints) {
                              level = $scope.levels.levels[j].index;
                              $scope.currentLevelPercent = 0;
                          } else {
                              level = $scope.levels.levels[j].index - 1;
                              /* 131px: max width */
                              $scope.currentLevelPercent = parseInt((131 / 100) * 100 * ($scope.levelPoints - previousThreshold) / ($scope.levels.levels[j].threshold - previousThreshold));
                          }
                          break;
                      } else
                          previousThreshold = $scope.levels.levels[j].threshold;
                  }

                  if (level) $scope.currentLevel = level;
                  else $scope.currentLevel = '';
              }
              //$scope.currentLevelPercent = $scope.currentLevelPercent + '%';

              if (!$scope.currentLevel && $scope.levels.levels.length)
                  $scope.currentLevel = $scope.levels.levels[0].index;
          }

          console.log('User: ' + JSON.stringify($scope.getUser()));
          angular.copy($scope.getUser(), GlobalData.user);

        }, function() {
          angular.copy($scope.getUser(), GlobalData.user);

        });
      };

      $scope.$on(Events.BALLANCE_UPDATED, function(event, properties) {
        if(angular.isDefined(properties) && angular.isDefined(properties.playSound)) {
                playSound(properties.playSound);
            if($rootScope.isMobile)
                TweenMax.to(".top-bar-points", 0.5, {borderColor:"#33b3ed",repeat:3,yoyo:true,delay:.2});
        }
        $scope.updateUserBalances();
      });

      $scope.getUser = function() {
        return {
          'isGuest': $scope.isGuest,
          'nickname': $scope.nickname,
          'picUrl': $scope.picUrl,
          'levelPoints': $scope.levelPoints,
          'currentLevel': $scope.currentLevel,
          'points': $scope.points,
          'gameCoins':$scope.gameCoins,
          'isFacebookUser': $scope.isFacebookUser,
          'isTwitterUser': $scope.isTwitterUser,
          'unreadNotifications': $scope.unreadNotifications,
          'checkIns': $scope.checkIns
        };
      }

      $scope.getUsername = function() {
        if ($scope.isGuest){
          return $filter('translate')('Guest');
          }
        else{
          return $scope.nickname;
        }
      };

      $scope.logUser = function() {
        if (!$scope.isGuest) {
          ngDialog.openConfirm({
            template: 'templates/dialogs/generic_confirmation.html',
            className: 'ngdialog-theme-mall',
            data: angular.toJson({
              'body': $translate.instant('Logout_Dialog_Message_Web'),
            }),
          }).then(function() {
            TalosValues.clearUserData();
            TalosCore.guest();
            $state.go("tab.home");
          });
        } else $state.go('login');
      }

      $scope.updateLatestNotification = function() {
        Messages.get(0, 1).then(function(data) {
            if (data.data.length) {
                try {
                  $scope.latestNotificationId = data.data[0].id;
                  $scope.latestNotification = data.data[0].content;
                } catch(e) {}
            } else {
              $scope.latestNotificationId = '';
              $scope.latestNotification = '';
            }
        });
      }

      $scope.$on(Events.LANGUAGE_SET, function () {
          $scope.updateLatestNotification();
      });

      $scope.getLatestNotification = function() {
          if (!$scope.latestNotificationId.length)
              return $translate.instant('No_New_Messages');
          else {
              if ($scope.latestNotification.length > Constants.LATEST_NOTIFICATION_MAX_LENGTH)
                  return $scope.latestNotification.substring(0, Constants.LATEST_NOTIFICATION_MAX_LENGTH) + '...';
              else
                  return $scope.latestNotification;
          }
      }

  
      var setView = function (profile){
          $scope.isGuest = profile.guest;
          $scope.nickname = profile.nickname;
          $scope.picUrl = profile.picUrl? profile.picUrl: Constants.DEFAULT_AVATAR;
          $scope.isFacebookUser = (profile.extAppSourceIds) && (profile.extAppSourceIds.indexOf(Constants.EXTERNAL_SYSTEM_FACEBOOK) != -1);
          $scope.isTwitterUser = (profile.extAppSourceIds) && (profile.extAppSourceIds.indexOf(Constants.EXTERNAL_SYSTEM_TWITTER) != -1);
          $scope.updateUserBalances();
          $scope.updateLatestNotification();
          $scope.getUnreadNotifications();
          
          if (profile!=null && !profile.guest)
                    $scope.showLogin=false;
      }
       
      $scope.$on(Events.PROFILE_RECEIVED,function (event,data)
      {
            setView(data);                  
      });
      
      
       $scope.$on(Events.PROFILE_UPDATED,function (event,data)
      {
            setView(Profile.profile);                  
      });
      
      
      $scope.$on(Events.NOTIFICATION_REMOVED, function(event, properties) {
        try {
            if (($scope.latestNotificationId == properties.id)) {
                $scope.latestNotificationId = (properties.newId? properties.newId: '');
                $scope.latestNotification = (properties.newText? properties.newText: '');
                $scope.getUnreadNotifications();
            }
        } catch(e) {$scope.getUnreadNotifications();}
      });

      $scope.$on(Events.NOTIFICATION_READ, function(event, properties) {
          $scope.getUnreadNotifications();
      });
      
      $scope.getLoginTitle = function() {
          return (($scope.isGuest)? $translate.instant('User_Menu_Login'): $translate.instant('User_Menu_Logout'));
      }

   
        Profile.get(setView,false);
        
        
        //$scope.openSideBar = function(name) {
        //    $rootScope.isLoadingBadges = false;
        //    ToggleHelper.toggle(name);
        //}

  $scope.toggle_visibility = function()
            {          
                      $scope.showLogin = !$scope.showLogin;
            };
$scope.$on(Events.HIDE_OR_SHOW_LOGIN,function (event,data)
          { 
              $scope.showLogin = data.show;
          });
          
 
       
       $scope.tostateFromSideBar = function(state,isGuest){

            if(isGuest)
            return;
            
            $state.go(state);

        };
       
       
       
    }]);
