'use strict';

/**
 * @ngdoc object
 * @name core.Controllers.TranslateController
 * @description TranslateController
 * @requires ng.$scope
*/
controllers.controller('TranslateController', [
        '$translate', '$scope', '$rootScope', 'TalosValues','Events','EventService',
        function($translate, $scope, $rootScope, TalosValues,Events,EventService) {
            //console.log('TranslateController, current language: ' + TalosValues.getLanguage());

            //$scope.language = TalosValues.getLanguage();
            //$translate.use(TalosValues.getLanguage());

            $scope.$on(Events.TRANSLATE_CONTROLLER_CONFIG, function () {
                  var lang = TalosValues.getLanguage();
                    $rootScope.language = lang
                  $translate.use(lang);
                  if(!$rootScope.isMobile)
                  $rootScope.showSplash = false;
                  moment.locale(lang);
              });


        }
]);
