'use strict';

controllers.controller('CouponDetailsCtrl', [
    '$scope','$rootScope', '$filter', 'Constants', 'Coupons', 'Resources', 'Utils', '$state', '$stateParams', 'TalosValues',  'Favorites', 'ItemStatistics', 'Comments', 'Rating', 'Events', 'EventService', 'GlobalData', '$translate', 'Brands', 'ApplicationSettings', 'TalosEvents','$log','CouponCategories','$location','$anchorScroll','$ionicPopup', '$ionicLoading',
    function($scope,$rootScope,$filter, Constants, Coupons, Resources, Utils, $state, $stateParams, TalosValues,  Favorites, ItemStatistics, Comments, Rating, Events, EventService, GlobalData, $translate, Brands, ApplicationSettings, TalosEvents,$log,CouponCategories,$location,$anchorScroll, $ionicPopup, $ionicLoading) {
      console.log('CouponDetailsCtrl');

      $scope.listIsLoading = false;
      $scope.itemId = '';
      $scope.itemTypeId = '';
      $scope.bookmarkListType = 'wishlist';
      $scope.name = '';
      $scope.shareTitle = $filter('translate')('ShareEmail_Title_Coupon');
      $scope.favBody = $filter('translate')('Wishlist_Add_ConfirmationBody');
      $scope.favSuccessMsg = $filter('translate')('Wishlist_Added');
      $scope.favErrorMsg = $filter('translate')('Wishlist_NotAdded');
      
      
      if (($state.current.name == 'tab.giftDetail') || ($state.current.name == 'tab.giftDetail')) {
        console.log($stateParams.couponID);
        $scope.itemId = $stateParams.couponID;
        $scope.itemTypeId = Constants.ITEM_TYPE_BRAND_PRODUCT;
      }


      $scope.showView = false;
      $scope.isLoadingCoupons=false;
      $scope.showViewStep = 0;
      if ($state.current.name == 'tab.giftDetail')
      {
        $scope.showViewNSteps = 2;
        $scope.isLoadingCoupons = true;
      }
      else if ($state.current.name == 'tab.giftDetail') {
        $scope.showViewNSteps = 2;
      }else if ($state.current.name == 'tab.gifts') {
        $scope.showViewNSteps = 4;
      }
      else {
        $scope.showViewNSteps = 2;
      }
      /* coupons state */
      var _allItemsLoaded = false;
      $scope.coupons = [];
      $scope.couponsSize = 0;

      /* coupon state */
      $scope.coupon = {};
      $scope.couponStatusId = '';
 
      $scope.likes = 0;
      $scope.liked = false;
      $scope.isFavorite = false;
      $scope.isMyCoupon = (($state.current.name == 'tab.giftDetail')? true: false);
             
      $scope.isGuest = true;
 

      $scope.couponStatus = $scope.allCouponStatus;
      $scope.couponStatusText = '';
      $scope.couponStatusMode = 'all';
      
      $scope.couponFilterId = '';
      $scope.couponFilterName = '';
      $scope.couponFilterCategory = '';
      $scope.brandTagFilter = '';
      $scope.brands = [];
      $scope.tags = [];
      $scope.brandTags = [];

      $scope.showDetailsButton = ($state.current.name == 'tab.gifts' && !$scope.isMobile);
 

      $scope.getCurrentCouponFilter = function() {
        return ($scope.couponFilterId.length || $scope.brandTagFilter.length? $scope.couponFilterName: 'All');
      }

 

      function searchForCouponsStateParams()
      {


        if(CouponCategories.activeFilters)
        {
          setFromActiveFilters();
          loadAfterFilterSelected();

        }
        else {
          if (angular.isDefined($stateParams.category)) {
            var i, l;
            var brand;
            var brandTag;
            l = $scope.brandTags.length;
            for (i = 0; i < l; i++) {
              brandTag = $scope.brandTags[i];
              if (brandTag.id == $stateParams.category) {
                $scope.selecetedBrandTag = brandTag;
                break;
              }
            }

            if (!$scope.selecetedBrandTag)
              $scope.selecetedBrandTag = $scope.brandTags[0];

          }
          else {
            $scope.selecetedBrandTag = $scope.brandTags[0];
          }


          if (angular.isDefined($stateParams.store)) {


            l = $scope.brands.length;
            for (i = 0; i < l; i++) {

              if ($scope.brands[i].brandId == $stateParams.store) {
                brand = $scope.brands[i];
                break;
              }
            }

          }

          if (brand) {
            $scope.applyBrandTagCategoryFilter("all", $scope.selecetedBrandTag, $scope.getFilterName($scope.selecetedBrandTag, 'brandTag'), true);
            $scope.applyFilter('brand', brand.brandId, $scope.getFilterName(brand, 'brand'))
          }
          else {
            $scope.applyBrandTagCategoryFilter("all", $scope.selecetedBrandTag, $scope.getFilterName($scope.selecetedBrandTag, 'brandTag'));
          }
        }
      }
      function loadAfterFilterSelected()
      {
        $scope.showViewStep = 2;
        $scope.isLoadingCoupons = true;
        var _allItemsLoaded = false;
        $scope.coupons = [];
        $scope.couponsSize = 0;

        $scope.addMoreItems();
      }

      $scope.applyBrandTagCategoryFilter = function(type,id, name,notload)
      {
        if(type == 'all')
        {
          $scope.selecetedBrandTag = id;
          $scope.brandTagFilter = $scope.selecetedBrandTag.ids;
          $scope.couponFilterId = '';
          $scope.couponFilterCategory = '';
        }
        else{
          $scope.brandTagFilter = id;
        }
        $scope.couponFilterName = name;

         if(notload == 'undefined' || !notload)
         loadAfterFilterSelected();
      }

        $scope.applyFilter = function(category, id, name,notload) {
            $scope.couponFilterCategory = category;
            $scope.couponFilterId = id;
            $scope.couponFilterName = name;
          if(notload == 'undefined' || !notload)
          loadAfterFilterSelected();
        }

        $scope.hasFilter = function() {
            return ($scope.couponFilterId.length? true: false);
        }

        $scope.getFilterName = function(item, category) {
            try {
                if (item.names[TalosValues.getLanguage()].length)
                    return item.names[TalosValues.getLanguage()];
                else {
                    if (category == 'brand')
                        return item.name;
                    else
                        return item.tagName;
                }
            } catch(error) {
                return "";
            }
        }


      $scope.handleCouponsResponse = function(data) {
        if (data.data.length)
          $scope.getCouponResources(data.data, 'tab.gifts');
        else $scope.checkShowView(1);

        $scope.coupons = $scope.coupons.concat(data.data);
        $scope.couponsSize = parseInt(data.headers(Constants.HEADER_XTalosItemCount));
        if (!$scope.couponsSize) $scope.coupons = [];
        if ($scope.couponsSize > $scope.coupons.length)
          _allItemsLoaded = false;

        $scope.checkShowView(1);
      }

      $scope.loadMore = function() {
        return _allItemsLoaded;
      };

      $scope.changeCouponStatus = function(newCouponStatus, newCouponStatusMode, newCouponStatusText) {
        $scope.showView = false;
        $scope.showViewStep = 0;

        $scope.couponStatus = newCouponStatus;
        $scope.couponStatusText = $filter('translate')(newCouponStatusText);
        $scope.couponStatusMode = newCouponStatusMode;

        _allItemsLoaded = false;
        $scope.coupons = [];
        $scope.couponsSize = 0;
        $scope.addMoreItems();
      }

      $scope.isSelectableCouponStatusMode = function(checkMode) {
        return !($scope.couponStatusMode == checkMode);
      }

      
      
      $scope.getCouponLogoResourceUrl = function(coupon) {
        try {
          return Resources.getResourceUrl(coupon.brandProductId, Constants.ITEM_TYPE_BRAND_PRODUCT, Constants.RESOURCE_TYPE_IMAGE, Constants['LANGUAGE_ID_' + TalosValues.getLanguage()]);
        } catch (error) {
          return "";
        }
      }

      
      
      
      
      $scope.couponHasTag = function(coupon) {
        try {
          return coupon.tagIds.length? true: false;
        } catch(error) { return false; }
      }

      
      
      
      
      $scope.getTagLogoResourceUrl = function(coupon) {
        try {
          if (coupon.tagIds.length) {
            var tagId = coupon.tagIds[0];
            return Resources.getResourceUrl(tagId, Constants.ITEM_TYPE_CRITERION, Constants.RESOURCE_TYPE_IMAGE, Constants['LANGUAGE_ID_' + TalosValues.getLanguage()]);
          }
        } catch (error) {
          return "";
        }
      }

      
      
      
      $scope.getCouponImageLargeResourceUrl = function(coupon) {
        try {
          return Resources.getResourceUrl(coupon.brandProductId, Constants.ITEM_TYPE_BRAND_PRODUCT, Constants.RESOURCE_TYPE_IMAGE_LARGE, Constants['LANGUAGE_ID_' + TalosValues.getLanguage()]);
        } catch (error) {
          return "";
        }
      }

      
      
      
      $scope.getCouponEndDate = function(coupon) {
        try {
          var date;
          if ($state.current.name == 'tab.gifts')
            date = new Date(coupon.endData);
          else
            date = new Date(coupon.activeUntil);
         // if ($state.current.name == 'tab.coupon')
            return date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
          //else
            //return date.getDate() + ' ' + $filter('translate')('Month_' + date.getMonth()) + ' ' + date.getFullYear();
        } catch(error) {
          return "";
        }
      }

      
      
      
      
      $scope.getCouponResource = function(coupon, resource) {
        try {
          return coupon.resources[TalosValues.getLanguage()][resource];
        } catch(error) {
          return "";
        }
      }

      
      
      
      $scope.getCouponPrice = function(coupon) {
        try {
          var amount, resAmount = 0;
          if ($state.current.name == 'tab.gifts') {
            amount = coupon.price.amount;
            resAmount = (amount.value / Math.pow(10, amount.scale));
          } else {
            angular.forEach(coupon.prices, function(price, key) {
              if (price.brandServiceId == Constants.DEFAULT_BRAND_SERVICE_ID) {
                amount = price.prices[0].amount;
                resAmount = (amount.value / Math.pow(10, amount.scale));
              }
            });
          }
          return Utils.numberFormat(resAmount, 0, ',', '.');
        } catch(error) {
          return "";
        }
      }

     
      
      
      $scope.getCouponBrandImageResourceUrl = function(coupon) {
        try {
          return Resources.getResourceUrl(coupon.brandId, Constants.ITEM_TYPE_BRAND, Constants.RESOURCE_TYPE_IMAGE, Constants['LANGUAGE_ID_' + TalosValues.getLanguage()]);
        } catch (error) {
          return "";
        }
      }

 
      
      $scope.getCouponResources = function(coupons, state) {
        var resource = {},
          resources = Array(),
          input = {},
          property = "",
          j;


          

        try {
          angular.forEach(coupons, function(coupon, key) {
            coupon.resources = {};
            angular.forEach(Constants.LANGUAGE_IDS, function(language, languageKey) {
              coupon.resources[Constants['LANGUAGE_CODE_' + language]] = {
                "name": "",
                "description": "",
                "brandName": "",
                "POST_TO_WALL_PIC_URL":"",
                "POST_TO_WALL_TEXT":"",
                "POST_TO_WALL_TITLE":"",
                "TWITTER_POST_TITLE": ""


              };
            });

            resource = {};
            resource.itemId = coupon.brandProductId;
            resource.itemTypeId = Constants.ITEM_TYPE_BRAND_PRODUCT;
            resource.resourceTypeId = Constants.RESOURCE_TYPE_NAME;
            resources.push(resource);

            resource = {};
            resource.itemId = coupon.brandId;
            resource.itemTypeId = Constants.ITEM_TYPE_BRAND;
            resource.resourceTypeId = Constants.RESOURCE_TYPE_TITLE;
            resources.push(resource);

            if (state == "tab.gifts") {
                Utils.trackScreen("coupon id:"+coupon.brandProductId);
              $scope.postResources =  coupon.resources;
              resource = {};
              resource.itemId = coupon.brandProductId;
              resource.itemTypeId = Constants.ITEM_TYPE_BRAND_PRODUCT;
              resource.resourceTypeId = Constants.RESOURCE_TYPE_DESCRIPTION;
              resources.push(resource);

            }

          });


          input.languageIds = Constants.LANGUAGE_IDS;
          input.resources = resources;
          Resources.getMultipleResources(0, -1, input,Constants.APPLICATION_SETTING_COUPONS_RESOURCES_VERSION).then(function(data) {
            angular.forEach(data.data, function(resource, resourceKey) {
              if (resource.itemTypeTO.itemTypeId == Constants.ITEM_TYPE_BRAND_PRODUCT) {
                //for (j = 0; j < coupons.length; j++)
                  //if (resource.itemId == coupons[j].brandProductId) break;
                switch (resource.resourceTypeTO.resourceTypeId) {
                  case Constants.RESOURCE_TYPE_NAME:
                    property = "name";
                    break;
                  case Constants.RESOURCE_TYPE_DESCRIPTION:
                    property = "description";
                    break;

                }
                //coupons[j].resources[Constants["LANGUAGE_CODE_" + resource.languageTO.languageId]][property] = resource.textResource;
                angular.forEach(coupons, function(coupon, couponKey) {
                  if (resource.itemId == coupon.brandProductId) {
                    coupon.resources[Constants["LANGUAGE_CODE_" + resource.languageTO.languageId]][property] = resource.textResource;
                    if (property == 'name') {
                        coupon.resources[Constants["LANGUAGE_CODE_" + resource.languageTO.languageId]]['TWITTER_POST_TITLE'] = resource.textResource;
                        coupon.resources[Constants["LANGUAGE_CODE_" + resource.languageTO.languageId]]['POST_TO_WALL_TITLE'] = resource.textResource;
                        coupon.resources[Constants["LANGUAGE_CODE_" + resource.languageTO.languageId]]['POST_TO_WALL_PIC_URL'] = $scope.getCouponLogoResourceUrl(coupon);
                    } else if (property == 'description') {
                        coupon.resources[Constants["LANGUAGE_CODE_" + resource.languageTO.languageId]]['POST_TO_WALL_TEXT'] = resource.textResource;
                    }
                  }
                });
              } else if (resource.itemTypeTO.itemTypeId == Constants.ITEM_TYPE_BRAND) {
                property = "brandName";
                angular.forEach(coupons, function(coupon, couponKey) {
                  if (resource.itemId == coupon.brandId)
                    coupon.resources[Constants["LANGUAGE_CODE_" + resource.languageTO.languageId]][property] = resource.textResource;
                });
              }
            });

            

            $scope.name = $scope.getCouponResource($scope.coupon, "name");
       
              
            $scope.checkShowView(1);
          }, function() {
            $scope.checkShowView(1);
          });
        } catch (error) {
          $scope.checkShowView(1);
        }
      };
      
   
      
   $scope.purchaseCoupon = function(coupon) {
     
           console.log("purchase function!");
           
//           if ($state.current.name != 'coupon') return;

           var user = GlobalData.user;
 
           if (user.isGuest) {
               var alertPopup = $ionicPopup.alert({
                             title: '',
                             template: 'You have to login to purchase the coupon!'
                      });

                return;
           }
           
           
           
           var confirmPopup = $ionicPopup.confirm({
                     title: '',
                     template: '{{"Coupon purchase confirm" |translate }}?'
           });
                 
           confirmPopup.then(function(res) {
                     if(res) {
                             console.log('You are sure');
                             $ionicLoading.show({
                                        template: 'Checking...'       
                             }); 
                      var input = {
                               'brandProductId': coupon.brandProductId,
                               'quantity': 1,
                               'amountsToPay': Array({
                               'amount': $scope.getCouponPrice(coupon).replace(/\./g, ''),
                               'unitTypeId': Constants.UNIT_TYPE_POINTS
                                })
                      }
                    try {
            Coupons.purchase(input).then(function(data) {
                  
                         $ionicLoading.hide();
                         EventService.sendEvent(Events.BALLANCE_UPDATED);

                         $ionicPopup.alert({
                                              title: 'Success!',
                                              template: '{{"Coupon is purchased" | translate}}!'
                         });
                         
                         
            }, function(data) {
               var errorMsg = $filter('translate')('Coupon_NotPurchased');
               try {
                          if (data.data.code == Constants.COUPON_PURCHASE_USER_SESSION_PERMISSION_ERROR)
                            errorMsg = $filter('translate')('Coupon_NotPurchased_Invalid');
                          else if (data.data.code == Constants.COUPON_PURCHASE_INSUFFICIENT_BALANCE_FOR_PURCHASE_ERROR)
                            errorMsg = $filter('translate')('Coupon_NotPurchased_Balance');
              } catch(error1) {}
              
                      $ionicLoading.hide();
                       
                      $ionicPopup.alert({
                                  title: 'Error!',
                                  template: '{{"You cannot purchase the coupon" | translate}}'
                      });
            });
          } catch(error) {
            
                 $ionicPopup.alert({
                          title: 'Error!',
                          template: '{{"Coupon was not purchased" | translate}}'
                 });
          }
          }
               else {
                             console.log('You are not sure');
                             return;
                      }
           }, function() {});
                     
          };

        
        
      $scope.checkShowView = function(steps) {
        $scope.showViewStep += steps;
          $scope.listIsLoading = false;
        if ($scope.showViewStep == $scope.showViewNSteps) {
            $scope.showView = true;
 
          }
      }

            $scope.showAlert = function() {
                    var alertPopup = $ionicPopup.show({
                    title: "",
                    templateUrl : 'templates/popup-aquire.html',
                    scope: $scope,
                    buttons: [
                  { text: 'OK',
                    type: 'button-positive',
                    onTap: function(e) {
                      //add code
                      return 'cancel button'
                        }
                  },]
                  });
                    alertPopup.then(function(res) {
                        //add code
              });
            };
        
        
            Utils.trackScreen("coupon id:"+$stateParams.couponID);
            Coupons.get(0, -1, Array($stateParams.couponID), true).then(function(data) {
                $scope.coupon = data.data[0];
                console.log(JSON.stringify($scope.coupon));

                if (data.headers(Constants.HEADER_XTalosItemCount) == "1") {
                    $scope.getCouponResources(Array($scope.coupon), 'tab.gifts');
                    $scope.likes = Utils.getStatisticsValue($scope.coupon.statistics, Constants.STATISTIC_TYPE_ITEM_LIKE, Constants.ITEM_TYPE_BRAND_PRODUCT, $scope.coupon.brandProductId, 'ratings');
                    $scope.liked = Utils.getStatisticsValue($scope.coupon.statistics, Constants.STATISTIC_TYPE_ITEM_LIKE, Constants.ITEM_TYPE_BRAND_PRODUCT, $scope.coupon.brandProductId, 'rated');
                    $scope.isFavorite = Utils.getStatisticsValue($scope.coupon.statistics, Constants.STATISTIC_TYPE_ITEM_FAVORITES_ADDED, Constants.ITEM_TYPE_BRAND_PRODUCT, $scope.coupon.brandProductId, 'rated');
                    ItemStatistics.getSingle(Constants.ITEM_TYPE_BRAND_PRODUCT, $scope.coupon.brandProductId, Constants.STATISTIC_TYPE_ITEM_LIKE).then(function(data) {

                    });
                } else $state.go('tab.gifts');

                $scope.checkShowView(1);

            }, function() {
                $scope.checkShowView(2);
            });
        

 

    }
  ]);
