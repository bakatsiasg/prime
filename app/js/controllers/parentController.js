/**
 * Created by georgefloros on 3/30/15.
 */
'use strict';
controllers.controller('ParentCtrl',['$scope','$rootScope','$state','TalosValues','$translate','$window','InitService','$ionicHistory','EventService'
    ,'Events','$ionicConfig','$ionicBackdrop','$timeout','localStorageService','$ionicLoading',
    'Utils','Profile','$ionicPlatform','$cordovaKeyboard', '$http', '$location','Constants', 'States','CouponCategories',"$ionicTabsDelegate",
    function($scope,$rootScope,$state,TalosValues,$translate,$window, InitService, $ionicHistory,
             EventService,Events,$ionicConfig, $ionicBackdrop, $timeout,
             localStorageService,$ionicLoading,Utils,Profile,$ionicPlatform,$cordovaKeyboard, $http, $location, Constants, States,CouponCategories,$ionicTabsDelegate) {

      $rootScope.appHeight = window.innerHeight;
      $scope.currentTransition = "fade";
      $rootScope.showRegister = false;
      var loadingPopUpPromise;

      var windowElement = angular.element($window);
            windowElement.on('beforeunload', function (event) {
                // do whatever you want in here before the page unloads.        
            
                // the following line of code will prevent reload or navigating away.
                event.preventDefault();
      });
      
        
        
        $rootScope.closeKeyBoard = function(){

            if($cordovaKeyboard.isVisible())
            {
                $cordovaKeyboard.close();
            }
 
        }
        $rootScope.changeLanguage = function (langKey) {
                $rootScope.language = langKey;
                TalosValues.setLanguage(langKey);
                $translate.use(langKey);
                moment.locale(langKey);
                EventService.sendEvent(Events.LANGUAGE_SET);
              }

        $rootScope.showLoading = function(message) {
            $timeout.cancel(loadingPopUpPromise);
            $ionicLoading.hide();
            $ionicLoading.show({
            template:$translate.instant(message)+'</br><object height="20%" width="30%" type="image/svg+xml" data="img/loadingOrange.svg"> </object>'
            });
        };

        $rootScope.hideLoading = function(){
            $ionicLoading.hide();
        };
        $rootScope.showErrorPopUp = function  (params){

            // $rootScope.showErrorPopUp({msg:"NETWORK_ERROR_TRY_AGAIN",actions:[{'label':'ok',action:null}]});

            $rootScope.showMainPopUp('templates/popUp/errorMessage.html',params)
        }

        $rootScope.showMainPopUp = function  (content,params){
            $rootScope.hideLoading();
            $rootScope.mainPopUpProps = {show:true,
            content:content,
            params:params};
        }
        
    $rootScope.closeMainPopUp = function  (){
        delete $rootScope['mainPopUpProps'];
        $timeout(function(){
            delete $rootScope['mainPopUpProps'];
        },250);
    }

    $rootScope.myGoBack = function (){      
        $ionicHistory.goBack();      
    }


    // $rootScope.goToState =  function(state,params){
  
    //             $state.go(state);
    //             //console.log('$state.go(state); = '+ state);
    //             //console.log(params);
   // }; 

        $rootScope.goToState =  function(state,params,b){
  
        console.log('$state.go(state); = '+ state);
        console.log(params);

        if(angular.isDefined(b) && b)
        {
               $ionicTabsDelegate.select(5);
               $state.go(state);   
        }
        else{
               $state.go(state);
        }

        }; 
   
   
                $rootScope.version = '2.2.4';
                $rootScope.showSplash = true;
                $rootScope.fu2llscreen = false;
                $rootScope.fullscreen = false;
                $rootScope.showLoadingMessage = false;
                $rootScope.isHomePage = false;
                $rootScope.initNetworkError = false;
                $rootScope.dialogIsOpen = false;
                $rootScope.showShareActionSheet = false;
    
                $rootScope.changeLanguage = function (langKey) {
                $rootScope.language = langKey;
                TalosValues.setLanguage(langKey);
                $translate.use(langKey);
                moment.locale(langKey);
                EventService.sendEvent(Events.LANGUAGE_SET);

                /**SET SERVICES FOR LANG CHANGE*/
                // Profession.setLocaleNames(langKey);
                // Countries.setLocaleNames(langKey);
                States.setLocaleNames(langKey);
            }
            
  
            // $rootScope.$on('ngDialog.closing', function(dialog){
            //     $rootScope.dialogIsOpen = false;
            // });
            // $rootScope.$on('ngDialog.opened', function(dialog){
            //     $rootScope.dialogIsOpen = true;
            // });


            $rootScope.toAction = function(item){
                console.log("to Action function is active.");
                if(item.allowGuest==0)
                {
                    if(Utils.checkIsGuestAndLogin('Login_Enter'))
                        return;

                }
                $rootScope.handleAction(item.action);
            }
            
            
            
            $rootScope.handleAction=function (action)
            {
                if(action.indexOf("prime:\/\/") < 0)
                return;

                CouponCategories.activeFilters = null;

                action = action.replace('prime:\/\/','');
                var actions = action.split("?");
                action = actions[0];
                var value,values,arr,l,i;
                var obj = {};

                if(actions.length > 1) {
                    values = actions[1].split("&");
                    l = values.length;
                    for(i = 0 ; i<l ; i++)
                    {
                        try{
                        value = values[i];
                        arr = value.split("=");
                        obj[arr[0]] = arr[1];
                        }catch (error){};
                    }
                }
                try{$state.go(action,obj)}catch(error){}

            }

            $rootScope.$on(Events.LOAD_APP_CONFIG, function(event, properties){
                loadConfig(properties.url);//dev
            });

   
          var loadConfig = function (url) {
                $http.get(
                    url,
                       {
                           transformResponse: function (data) {
                           // convert the data to JSON and provide
                           // it to the success function below
                           var x2js = new X2JS();
                           var json = x2js.xml_str2json(data);
                           return json;
                        }
                    }
                ).success(function (data, status) {

                        // Constants.SERVER = data.root.server;//;//data.root.server;
                        // Constants.SERVER = "https:\/\/dev.saicongames.com:7090/talos_api/v1"; //talos api.

                           Constants.SERVER = "http:\/\/dev.saicongames.com/talos/v1"; //proxy.
                       //  Constants.SERVER = "http:\/\/sherlock:7091/talos_api/v1";


                        var appId,clientType,magicKey;


                        if(ionic.Platform.isAndroid())
                        {
                            clientType = 6;
                            appId = "839A6D2C-A27C-483A-F925-F218BDE6E65C";
                            magicKey = "zY2MDQxM0EtNDNERC00OTYyLUIyNjktND";


                        }
                        else if(ionic.Platform.isIOS())
                        {
                            clientType = 3;
                            appId = "04AD86F6-955D-41AA-ABD6-559C586583B3";
                            magicKey = "DU5NDA5RjAtNjZBNi00QzhDLUEwQ0EtQkVCN0";

                        }
                        else { //portal

                            clientType = 4;
                            appId = "BBCACC13-5270-48E4-8191-DD589486C269";
                            magicKey = "jM5NDQtRjc4NC00MjMyLUI4REUtQkVGNDh";

                        }


                        Constants.SERVER = "https://dev.saicongames.com/talos_api/v1";
                        Constants.RESOURCE_URL = "https://dev.saicongames.com/talos_api/resource";
                        Constants.API_VERSION = "v1"
                        Constants.CLIENT_TYPE = clientType;
                        console.log(Constants.CLIENT_TYPE);
                        
                        Constants.APPLICATION_ID = appId;
                        console.log(Constants.APPLICATION_ID);
                        
                        Constants.MAGIC_KEY = magicKey;
                        console.log(Constants.MAGIC_KEY);
                        
                        Constants.OS_TYPE = ionic.Platform.platform();
                        console.log(Constants.OS_TYPE);
                        // Constants.TRANSLATE_URL = "translate/";
                        // Constants.TRANSLATE_SUFFIX = ".json";


                        Constants.API_VERSION = data.root.apiVersion;
                        Constants.RESOURCE_URL = data.root.resourceUrl;
                        Constants.USER_AVATAR_IMAGE_URL = data.root.userAvatarImageUrl;
                        Constants.FACEBOOK_PROFILE_URL = data.root.facebookProfileUrl;
                        Constants.TWITTER_PROFILE_URL = data.root.twitterProfileUrl;
                        Constants.TRANSLATE_URL = data.root.translate.prefix;
                        Constants.TRANSLATE_SUFFIX = data.root.translate.suffix;
                        Constants.TERMS_URL = data.root.tesrmsUrl;
                        Constants.FB_APP_REQUEST_URI = data.root.facebookAppRequestURI;
                        Constants.HOW_TO_URL = data.root.resources.howToUrl;
                        Constants.FLOOR_IMAGE_URL = data.root.resources.floorImageUrl;
                        Constants.APP_STORE_URL = data.root.resources.appStoreUrl;
                        Constants.GOOGLE_PLAY_URL = data.root.resources.googlePlayUrl;

                    
                      
                        $rootScope.languagesArr = data.root.translate.preferredLanguages.language;
                        $rootScope.languagesArrLength = $rootScope.languagesArr.length - 1;
                        $rootScope.languagesArrCurrentIndex = 0;
                    
                        
                        $translate.use($rootScope.languagesArr[$rootScope.languagesArrCurrentIndex]);
                       
                        //$rootScope.translateProvider.preferredLanguage(data.root.translate.preferredLanguage); // must be always the last set
                        EventService.sendEvent(Events.APP_CONFIG_LOADED);

                    }).error(function (data

                        , status) {
                        loadConfig(url);
                    })
            }
    
    
}]);