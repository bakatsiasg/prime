/**
 * Created by georgefloros on 4/2/15.
 */

'use strict';
controllers.controller('SideMenuCtrl',['$scope','$rootScope','$location','TalosCore','$log','EventService','$ionicHistory','$ionicSideMenuDelegate','Events',
    '$state','Constants','Utils', '$ionicLoading', '$ionicPopup', 'Profile','TalosValues','localStorageService','ApplicationSettings',function($scope,$rootScope,$location,TalosCore,$log,EventService,Events,$state,
        Constants,Utils,$ionicHistory,$ionicSideMenuDelegate, $ionicLoading, $ionicPopup, Profile, TalosValues,localStorageService,ApplicationSettings) {


        $scope.isGuest = true;
        $scope.showLogin = false;


        var setView = function (profile){
            $scope.isGuest = profile.guest;
            
        }   
             
         Profile.get(setView,false);
            
           
          $scope.$on(Events.PROFILE_RECEIVED,function (event,data)
          {
              setView(data);
          });


         $scope.$on(Events.HIDE_OR_SHOW_LOGIN,function (event,data)
          { 
              $scope.showLogin = data.show;
              
          });
          
              
         $rootScope.logoutBtn = function() {

                 var confirmPopup = $ionicPopup.confirm({
                     title: 'Logout',
                     template: '{{"Are you sure you want to logout?" | translate}}'
                 });
                 
                 confirmPopup.then(function(res) {
                     if(res) {
                             console.log('You are sure');
                             $ionicLoading.show({
                                        template: 'Logout...'       
                             }); 
                             TalosValues.clearUserData();
                             Profile.clear();
                             $scope.showLogin = false;
                            
                             TalosCore.guest().then(function(data)
                             {
                                     Profile.get(function(profile){
                                         
                                          $ionicLoading.hide();
         
                                     },true);
                                  
                                 
                             });
                             
                            $rootScope.goToState('login');
                             
                     }  else  {
                             console.log('You are not sure');
                             return;
                             }
                 });

       } ;


}]);