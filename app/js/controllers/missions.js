'use strict';
controllers.controller('MissionsCtrl',['$scope','$rootScope','EventService','Events',
    '$state','Constants','Utils','TalosValues','localStorageService',function($scope,$rootScope,EventService,Events,$state,
        Constants,Utils, TalosValues,localStorageService) {

    $scope.missions = [{
    id:0,
    header:'Ασφάλειες εν οίκω για κύρια κατοικία',
    image:'img/card_image.png',
    mainText:'Asfaleies en oiko gia kyria katoikia',
    points:'100',
    ratio:'5/10'
  }
  , {
    id:1,
    header:'Προσφορές για σκάφη αναψυχής ',
    image:'img/card_image.png',
    mainText:'2 eisitiria gia ta village cinemas',
    points:'1.500',
    ratio:'4/10'

  }, {
    id:2,
    header:'Ανανεώσεις συμβολαίων ',
    image:'img/card_image.png',
    mainText:'2 eisitiria gia ta village cinemas',
    points:'1.000',
    ratio:'7/10'

  }];

}]);